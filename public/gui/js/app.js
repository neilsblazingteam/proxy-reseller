$(document).ready(function () {

    // MetsiMenu
    $('#side-menu').metisMenu();

    // Minimalize menu when screen is less than 768px
    $(window).bind("resize", function () {
        if ($(this).width() < 769) {
            $('body').addClass('body-small')
        } else {
            $('body').removeClass('body-small')
        }
    });

    // app specific
    // proxy replacement
    $('.ui-replace-proxy').click(function () {

        $('input[name="replace_ip"]').val($(this).attr('data-value'));

        $('#form_replace_proxy').submit();

    });

    // password reset
    $('#ui-password-reset-fields').hide();
    $('#ui-password-reset-button').click(function() {
        $('#ui-password-reset-fields').show();
    });

    $('#ui-password-reset-cancel').click(function() {
        $('input[name="password_current"]').val('');
        $('input[name="password_new"]').val('');
        $('input[name="password_repeat"]').val('');
        $('#ui-password-reset-fields').hide();
    });

    // dashboard plans
    var currentSelectedCountry = $('#firstCountry').attr('data-value'),
        currentSelectedCategory = $('#firstCategory').attr('data-value');

    $('.plan-box[data-country="'+currentSelectedCountry+'"]').fadeIn();
    $('.plan-box-alert[data-country="'+currentSelectedCountry+'"][data-category="'+currentSelectedCategory+'"]').fadeIn();

    $('#ddCountryList').html($('.plan-country-selector[data-value="'+currentSelectedCountry+'"]').html());
    $('#ddCategoryList').html($('.plan-country-selector[data-value="'+currentSelectedCategory+'"]').html());

    $('.plan-country-selector').click(function(){

        $('.plan-box').hide();
        $('.plan-box-alert').hide();

        var selector = $(this).parents('ul').attr('aria-labelledby');
        $('#'+selector).html($(this).html());

        currentSelectedCountry = $(this).attr('data-value');
        $('.plan-box[data-country="'+currentSelectedCountry+'"]').fadeIn();
        $('.plan-box-alert[data-country="'+currentSelectedCountry+'"]').fadeIn();

    });
    $('.plan-category-selector').click(function(){

        $('.plan-box').hide();

        if(currentSelectedCountry=='') {
            swal({
                title: "Hold on",
                text: "Please select a country first"
            });
        } else {
            var selector = $(this).parents('ul').attr('aria-labelledby');
            $('#'+selector).html($(this).html());

            currentSelectedCategory = $(this).attr('data-value');

            $('.plan-box[data-country="'+currentSelectedCountry+'"][data-category="'+currentSelectedCategory+'"]').fadeIn();
        }

    });

    $('.ui_delete_plan').click(function(e) {
        e.preventDefault();
        swal({
            title: "Sure to cancel this plan?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#1c84c6",
            confirmButtonText: "Yes, go ahead!",
            cancelButtonText: "No, keep it",
            closeOnConfirm: true
        }, function () {
            var country = $('input[name="plan[country]"]').val();
            var category = $('input[name="plan[category]"]').val();
            window.location = '/user/plans/cancel/'+country+'/'+category;
        });
    });
    $('.ui_delete_user_plan').click(function(e) {
        e.preventDefault();
        swal({
            title: "Sure to cancel this plan?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#1c84c6",
            confirmButtonText: "Yes, go ahead!",
            cancelButtonText: "No, keep it",
            closeOnConfirm: true
        });
    });

    // authorization
    $('#ui-auth-type-container-pw').hide();
    $('#ui-auth-type-container-ip').hide();
    $('.show_auth_type').hide();

    // custom step wizard
    $(document).on('click', '.goToStep',function() {

        $('#plan_exists_notice').hide();
    });

    //$('.ui_auth_type[data-visible="true"]').show(); // once both types are editable
    $('.show_auth_type[data-visible="true"]').show(); // until both types are editable
    $('.ui_add_auth_ip').hide();
    $('.ui_add_auth_ip:last').show();

    // pending state
    if($(document).find('form').attr('name') == 'pendingAuth') {
        $('.ui_delete_checkbox').hide();
        if($(document).find('#has_rotate_proxy').length) {
            $('input[name="auth_type"][value="ip"]').iCheck('check');
            $('input[name="auth_type"][value="pw"]').iCheck('disable');
            $('#ui-auth-type-container-ip').show();
        }
    }
    // posted state
    if($('input[name="auth_type"][value="ip"]:checked').length>0) {
        $('#ui-auth-type-container-pw').hide();
        $('#ui-auth-type-container-ip').show();
    };
    if($('input[name="auth_type"][value="pw"]:checked').length>0) {
        $('#ui-auth-type-container-ip').hide();
        $('#ui-auth-type-container-pw').show();
    };
    // any state
    $('input[name="auth_type"]').on('ifChecked', function(event){

        var choice = $(this).val();

        $('#ui-auth-type-container-pw').hide();
        $('#ui-auth-type-container-ip').hide();

        $('#ui-auth-type-container-'+choice).show();

    });
    $('#show_auth_pass')
        .mousedown(function(){
            $('#auth_pass').attr('type','text');
        }).mouseup(function(){
            $('#auth_pass').attr('type','password');
        }).mouseout(function(){
            $('#auth_pass').attr('type','password');
        });

    $.validator.addMethod('IP4Checker', function(value) {
        var ip = "^(?:(?:25[0-5]2[0-4][0-9][01]?[0-9][0-9]?)\.){3}" +
            "(?:25[0-5]2[0-4][0-9][01]?[0-9][0-9]?)$";
        return value.match(ip);
    }, 'Invalid IP address');

    $.validator.addClassRules({
        ui_add_auth_ip: {
            required:true,
            IP4Checker: true
        }
    });

    $('.ui_add_auth_ip').on('click',function(){

        var lastAuthIpRowElement = $('#ui_auth_ip_table tr:last-child');

        // check if current row input field is empty and return if so
        lastAuthIpRowElement.find('.form-group').removeClass('has-error');
/*
        if(lastAuthIpRowElement.find('input').val()=='') {
            lastAuthIpRowElement.find('.form-group').addClass('has-error');
            return;
        }*/

        // clone previous row
        var clonedPreviousRow = lastAuthIpRowElement.clone(true);
        var nextIndex = parseInt(lastAuthIpRowElement.attr('data-index'))+1;

        // make delete button visible in last row
        lastAuthIpRowElement.find('.ui_add_auth_ip').hide();

        // set index of newly created row
        clonedPreviousRow.attr('data-index',nextIndex);
        clonedPreviousRow.find('input').attr('name','auth_ips['+nextIndex+']');

        // empty value
        clonedPreviousRow.find('input').val('');

        // append row to ip list
        clonedPreviousRow.appendTo('#ui_auth_ip_table');

    });
    $(document).on('click','#doCheckout',function() {

        $('.sub-step').hide();
        $('#doCheckout').hide();

        $('#final_loader').fadeIn();

    });
    var bulk_ip_collection = [];
    $('.bulk_replace_action').hide();

    $('.bulk_replace_ip').on('ifClicked',function(){
        var ip_address = $(this).val();
        if(contains.call(bulk_ip_collection, ip_address) !== true)
        bulk_ip_collection.push(ip_address);
        $('.bulk_replace_action[data-country="'+$(this).attr('data-country')+'"][data-category="'+$(this).attr('data-category')+'"]').show();
    });
    $('.bulk_replace_action').on('click',function(){
        $('textarea[name="bulk_replace"]').val(bulk_ip_collection.join("\n"));
        $('.bulk_replace_action').hide();
    });

    // Minimalize menu
    $('.navbar-minimalize').click(function () {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();

    });

    $('.location_save').attr('disabled','disabled');
    $(document).find('.poll-user-location').each(function(){

        var country = $(this).attr('data-country');
        var category = $(this).attr('data-category');

        checkUserLocations(country,category);

    });

    // checks for specific plan category locations
    function checkUserLocations(country,category) {

        setTimeout(
            function(){
                $.ajax({
                    url: '/api/locations',
                    dataType: 'json',
                    success: function(data, status) {
                        if(data[country][category] == 0) {
                            console.log(country,category);
                            return checkUserLocations(country,category);
                        } else {
                            window.location = '/user/dashboard';
                        }
                    },
                    beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + Cookies.get('auth') ); }
                });

            },
            15000
        );

    }
    function SmoothlyMenu() {
        if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
            // Hide menu in order to smoothly turn on when maximize menu
            $('#side-menu').hide();
            // For smoothly turn on menu
            setTimeout(
                function () {
                    $('#side-menu').fadeIn(400);
                }, 200);
        } else if ($('body').hasClass('fixed-sidebar')) {
            $('#side-menu').hide();
            setTimeout(
                function () {
                    $('#side-menu').fadeIn(400);
                }, 100);
        } else {
            // Remove all inline style from jquery fadeIn function to reset menu state
            $('#side-menu').removeAttr('style');
        }
    }

    $('.dataTable-dashboard').DataTable({
        pageLength: 10,
    });

    new Clipboard('.ui_copyPasteButton');

    // See: http://stackoverflow.com/questions/1181575/determine-whether-an-array-contains-a-value
    var contains = function(needle) {
        // Per spec, the way to identify NaN is that it is not equal to itself
        var findNaN = needle !== needle;
        var indexOf;

        if(!findNaN && typeof Array.prototype.indexOf === 'function') {
            indexOf = Array.prototype.indexOf;
        } else {
            indexOf = function(needle) {
                var i = -1, index = -1;

                for(i = 0; i < this.length; i++) {
                    var item = this[i];

                    if((findNaN && item !== item) || item === needle) {
                        index = i;
                        break;
                    }
                }

                return index;
            };
        }

        return indexOf.call(this, needle) > -1;
    };
});
