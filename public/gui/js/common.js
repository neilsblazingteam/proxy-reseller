$(document).ready(function () {

    var selector_checkout_btn = '#checkout_button',
        selector_left2assign = '#left2assign',
        selector_input_plan_country = 'input[name="plan[country]"]',
        selector_input_plan_category = 'input[name="plan[category]"]',
        selector_input_plan_count = 'input[name="plan[count]"]',
        selector_wizard_sub_step = '#sub-step-error';

    // set posted data
    var country = getCountry().name;
    var category = getCategory();
    var totalAssignable = getTotalAssignable(country,category);
    var left2assign = calcAssignmentsLeft(totalAssignable,country,category);
    var input_username_selector = '#username';
    var input_email_selector = '#email';
    var endpoint_check = $('#app_url').attr('data-value')+'/check';

    hideAssignMessages();

    // initially hidden elements
    $('#final_loader').hide();
    $(selector_checkout_btn).hide();
    $('.plan-box-alert').hide();
    $('.info-semi-dedicated').hide();
    $('.info-dedicated').hide();
    $('.info-rotating').hide();
    $('.btn_location_available').hide();
    $('#username_available').hide();
    $('#username_exists').hide();
    $('#email_exists').hide();
    $('.locations_country').hide();
    $('#plan_exists_notice').hide();

    function hideAssignMessages() {

        $('.less').hide();
        $('.more').hide();
        $('.good').hide();
        $('.general_less').hide();
        $('.general_more').hide();
        $('.general_good').hide();
    }

    function getCategoryLabel(category) {
        var labels = {
            'semi-3':'Semi Dedicated',
            'dedicated':'Dedicated',
            'rotate':'Rotating'
        };
        var metaCategories = getGlobalVar('locations_opts');
        if (metaCategories) {
            for (var i in metaCategories) {
                if (metaCategories.hasOwnProperty(i)) {
                    var metaCategory = metaCategories[i];
                    labels[i] = metaCategory.title;
                }
            }
        }
        return labels[category];
    }
    function initCheckboxes() {
        // pretty checkboxes
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    }
    initCheckboxes();
    // set default stuff
    $('.register-category-button[data-category="'+category+'"]').addClass('active');
    if(left2assign >= 0)
    $(selector_left2assign).text(left2assign);
    $('button[aria-labelledby="ddCountryList"]').html($('.register-country-button[data-country="'+country+'"]').html());
    checkIfLocationsAssigned();

    // init register steps
    $("#register").steps({
        bodyTag: "fieldset",
        enableCancelButton: false,
        enableFinishButton: false,
        startIndex: parseInt($('#validation_step').attr('data-step')),
        onStepChanging: function (event, currentIndex, newIndex)
        {

            $('#final_loader').hide();
            $(selector_wizard_sub_step).hide();

            var form = $(this);

            // Clean up if user went backward before
            if (currentIndex < newIndex)
            {
                // To remove error styles
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
            }

            if (newIndex === 1)
            {

                if($('#plan_country').val()=='') {
                    $(selector_wizard_sub_step).text('You must choose a country first').fadeIn();
                    return false;
                }
                if($('#plan_category').val()=='') {
                    $(selector_wizard_sub_step).text('You must choose a category now').fadeIn();
                    return false;
                }
                if($('#proxy_calculator').val()=='') {
                    $(selector_wizard_sub_step).text('You must choose at least one proxy').fadeIn();
                    return false;
                }

            }
            if (newIndex === 2) {

                $('a[href="#next"]').parent().removeClass('disabled');

                if ($('#username_exists').attr('data-switch')=='true')
                {
                    $(selector_wizard_sub_step).text('Username is already taken').fadeIn();
                    return false;
                }
                if ($('#email_exists').attr('data-switch')=='true')
                {
                    $(selector_wizard_sub_step).text('Account with email '+$('#email').val()).fadeIn();
                    return false;
                }

            }

            // Disable validation on fields that are disabled or hidden.
            form.validate().settings.ignore = ":disabled,:hidden";

            // Start validation; Prevent going forward if false
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {
            var country = getCountry();
            var category = $('input[name="plan[category]"]').val();

            if(currentIndex === 1) {


                $('.locations_country').hide();

                if (!country.options.strictToLocations || !country.options.strictToLocations.length) {
                    $('.locations_country[data-country="'+country.value+'"][data-category="'+category+'"]').show();
                }
                else {
                    for (var i = 0; i < country.options.strictToLocations.length; i ++) {
                        var location = country.options.strictToLocations[i];
                        $('.locations_country[data-country="' + country.value + '"][data-category="' + category + '"]' +
                            '[data-location="' + location + '"]').show();
                    }
                }

                if(category=='rotate') {
                    $('.ui_locations[data-country="'+country.value+'"][data-category="'+category+'"][data-location="Mixed"]').val($(selector_input_plan_count).val());
                }

                $('.ui_locations').on("keyup change",function() {

                    if(!checkIfLocationsAssigned()) {
                        $(selector_wizard_sub_step).text('You can assign exactly '+$(selector_input_plan_count).val() +' proxies').fadeIn();
                    }

                });

                // Category over countries
                if (getCountry().isCategory) {
                    if (getCountry().options.defaultLocation) {
                        // Skip this step
                        var self = $(this);
                        if (priorIndex < currentIndex) {
                            //$(this).steps().next();
                            setTimeout(function() {
                                self.find('a[href=#next]').click();
                            }, 0);
                        }
                        if (priorIndex > currentIndex) {
                            //$(this).steps().previous();
                            setTimeout(function() {
                                self.find('a[href=#previous]').click();
                            }, 0);
                        }
                    }
                }
            }
            if (currentIndex === 2)
            {
                $(document).on('click','#doCheckout',function() {

                    checkIfLocationsAssigned();

                    $(this).hide();
                    $('#final_loader').fadeIn();

                    var form = $('#register');

                    form.submit();

                });
            }

            if(currentIndex === 3) {

                // terms and conditions
                $('#acceptTerms').on('ifChecked', function(e){
                    $('#terms_and_conditions').hide();
                    $(selector_checkout_btn).show();
                });
                initCheckboxes();
            }

            // Disable in UI step with locations for metacategory
            if (currentIndex > 0) {
                if (getCountry().isCategory) {
                    $('#register').find('li[role=tab]').eq(1).removeClass('done').addClass('disabled');

                }
            }
        },
        onFinishing: function (event, currentIndex)
        {
            var form = $(this);

            // Disable validation on fields that are disabled.
            // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
            form.validate().settings.ignore = ":disabled";

            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            var form = $(this);

            // Submit form input
            form.submit();
        }
    }).validate({
        errorPlacement: function (error, element)
        {
            element.before(error);
        },
        rules : {
            password : {
                minlength : 8
            },
            password_confirm : {
                minlength : 8,
                equalTo : "#password"
            }
        }
    });

    function setCountry(country) {
        $(selector_input_plan_country).data('data-country', country).val(getCountryInfo(country).value);

        // Category over countries
        if (getCountry().isCategory) {
            setCategory(country);
        }

        return getCountry();
    }

    function getCountry() {
        var opts = getGlobalVar('locations_opts');

        return getCountryInfo(
            ($(selector_input_plan_country).data() && $(selector_input_plan_country).data().dataCountry) ?
                $(selector_input_plan_country).data().dataCountry :
                ($(selector_input_plan_category).val() && opts[$(selector_input_plan_category).val()]) ?
                    $(selector_input_plan_category).val() :
                    $(selector_input_plan_country).val() ?
                        $(selector_input_plan_country).val() :
                        null
        );
    }

    function getCountryInfo(country) {
        var ret = {
            name: country,
            value: country,
            isCategory: false,
            options: {}
        };

        var opts = getGlobalVar('locations_opts');
        // Is it option?
        if (opts[country]) {
            ret.value = opts[country].country;
            ret.isCategory = true;
            ret.options = opts[country];
        }

        return ret;
    }

    function setCategory(category) {
        $(selector_input_plan_category).val(category);
    }

    function getCategory() {
		return getCountry().isCategory ?
            // Category over country
            getCountry().name :
            // Usual way
            $(selector_input_plan_category).val();
    }

    // plan and category filters for plan tables
    $('.form-plan-country-selector').click(function(){

        var selector = $(this).parents('ul').attr('aria-labelledby');
        $('#'+selector).html($(this).html());
        var currentCountry = $(this).attr('data-value');

        $(selector_input_plan_country).val(currentCountry);

    });
    $('.form-plan-category-selector').click(function(){

        var selector = $(this).parents('ul').attr('aria-labelledby');
        $('#'+selector).html($(this).html());
        var currentCategory = $(this).attr('data-value');

        setCategory(currentCategory);
    });

    // custom step wizard
    $(document).on('click', '.goToStep',function(){

        var step2goto = $(this).attr('data-step');

        $('#plan_exists_notice').hide();

        if(step2goto == 3) {
            calcPrice();
        }

        $('.sub-step').hide();
        $('#sub-step-'+step2goto).fadeIn();

    });

    // wizard step 2 specific
    $('.toggle-info-semi-dedicated').click(function(){
        $('.info-semi-dedicated').toggle();
    });
    $('.toggle-info-dedicated').click(function(){
        $('.info-dedicated').toggle();
    });
    $('.toggle-info-rotating').click(function(){
        $('.info-rotating').toggle();
    });


    // 1st step (country)
    $(document).on('click', '.register-country-button', function(){

        var country = setCountry($(this).attr('data-country'));

        $('#plan_exists_notice').hide();
        if (country.isCategory) {
            if($('.existing_plans[data-country="'+country.value+'"][data-category="'+getCategory()+'"]').length) {
                $('#change_plan_link').attr('href',$('#change_plan_link').attr('data-url')+'/'+country.value+'/'+getCategory());
                $('#plan_exists_notice').fadeIn();
                return false;
            }
        }

        $(selector_wizard_sub_step).hide();

        $('button[aria-labelledby="ddCountryList"]').html($(this).html());


        $('#proxy_calculator').val('');
        $('#proxy_calculator_result').hide();

        // switch to next sub step
        $('#sub-step-1').hide();
        // country is selected, choose category

        if (!country.isCategory) {
            $('#sub-step-2').fadeIn();
            $('#sub-step-3').find('.goToStep').attr('data-step', 2);

            // restrict some categories
            $('.button-category').show();
            if (getGlobalVar('allowed_country_categories', {})[country.name]) {
                $('.button-category').hide();

                var allowedCategories = getGlobalVar('allowed_country_categories')[country.name];
                for (var i = 0; i < allowedCategories.length; i++) {
                    $('.button-category-' + allowedCategories[i]).show();
                }
            }
        }
        // country is preselected, category is selected, choose details
        else {
            $('.chosenCategory').text(getCategoryLabel(getCategory()));

            $('#proxy_calculator').val('');
            $('#proxy_calculator_result').hide();

            // switch to next sub step
            $('#sub-step-3').show();
            $('#sub-step-3').find('.goToStep').attr('data-step', 1);

            if (!country.options.strictToLocations || !country.options.strictToLocations.length) {
                 $('#sub-step-3').find('#btn_assign_locations').hide();
                 $('#sub-step-3').find('#doCheckout').show();
            }
            else {
                $('#sub-step-3').find('#btn_assign_locations').hide();
                $('#sub-step-3').find('#doCheckout').hide();
            }
        }

        var newSrc = $('.chosenCountryImage').attr('data-imagepath')+ country.name +'.png';
        $('.chosenCountryImage').attr('src',newSrc);
    });

    // set category names from buttons for labelling
    var categories = {};
    $('.register-category-button').each(function(){
        categories[$(this).attr('data-category')] = $(this).text();
    });

    // 2nd step (category)
    $(document).on('click', '.register-category-button', function(){

        var country = getCountry().name;
        var category = $(this).attr('data-category');

        // reset buttons
        $('.register-category-button').each(function(){
            $(this).html(categories[$(this).attr('data-category')]).removeClass('active');
        });

        $('#plan_exists_notice').hide();
        if($('.existing_plans[data-country="'+country+'"][data-category="'+category+'"]').length) {
            $('#change_plan_link').attr('href',$('#change_plan_link').attr('data-url')+'/'+country+'/'+category);
            $('#plan_exists_notice').fadeIn();
            return false;
        }
        var btnHtml = categories[category];
        $('.register-category-button.active').html('<i class="fa fa-check"></i> '+btnHtml);
        $(this).addClass('active');
        $('.register-category-button.active').html('<i class="fa fa-check"></i> '+btnHtml);
        $(selector_wizard_sub_step).hide();
        setCategory(category);
        var newSrc = $('.chosenCountryImage').attr('data-imagepath')+country+'.png';
        $('.chosenCountryImage').attr('src',newSrc);
        $('.chosenCategory').text(getCategoryLabel(category));
        $('#proxy_calculator').val('');
        $('#proxy_calculator_result').hide();

        // switch to next sub step
        $('#sub-step-2').hide();
        $('#sub-step-3').fadeIn();

        // Doesn't work
        //$('.goToStep[data-step="1"]').show();
        //$('.goToStep[data-step="3"]').show();

        $('#sub-step-3').find('#btn_assign_locations').hide();
        $('#sub-step-3').find('#doCheckout').hide();
    });

    // terms and conditions
    $('#acceptTerms').on('ifChecked', function(e){
        $('#terms_and_conditions').hide();
        $('.checkout_button').show();
    });
    $('#fill_sample').click(function() {
        $('input[name="username"]').val('test');
        $('input[name="email"]').val('test@testerf.com');
        $('input[name="password"]').val('testpass');
        $('input[name="password_confirm"]').val('testpass');
        $('input[name="first_name"]').val('test');
        $('input[name="last_name"]').val('test');
    });
    // proxy pricing calculator
    function getTierFromProxyCount(proxy_count) {

        var plan_tiers = $.parseJSON($("#plan_tiers").html());
        var tier = 0;

        $.each(plan_tiers, function(index, value) {
            if(proxy_count <= value[1]) {
                tier = index;
                return false;
            }
        });
        return tier;
    }
    function calcPrice(){
        var proxy_count = parseInt($('#proxy_calculator').val());
        var plan_prices = $.parseJSON($("#plan_prices").html());
        console.log(plan_prices);
        var country = getCountry();
        var category = getCategory();
        var tier = getTierFromProxyCount(proxy_count);
        var price = plan_prices[country.value][category]['tier_'+tier]*proxy_count;
        var proxyLabel = proxy_count > 1 ? 'Proxies' : 'Proxy';
        $('#proxy_calculator_result').fadeIn();
        $('#proxy_calculator_amount').text($.number( price, 2 ));
        $('#proxy_calculator_count').text(proxy_count);
        $('#proxy_calculator_label').text(proxyLabel);
        $('#proxy_old_price').hide();
        $('#total_proxies_assignable').attr('data-count',proxy_count);
        $('#left2assign').text(proxy_count);

        //$('input[name="locations['+country+'][Mixed]['+category+']"]').val(proxy_count);
        //$('#proxies2assign').attr('data-count',proxy_count);
        $('.ui_total_proxies_assignable[data-country="'+country.value+'"][data-category="'+category+'"]').attr('data-count',proxy_count)
        $('.btn_location_available').each(function() {
            if(proxy_count > $(this).attr('data-available')) {
                $(this).show();
            }
        });

        if(category=='rotate') {
            $('.ui_locations[data-country="'+country.value+'"][data-category="'+category+'"][data-location="Mixed"]').val($(selector_input_plan_count).val());
            $('#doCheckout').show();
            $('.ui_plan_save_button').removeAttr('disabled');
        }

        /*
         var oldProxyCount = $('.ui_old_plan_count').text();
        if(proxy_count > oldProxyCount) {
            $('.ui_plan_save_button').html('<i class="fa fa-arrow-circle-up"></i> Upgrade').show();
        }
        if(proxy_count < oldProxyCount) {
            $('.ui_plan_save_button').html('<i class="fa fa-arrow-circle-down"></i> Downgrade').show();
        }
        if(proxy_count == oldProxyCount) {
            $('.ui_plan_save_button').hide();
        }*/
        if (!getCountry().isCategory ||
            (getCountry().options.strictToLocations && getCountry().options.strictToLocations.length)) {
            $('#btn_assign_locations').show();
            $('#sub-step-3').find('#doCheckout').hide();
        }
    };

    $('#btn_assign_locations').hide();

    $('#proxy_calculator').on('input', function(){

        var value = parseInt($(this).val());
        $(selector_wizard_sub_step).hide();
        $('#proxy_calculator_result').hide();
        if(isNaN(value)) {
            $(this).val('');
            $(selector_wizard_sub_step).text('You must type a number').fadeIn();
            return false;
        } else if(value==0) {
            $(selector_wizard_sub_step).text('You cannot have zero proxies').fadeIn();
            return false;
        }
        calcPrice();
    });
    $(document).on('touchspin.on.stopdownspin',function(){
        calcPrice();
    });
    $(document).on('touchspin.on.stopupspin',function(){
        calcPrice();
    });

    $('.ui_plan_save_button').attr('disabled','disabled');
    // location preferences
    $('.ui_locations').on("keyup change",function(){

        var currentValue = parseInt($(this).val());
        var country = $(this).attr('data-country');
        var category = $(this).attr('data-category');
        var count = $('input[name="plan[count]"]').val();
        var totalAssignable = getTotalAssignable(country,category);
        var left2assign = calcAssignmentsLeft(totalAssignable,country,category);

        // check if value is not below zero
        if(currentValue<0) {
            $(this).val(0);
            return false;
        }

        // check if input is numeric
        if(isNaN(currentValue)) {
            $(this).val('');
            $(selector_wizard_sub_step).text('You must type a number').fadeIn();
            return false;
        } else {
            $(selector_wizard_sub_step).hide();
        }

        $('#doCheckout').show().attr("disabled",'disabled');
        $('.ui_plan_save_button').attr('disabled','disabled');

        $('.location_category.'+country+'.'+category).text(left2assign);
        $('#left2assign').text(left2assign);
        $('.default_assigned_label.'+country+'.'+category).hide();

        $('.general_good').hide();
        $('.general_more').hide();
        $('.general_less').hide();

        if(left2assign < 0) {
            $(selector_checkout_btn).hide();
            $('a[href="#next"]').parent().addClass('disabled');
            $(selector_left2assign).text(0);
            $('.location_params_notice[data-country="'+country+'"][data-category="'+category+'"]').hide();
            $('.less[data-country="'+country+'"][data-category="'+category+'"]').show();
            $('.general_less').show();
            $('.location_save[data-country="'+country+'"][data-category="'+category+'"]').attr('disabled','disabled');
        }
        if(left2assign > 0) {
            $(selector_checkout_btn).hide();
            $('a[href="#next"]').parent().addClass('disabled');
            $(selector_left2assign).text(left2assign);
            $('.location_params_notice[data-country="'+country+'"][data-category="'+category+'"]').hide();
            $('.more[data-country="'+country+'"][data-category="'+category+'"]').show();
            $('.general_more').show();
            $('.location_save[data-country="'+country+'"][data-category="'+category+'"]').attr('disabled','disabled');
        }
        if(left2assign == 0) {
            $('a[href="#next"]').parent().removeClass('disabled');
            $(selector_left2assign).text(0);
            $('.general_good').show();
            $('.location_params_notice[data-country="'+country+'"][data-category="'+category+'"]').hide();
            $('.location_save[data-country="'+country+'"][data-category="'+category+'"]').removeAttr('disabled');
            $('.good[data-country="'+country+'"][data-category="'+category+'"]').show();
            $('.goToStep[data-step="2"]').show();
            $('#doCheckout').prop("disabled", false);

            var oldProxyCount = $('.ui_old_plan_count').text();
            if(count > oldProxyCount) {
                $('.ui_plan_save_button').removeAttr('disabled');
            }
            if(count < oldProxyCount) {
                $('.ui_plan_save_button').removeAttr('disabled');
            }
        }

    });
    function getTotalAssignable(country,category) {

        var totalAssignable = 0;

        if($('input[name="country"]').length && $('input[name="category"]').length) {
            totalAssignable = parseInt($('.ui_total_proxies_assignable[data-country="'+country+'"][data-category="'+category+'"]').attr('data-count'));
        } else {
            totalAssignable = parseInt($('#total_proxies_assignable').attr('data-count'));
        }

        return totalAssignable;

    }
    function calcAssignmentsLeft(totalAssignable,country,category) {

        var totalAssigned = 0;

        $('.ui_locations[data-country="'+country+'"][data-category="'+category+'"]').each(function() {
            var num = parseInt(this.value, 10);
            if (!isNaN(num)) {
                totalAssigned += num;
            }
        });

        return totalAssignable - totalAssigned;

    }

    // Step 4 - choose locations
    $(document).on('click', '#btn_assign_locations',function(){
        var country = getCountry();
        var category = getCategory();
        $('#sub-step-1').hide();
        $('#sub-step-3').hide();
        $('#sub-step-4').fadeIn();
        $('.locations_country').hide();

        // Strict locations if needed
        if (!country.options.strictToLocations || !country.options.strictToLocations.length) {
            $('.locations_country[data-country="' + country.value + '"][data-category="' + category + '"]').show();
        }
        else {
            for (var i = 0; i < country.options.strictToLocations.length; i ++) {
                var location = country.options.strictToLocations[i];
                $('.locations_country[data-country="' + country.value + '"][data-category="' + category + '"]' +
                    '[data-location="' + location + '"]').show();
            }
        }
    });

    // set selections if form is posted
    var formCurrentPlanCountry = getCountry().name;
    if(formCurrentPlanCountry!='') {
        var selector = $('.form-plan-country-selector').parents('ul').attr('aria-labelledby');
        $('#'+selector).html($('.form-plan-country-selector[data-value="'+formCurrentPlanCountry+'"]').html());
    }
    var formCurrentPlanCategory = getCategory();
    if(formCurrentPlanCategory!='') {
        var selector = $('.form-plan-category-selector').parents('ul').attr('aria-labelledby');
        $('#'+selector).html($('.form-plan-category-selector[data-value="'+formCurrentPlanCategory+'"]').html());
    }
    if(formCurrentPlanCountry!='' && formCurrentPlanCategory!='') {
        $('.locations_country[data-country="'+formCurrentPlanCountry+'"][data-category="'+formCurrentPlanCategory+'"]').show();
    }
    $(selector_checkout_btn).hide();

    /**
     * Registration process
     */
    // initially show first step
    $('#sub-step-1').show();

    function checkIfLocationsAssigned() {

        hideAssignMessages();
        var country = getCountry().value;
        var category = getCategory();

        var totalAssigned = 0;
        $('.ui_locations[data-country="'+country+'"][data-category="'+category+'"]').each(function() {
            var num = parseInt(this.value, 10);
            if (!isNaN(num)) {
                totalAssigned += num;
            }
        });

        var totalAssignable = parseInt($('#total_proxies_assignable').attr('data-count'));
        var left2assign = totalAssignable - totalAssigned;
        $('#left2assign').text(left2assign);

        if(totalAssigned > totalAssignable) {
            $('.general_less').show();
            $('a[href="#next"]').parent().addClass('disabled');
            return false;
        }
        if(left2assign > 0) {
            $('a[href="#next"]').parent().addClass('disabled');
            $('.general_more').show();
            return false;
        }
        if(left2assign == 0) {
            $('a[href="#next"]').parent().removeClass('disabled');
            $('.general_good').show();
            return true;
        }

    }
    // check username function
    function checkUsernameExists() {

        var username = $(input_username_selector).val();

        if(username && username != '') {

        var endpoint = endpoint_check+'/username/'+username;

        $('.username_exists').hide();

        $.ajax(endpoint, {
            success: function(data) {
                if(data.success) {
                    $('.username_exists').hide();
                    if($('#email_exists').attr('data-switch')=='false')
                        $('a[href="#next"]').parent().removeClass('disabled');
                } else {
                    $('.username_exists').attr('data-switch','true').show();
                }
            }
        });

        }
    }
    // check email function
    function checkEmailExists() {

        var email = $(input_email_selector).val();

        if(email && email != '') {
        var endpoint = endpoint_check+'/email/'+$(input_email_selector).val();

        $('#email_exists').hide();

        $.ajax(endpoint, {
            success: function(data) {
                if(!data.success) {
                    $('.email_exists').show();
                    if($('#username_exists').attr('data-switch')=='false')
                        $('a[href="#next"]').parent().removeClass('disabled');
                } else {
                    $('.email_exists').hide();
                }
            }
        });
        }

    }
    // check username action
    $(input_username_selector).bind('keyup',function(event) {

        delayedCallback(function(){
            checkUsernameExists();
        }, 250 );

    });
    // check email action
    $(input_email_selector).bind('keyup',function(event) {

        delayedCallback(function(){
            checkEmailExists();
        }, 250 );

    });
    // form validation
    $("#passwordResetForm").validate({
        rules: {
            password_confirm : {
                minlength : 8,
                equalTo : "#password"
            }
        }
    });
    $("#passwordRequestForm").validate({
        rules: {
            email : {
                required: true
            }
        }
    });

    // theme specific
    // tooltips
    $('.tooltip-container').tooltip({
        selector: '[data-toggle="tooltip"]'
    });
    // number input
    $(".touchspin").TouchSpin({
        buttondown_class: 'btn btn-white proxy_decrease',
        buttonup_class: 'btn btn-white proxy_increase',
        min : 0,
        max : 100000
    });
    // scrollable dropdown
    $('.scrollable-menu').slimScroll({
        height: '200px'
    });
    // scrollable content
    $('.scroll_content').slimscroll({
        height: '200px',
        alwaysVisible: true
    })
    $('#form_date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

});
// helper

var delayedCallback = (function(){
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();

function getGlobalVar(key, def) {
    if (undefined === def) {
        def = false;
    }

    // Initialize cache
    if (undefined === this.$data) {
        this.$data = {};
    }

    // Use cache if possible
    if (undefined !== this.$data[key]) {
        return this.$data[key];
    }

    // Return default value, if key not found
    var container = $("#" + key);
    if (!container.length) {
        return def;
    }

    return this.$data[key] = $.parseJSON(container.html());
}