<h3>System Requirements</h3>
<p><i class="fa fa-check color-positive text-success"></i> Database has been set up</p>
<h4>Software Versions</h4>
<form class="m-t" role="form" name="step2" method="POST" action="?step=2">
    <ul class="list-unstyled">
        <li><?php echo getCheckCross($isValidPhpVersion); ?> PHP <?php echo $minimumRequirements['php']; ?></li>
        <li><?php echo getCheckCross($isValidMysqlVersion); ?> MySQL <?php echo $minimumRequirements['mysql']; ?></li>
    </ul>

    <h4>PHP Extensions</h4>
    <ul class="list-unstyled">
        <?php
        foreach (getRequiredPhpExtensions() as $key => $extension) {
            echo '<li>' . getCheckCross(checkExtension($key)) . ' ' . $extension . '</li>';
        }
        ?>
    </ul>
    <?php
    if(empty($error)) :
        ?>
    <p>
        <a href="<?php echo getInstallUrl() . '?step=3'; ?>" class="btn btn-primary block full-width m-b">Go to Step
            3</a>
    </p>
    <?php
    endif;
    ?>


</form>