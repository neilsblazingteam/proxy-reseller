<h3>Configure your app</h3>
<form class="m-t" role="form" method="POST" action="?step=3" enctype="multipart/form-data">
    <p>
        Enter Blazing Proxies credentials
    </p>
    <div class="form-group">
        <label>API Key</label>
        <input class="form-control" type="text" name="bp_api_key" value="<?php echo $fieldValues['bp_api_key'];?>" required>
        <?php if(!empty($fieldErrors['bp_api_key'])) echo '<p class="text-danger">'.$fieldErrors['bp_api_key'].'</p>';?>
    </div>
    <p>
        Personalize your app
    </p>
    <div class="form-group">
        <label>Admin Username</label>
        <input class="form-control" type="text" name="bp_user" value="<?php echo $fieldValues['bp_user'];?>" required>
        <p class="help-block">You will use this username as a login to your reseller admin panel.</p>
        <?php if(!empty($fieldErrors['bp_user'])) echo '<p class="text-danger">'.$fieldErrors['bp_user'].'</p>';?>
    </div>
    <div class="form-group">
        <label>First name</label>
        <input class="form-control" type="text" name="app_first_name" value="<?php echo $fieldValues['app_first_name'];?>" required>
        <?php if(!empty($fieldErrors['app_first_name'])) echo '<p class="text-danger">'.$fieldErrors['app_first_name'].'</p>';?>
    </div>
    <div class="form-group">
        <label>Last name</label>
        <input class="form-control" type="text" name="app_last_name" value="<?php echo $fieldValues['app_last_name'];?>" required>
        <?php if(!empty($fieldErrors['app_last_name'])) echo '<p class="text-danger">'.$fieldErrors['app_last_name'].'</p>';?>
    </div>
    <div class="form-group">
        <label>Email</label>
        <input class="form-control" type="app_email" name="app_email" value="<?php echo $fieldValues['app_email'];?>" required>
        <?php if(!empty($fieldErrors['app_email'])) echo '<p class="text-danger">'.$fieldErrors['app_email'].'</p>';?>
    </div>
    <div class="form-group">
        <label>Password</label>
        <input class="form-control" type="password" name="app_pass" value="" required>
        <?php if(!empty($fieldErrors['app_pass'])) echo '<p class="text-danger">'.$fieldErrors['app_pass'].'</p>';?>
    </div>
    <div class="form-group">
        <label>Repeat Password</label>
        <input class="form-control" type="password" name="app_pass_confirm" value="" required>
        <?php if(!empty($fieldErrors['app_pass_confirm'])) echo '<p class="text-danger">'.$fieldErrors['app_pass_confirm'].'</p>';?>
    </div>
    <div class="form-group">
        <label>Your name or your company name</label>
        <input class="form-control" type="text" name="app_name" value="<?php echo $fieldValues['app_name'];?>" required>
        <p class="help-block">This name will be in footer as a copyright. You can change it later</p>
        <?php if(!empty($fieldErrors['app_name'])) echo '<p class="text-danger">'.$fieldErrors['app_name'].'</p>';?>
    </div>
    <div class="form-group">
        <label>Your company logo (optional)</label>
        <input class="form-control" type="file" name="app_logo" value="<?php echo $fieldValues['app_logo'];?>">
        <?php if(!empty($fieldErrors['app_logo'])) echo '<p class="text-danger">'.$fieldErrors['app_logo'].'</p>';?>
    </div>
    <p>
        <button type="submit" class="btn btn-primary block full-width m-b">Go to last step</button>
    </p>
</form>