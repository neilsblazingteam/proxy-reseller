<h3><i class="fa fa-paypal"></i> Setup Paypal</h3>
<form class="m-t" role="form" method="POST" action="?step=4">
    <h4>Mode <small>You can change this later</small></h4>
    <div class="form-group">
        <label>Paypal mode</label>
        <div>
            <div class="i-checks">
                <label>
                    <input type="radio" value="live" name="paypal[mode]" <?php if(!isset($_POST['paypal']['mode']) || $_POST['paypal']['mode'] == 'live') { echo 'checked=""'; } ?>>
                    <i></i>
                    Live
                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right"
                        title="This allows customers to start buying right away. Not recommend for first setup, see Sandbox."></i>
                </label>
            </div>
            <div class="i-checks">
                <label>
                    <input type="radio" value="sandbox" name="paypal[mode]" <?php if(isset($_POST['paypal']['mode']) && $_POST['paypal']['mode'] == 'sandbox') { echo 'checked=""'; } ?>>
                    <i></i>
                    Sandbox
                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right"
                       title="This allows you to make test purchases as a customer to make sure everything is setup and running properly. Use Live Mode to accept real payments."></i>
                </label>
            </div>
        </div>
        <?php if(!empty($fieldErrors['paypal']['mode'])) echo '<p class="text-danger">'.$fieldErrors['paypal']['mode'].'</p>';?>
    </div>

    <h3><i class="fa fa-key"></i> PayPal credentials</h3>
    <p class="help-block">
        Credentials which are will be used as an authorization data to your seller account in PayPal
        and processing money from your customers to your account. To input credentials you should
        get it from your seller application. If application is not created yet, you should follow
        steps below:
        <ol>
            <li>Go to <a href="https://developer.paypal.com/developer/applications/create">Create New App</a></li>
            <li>Login (if you are not authorized)</li>
            <li>Enter App Name</li>
            <li>Click Create App</li>
            <li>Choose live mode (in right top corner)</li>
        </ol>
        After these steps above your application is created and you can get credentials from it. To do it:
        <ol>
            <li>Go to <a href="https://developer.paypal.com/developer/applications/">your applications</a></li>
            <li>Login (if you are not authorized)</li>
            <li>Choose your application in from list</li>
            <li>
                Choose Live mode (in right top corner)
                <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right"
                   title="Or you can choose Sandbox mode, if you want to test application"></i>
            </li>
            <li>Get Client ID</li>
            <li>Get secret (by clicking on Show)</li>
        </ol>
    </p>

    <div class="paypal_mode_box <?php if(isset($_POST['paypal']['mode']) && $_POST['paypal']['mode'] == 'sandbox') { echo 'is_visible'; }?>" id="mode_sandbox">
        <div class="form-group">
            <label>Sandbox Client Id</label>
            <input type="text" class="form-control" name="paypal[credentials][sandbox][clientId]" value="<?php echo (isset($_POST['paypal']['credentials']['sandbox']['clientId'])) ? $_POST['paypal']['credentials']['sandbox']['clientId'] : '' ;?>">
            <?php if(!empty($fieldErrors['paypal']['credentials']['sandbox']['clientId'])) echo '<p class="text-danger">'.$fieldErrors['paypal']['credentials']['sandbox']['clientId'].'</p>';?>
        </div>
        <div class="form-group">
            <label>Sandbox Client Secret</label>
            <input type="text" class="form-control" name="paypal[credentials][sandbox][secret]" value="<?php echo (isset($_POST['paypal']['credentials']['sandbox']['secret'])) ? $_POST['paypal']['credentials']['sandbox']['secret'] : '' ;?>">
            <?php if(!empty($fieldErrors['paypal']['credentials']['sandbox']['secret'])) echo '<p class="text-danger">'.$fieldErrors['paypal']['credentials']['sandbox']['secret'].'</p>';?>
        </div>
    </div>

    <div class="paypal_mode_box <?php if(!isset($_POST['paypal']['mode']) || $_POST['paypal']['mode'] == 'live') { echo 'is_visible'; }?>" id="mode_live">
        <div class="form-group">
            <label>Live Client Id</label>
            <input type="text" class="form-control" name="paypal[credentials][live][clientId]" value="<?php echo (isset($_POST['paypal']['credentials']['live']['clientId'])) ? $_POST['paypal']['credentials']['live']['clientId'] : '' ;?>">
            <?php if(!empty($fieldErrors['paypal']['credentials']['live']['clientId'])) echo '<p class="text-danger">'.$fieldErrors['paypal']['credentials']['live']['clientId'].'</p>';?>
        </div>
        <div class="form-group">
            <label>Live Client Secret</label>
            <input type="text" class="form-control" name="paypal[credentials][live][secret]" value="<?php echo (isset($_POST['paypal']['credentials']['live']['secret'])) ? $_POST['paypal']['credentials']['live']['secret'] : '' ;?>">
            <?php if(!empty($fieldErrors['paypal']['credentials']['live']['secret'])) echo '<p class="text-danger">'.$fieldErrors['paypal']['credentials']['live']['secret'].'</p>';?>
        </div>
    </div>
    <div class="form-group hidden">
        <label>How do you want PayPal to send transaction states?</label>
        <div>
        <div class="i-checks"><label> <input type="radio" name="paypal_transaction" value="ipn" <?php if(true or isset($_POST['paypal_transaction']) && $_POST['paypal_transaction'] == 'ipn') { echo 'checked=""'; } ?>> IPN</label></div>
        <div class="i-checks"><label> <input type="radio" name="paypal_transaction" value="webhooks" <?php if(isset($_POST['paypal_transaction']) && $_POST['paypal_transaction'] == 'webhooks') { echo 'checked=""'; } ?>> Webhooks</label></div>
        </div>
        <?php if(!empty($fieldErrors['paypal_transaction'])) echo '<p class="text-danger">'.$fieldErrors['paypal_transaction'].'</p>';?>
    </div>
    <div id="ipn_message" class="setup_message">
        <div class="alert alert-info"><i class="fa fa-info-circle"></i> Your IPN listener URL for PayPal: <strong><?php echo $ipnUrl;?></strong></div>
    </div>
    <div id="webhooks_message" class="setup_message">
        <div class="alert alert-warning"><i class="fa fa-warning"></i> You must have SSL installed and use HTTPS to activate webhooks!</div>
        <div class="alert alert-info"><i class="fa fa-info-circle"></i> Your webhooks will be added automatically</div>
    </div>
    <button type="submit" class="btn btn-primary block full-width m-b">Finalize</button>
</form>
<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
