<?php
use MigrateComfortable\Migrator;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use ResellerApp\AppFramework;
use Symfony\Component\Yaml\Yaml;

require 'autoload.php';
require_once __DIR__ . '/../../vendor/autoload.php';
require_once 'check.php';
require_once 'helper.php';

$framework = new AppFramework();

$error = '';
$success = '';
$step = isset($_GET['step']) ? $_GET['step'] : 1;
$predefinedConfig = array_flip(explode(',', !empty($_GET['use_config']) ? $_GET['use_config'] : ''));
$predefinedConfig = array_merge(['db' => false], array_fill_keys(array_keys($predefinedConfig), true));
$pdo = 0;
$db_host = '';
$db_user = '';
$db_pass = '';
$db_name = '';
$formError = false;
$doTablesExist = false;
$isValidPhpVersion = false;
$isValidMysqlVersion = false;
$areExtensionsValid = false;
$minimumRequirements = getMinimumRequirements();
$sureToDeleteDatabase = true;
$configStoragePath = $framework->getAppPath() . '/app/config';
$dbConfigFile = $configStoragePath . '/database.php';
$appUrl = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}";
$ipnUrl = $appUrl."/ipn/paypal";
$webhookUrl = $appUrl."/webhooks/paypal";
$showNextBtn = false;

// process installation steps
if ($step == 1) {

    $error = '';
    $fieldKeys = [
        'db_host',
        'db_user',
        'db_pass',
        'db_name'
    ];
    // init form values
    $fieldValues = array_fill_keys($fieldKeys, '');
    $fieldErrors = array_fill_keys($fieldKeys, '');

    // validate form when sent
    if (isset($_POST) && !empty($_POST)) {

        foreach ($fieldValues as $key => $val) {
            $fieldValues[$key] = isset($_POST[$key]) && !empty($_POST[$key]) ? $_POST[$key] : '';
            if (!isset($_POST[$key]) || (isset($_POST[$key]) && empty($_POST[$key]))) {
                $error = 'Please correct form fields';
                $fieldErrors[$key] = 'This field is required';
            }
        }


        // check if pdo instance can be created
        if (empty($error)) {

            // store database values entered by user
            $db_host = $_POST['db_host'];
            $db_user = $_POST['db_user'];
            $db_pass = $_POST['db_pass'];
            $db_name = $_POST['db_name'];

            $pdo = checkPdoInstance($db_host, $db_user, $db_pass, $db_name);
            if (is_int($pdo)) {
                $error = getPdoError($pdo);
            }
        }

        // try to create database config file
        if (empty($error)) {
            $configFileDatabase = "<?php
return [
    'host'=>'" . $db_host . "',
    'user'=>'" . $db_user . "',
    'pass'=>'" . $db_pass . "',
    'database'=>'" . $db_name . "',
    'type'=>'mysql',
    'port'=>3301
];";

            if (!is_dir($configStoragePath)) {
                mkdir($configStoragePath, 0755);
            }
            if (!writeFile($dbConfigFile, $configFileDatabase)) {
                $error = 'Could not create database config file. Permissions denied.';
            }

        }

        $dbSetupReady = true;

    }
    // Start point for autoinstaller
    elseif (is_file($dbConfigFile)) {

        try  {
            $dbConfig = include $dbConfigFile;

            $db_host = $dbConfig['host'];
            $db_user = $dbConfig['user'];
            $db_pass = $dbConfig['pass'];
            $db_name = $dbConfig['database'];

            if (checkPdoInstance($db_host, $db_user, $db_pass, $db_name)) {
                $dbSetupReady = true;
            }
        }
        catch (\Exception $e) {
            $error = 'Error occured! Details - ' . $e->getMessage();
        }
    }

    // try to create database tables
    if (empty($error) and !empty($dbSetupReady)) {

        $pdo = getPdoInstance($db_host, $db_user, $db_pass, $db_name);
        $isValidMysqlVersion = checkVersion('mysql', $minimumRequirements['mysql'], ['db_host' => $db_host, 'db_user' => $db_user, 'db_pass' => $db_pass, 'database' => $db_name]);

        if ($isValidMysqlVersion) {

            if (empty($error)) {

                $qry = file_get_contents(__DIR__ . '/dump/db.sql');

                if (!$pdo->query($qry)) {

                    $sureToDeleteDatabase = true;

                    if (isset($sureToDeleteDatabase)) {

                        if ($pdo->query("CREATE DATABASE IF NOT EXISTS `" . $db_name . "`")) {

                            header('Location:' . getInstallUrl() . '?step=2');

                        } else {
                            $error = 'Database exists. Are you sure you want to delete the current database?';
                            $error .= ' ' . json_encode($pdo->errorInfo(), JSON_PRETTY_PRINT);
                        }

                    } else {
                        $error = 'Database exists. Are you sure you want to delete the current database?';
                    }

                } else {
                    Migrator::construct()->setOutput(false)->migrateCommand();

                    header('Location:' . getInstallUrl() . '?step=2');
                }
            }
        }
    }

    $pdo = null;
}
if ($step == 2) {

    //@todo make sure all checkpoints are clear before goint to step 3

    // check if step 1 went right, else redirect back
    if (!is_readable($dbConfigFile))
        header('Location:' . getInstallUrl() . '?step=1');

    $extensionCount = count(getRequiredPhpExtensions());

    // load database configuration
    $dbConfig = include($dbConfigFile);

    $pdo = checkPdoInstance($dbConfig['host'], $dbConfig['user'], $dbConfig['pass'], $dbConfig['database']);
    // check if pdo instance can be created
    if (is_int($pdo)) {
        $error = getPdoError($pdo);
    }

    if (empty($error)) {

        foreach (getRequiredPhpExtensions() as $key => $extension) {
            $isValidExtension[$key] = checkExtension($key) ? 1 : 0;
        }
        $areExtensionsValid = count(getRequiredPhpExtensions()) === array_sum($isValidExtension);
        // prepare version checking
        $isValidPhpVersion = checkVersion('php', $minimumRequirements['php']);

        $isValidMysqlVersion = checkVersion('mysql', $minimumRequirements['mysql'], [
            'db_host' => $dbConfig['host'],
            'db_user' => $dbConfig['user'],
            'db_pass' => $dbConfig['pass'],
            'database' => $dbConfig['database']]);

        if (!$areExtensionsValid
            || $isValidPhpVersion
            || $isValidMysqlVersion
        ) {
            if(!$isValidPhpVersion) {
                $error = 'You must meet all server requirements. <br />Please update your PHP version.';
            } else if(!$isValidMysqlVersion) {
                $error = 'You must meet all server requirements. <br />Please update your MySQL version';
            } else if(!$areExtensionsValid) {
                $error = 'You must meet all server requirements. <br />Please enable missing PHP extensions.';
            }

        }

    }
    $pdo = null;
}
if ($step == 3) {

    $error = '';
    $dbConfig = include $dbConfigFile;

    $pdo = getPdoInstance($dbConfig['host'], $dbConfig['user'], $dbConfig['pass'], $dbConfig['database']);

    $fieldKeys = [
        'bp_user',
        'bp_api_key',
        'app_first_name',
        'app_last_name',
        'app_email',
        'app_pass',
        'app_pass_confirm',
        'app_name',
        'app_company',
        'app_logo',
    ];
    $formRules = [
        'bp_user',
        'bp_api_key',
        'app_first_name',
        'app_last_name',
        'app_email',
        'app_name',
        'app_pass',
        'app_pass_confirm',
    ];
    // init form values
    $fieldValues = array_fill_keys($fieldKeys, '');
    $fieldErrors = array_fill_keys($fieldKeys, '');

    // validate form when sent
    if (isset($_POST) && !empty($_POST)) {

        foreach ($formRules as $key) {
            $fieldValues[$key] = isset($_POST[$key]) && !empty($_POST[$key]) ? $_POST[$key] : '';
            if (!isset($_POST[$key]) || (isset($_POST[$key]) && empty($_POST[$key]))) {
                $error = 'Please correct form errors';
                $formError = true;
                $fieldErrors[$key] = 'This field is required';
            }
        }

        if (empty($error)) {

            $bpUsername = $_POST['bp_user'];
            $bpKey = $_POST['bp_api_key'];

            $appName = isset($_POST['app_name']) ? $_POST['app_name'] : '';
            $appLogo = isset($_FILES['app_logo']) ? $_FILES['app_logo'] : null;
            $appPass = $_POST['app_pass'];
            $appEmail = $_POST['app_email'];
            $appFirstName = $_POST['app_first_name'];
            $appLastName = $_POST['app_last_name'];
            $appPassConfirm = $_POST['app_pass_confirm'];

            $passPhrase = password_hash($appPassConfirm, PASSWORD_DEFAULT);

            // check if passwords match
            if ($appPass != $appPassConfirm) {
                $error = 'Passwords do not match';
            }

            if (empty($error)) {
                $apiUrl = 'http://www.blazingseollc.com/reseller/api/proxy/user';
                $appConfig = getPublicPath() . '/../app/config/app.yml';
                if (is_file($appConfig)) {
                    $appConfig = Yaml::parse(file_get_contents($appConfig));

                    if (!empty($appConfig['api'])) {
                        $apiUrl = "{$appConfig['api']['protocol']}://{$appConfig['api']['host']}{$appConfig['api']['url']}";
                    }
                }

                $bpUsers = callApi('GET',"$apiUrl/user", ['api_key' =>$bpKey]);

                if (isset($bpUsers['error'])) {
                    $error = $bpUsers['message'];
                }

                if (empty($error)) {
                    $bpId = 0;

                    foreach($bpUsers as $user) {
                        if($user['username'] == $bpUsername) {
                            $bpId = $user['user_id'];
                            break;
                        }
                    }

                    if ($bpId==0) {
                        $bpUser = callApi('POST','http://www.blazingseollc.com/reseller/api/proxy/user', ['api_key'=>$bpKey,'username'=>$bpUsername]);

                        if (isset($bpUser['error'])) {
                            $error = $bpUser['message'];
                        } else {
                            $bpId = $bpUser['user_id'];
                        }

                    }
                }

            }

            if (empty($error)) {

                // check if installation complete
                $qry = "SELECT api_key FROM bp_api_credentials WHERE api_key = '" . $bpKey . "'";
                $stm = $pdo->prepare($qry);

                $stm->execute();
                $res = $stm->fetch();

                if (!isset($res['api_key'])) {
                    // add user to database
                    $qry = "INSERT INTO bp_api_credentials (api_key,created_on) VALUES ('{$bpKey}',NOW())";

                    if (!$pdo->query($qry)) {

                        $error = 'Could not store API key';

                    }
                }
            }
            if (empty($error)) {

                $imageFileType = '';

                if (empty($error) && !empty($appLogo['tmp_name'])) {

                    $target_file = getImgPath() . basename($appLogo["name"]);
                    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                    $res = uploadFile($appLogo);
                    if(true!==$res) {
                        $error = 'Logo file error: '.$res;
                    }

                }
                if (!empty($appName)) {

                    // check if installation complete
                    $qry = "SELECT name FROM bp_api_credentials WHERE name = '" . $appName . "'";
                    $stm = $pdo->prepare($qry);

                    $stm->execute();
                    $res = $stm->fetch();

                    if (!isset($res['name'])) {
                        // add user to database
                        $qry = "INSERT INTO bp_reseller (name,logo,created_on) VALUES ('{$appName}','{$imageFileType}',NOW())";

                        if (!$pdo->query($qry)) {

                            $error = 'Could not store reseller';

                        }
                    }

                }

            }

            if (empty($error)) {

                $qry = "SELECT username FROM bp_user WHERE username = '" . $bpUsername . "'";
                $stm = $pdo->prepare($qry);

                $stm->execute();
                $res = $stm->fetch();

                if (!isset($res['username'])) {
                    // add user to database
                    $qry = "INSERT INTO bp_user (bp_id,email,username,passphrase,first_name,last_name,roles,created_on) VALUES ('{$bpId}','{$appEmail}','{$bpUsername}','{$passPhrase}','{$appFirstName}','{$appLastName}','ROLE_ADMIN,ROLE_USER',NOW())";

                    if (!$pdo->query($qry)) {

                        $error = 'Could not create user: ' . implode('<br/>', $pdo->errorInfo());

                    }
                }

            }
        }

        $pdo = null;

        if(empty($error)) {
            header('Location:' . getInstallUrl() . '?step=4');
        }

    }

}
function callApi($method,$url,$fields=[]) {

    $fields_string = '';

    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string, '&');

    $ch = curl_init();

    if($method=='POST') {
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    } else {
        curl_setopt($ch,CURLOPT_URL, $url.'?'.$fields_string);
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);

    curl_close($ch);
    return json_decode($result,true);
}
if ($step == 4) {

    $formError = false;

    if (isset($_POST) && !empty($_POST)) {

        $dbConfig = include $dbConfigFile;
        $pdo = getPdoInstance($dbConfig['host'], $dbConfig['user'], $dbConfig['pass'], $dbConfig['database']);

    if(!isset($_POST['paypal']['mode'])) {
        $formError = true;
        $fieldErrors['paypal']['mode'] = 'Please choose a mode';
    } else {

        if(empty($_POST['paypal']['credentials'][$_POST['paypal']['mode']]['clientId'])) {
            $formError = true;
            $fieldErrors['paypal']['credentials'][$_POST['paypal']['mode']]['clientId'] = 'This field is required';
        }

        if(empty($_POST['paypal']['credentials'][$_POST['paypal']['mode']]['secret'])) {
            $formError = true;
            $fieldErrors['paypal']['credentials'][$_POST['paypal']['mode']]['secret'] = 'This field is required';
        }

    }

        if(!isset($_POST['paypal_transaction']) || empty($_POST['paypal_transaction'])) {
            $formError = true;
            $fieldErrors['paypal_transaction'] = 'Please choose a transaction type';
        }

        if($formError) {
            $error = 'Please correct form errors';
        }
    // try to create paypal config file
    if (empty($error)) {
        $configFile = "<?php
return [
    'credentials'=>[
        'sandbox'=>[
            'clientId'=>'" . $_POST['paypal']['credentials']['sandbox']['clientId'] . "',
            'secret'=>'" . $_POST['paypal']['credentials']['sandbox']['secret'] . "',
        ],
        'live'=>[
            'clientId'=>'" . $_POST['paypal']['credentials']['live']['clientId'] . "',
            'secret'=>'" . $_POST['paypal']['credentials']['live']['secret'] . "',
        ],
    ],
    'mode'=>'" . $_POST['paypal']['mode'] . "'
];";


        if (!is_dir($configStoragePath)) {
            mkdir($configStoragePath, 0755);
        }
        if (!writeFile($configStoragePath . '/paypal.php', $configFile)) {
            $error = 'Could not create paypal config file. Permissions denied.';
        }

        if($_POST['paypal_transaction'] == 'webhooks') {
        $webhook = new \PayPal\Api\Webhook();
        $webhook->setUrl($webhookUrl);

        $webhookEventTypes = array();
        $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
            '{
        "name":"BILLING.PLAN.CREATED"
    }'
        );
        $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
            '{
        "name":"BILLING.PLAN.UPDATED"
    }'
        );
        $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
            '{
        "name":"BILLING.SUBSCRIPTION.CREATED"
    }'
        );
        $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
            '{
        "name":"BILLING.SUBSCRIPTION.UPDATED"
    }'
        );
        $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
            '{
        "name":"BILLING.SUBSCRIPTION.SUSPENDED"
    }'
        );
        $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
            '{
        "name":"BILLING.SUBSCRIPTION.RE-ACTIVATED"
    }'
        );
        $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
            '{
        "name":"BILLING.SUBSCRIPTION.CANCELLED"
    }'
        );
        $webhook->setEventTypes($webhookEventTypes);

// ### Create Webhook
        try {

            $apiContext = new ApiContext(
                new OAuthTokenCredential(
                    $_POST['paypal_clientId'],
                    $_POST['paypal_secret']
                )
            );

            $output = $webhook->create($apiContext);

            // add user to database
            $qry = "INSERT INTO bp_paypal_webhook (webhook_id,valid_from) VALUES ('{$output->getId()}',NOW())";

            if (!$pdo->query($qry)) {

                $error = 'Could not add webhook to database';

            }

        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            $errorData = json_decode($ex->getData(),true);
            $error = 'Paypal webhook setup failed!'." <br/>";;
            $error .= 'HTTP error code '.$ex->getCode()." <br/>";
            foreach($errorData['details'] as $detail) {
                $error .= "Field: {$detail['field']} Issue: {$detail['issue']} <br/>";
            }
        } catch (Exception $ex) {
            $error = $ex->getMessage();
        }
        }
    }

    $pdo = null;
    if (empty($error)) {

        header('Location:' . getLoginUrl());
    }

    }
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Install - Blazing SEO Reseller Application</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="../gui/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../gui/css/animate.css">
    <link rel="stylesheet" href="../gui/css/style.css">
    <link rel="stylesheet" href="../gui/css/plugins/iCheck/custom.css">
    <link rel="stylesheet" href="css/style.css">

    <script src="../gui/js/jquery-2.1.1.js"></script>
    <script src="../gui/js/bootstrap.min.js"></script>
    <script src="../gui/js/plugins/iCheck/icheck.min.js"></script>
</head>
<body class="gray-bg">
<h1 class="text-center"><i class="fa fa-dropbox"></i> Reseller App Setup</h1>
<h3 class="text-center">Step <?php echo $step; ?> of 4</h3>
<div class="middle-box animated fadeInDown">
    <?php

    if (!empty($error)) {
        echo '<div class="alert alert-danger">';
        echo $error;
        echo '</div>';
    }

    include './step' . $step . '.php';

    ?>
</div>
<script type="text/javascript">

    $(document).ready(function(){

        $('.setup_message').hide();
        $('.paypal_mode_box').hide();

        $('input[name="paypal_transaction"]').on('ifChecked',function(){

            $('.setup_message').hide();

            if($(this).val() == 'ipn') {

                $('#ipn_message').show();

            }
            if($(this).val() == 'webhooks') {

                $('#webhooks_message').show();

            }

        });

        $('.paypal_mode_box.is_visible').show();

        $('input[name="paypal[mode]"]').on('ifChecked',function(){

            var value = $(this).val();

            $('.paypal_mode_box').hide();

            if(value == 'live') {

                $('#mode_live').show();

            }
            if(value == 'sandbox') {

                $('#mode_sandbox').show();

            }

        });

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });
</script>
</body>
</html>