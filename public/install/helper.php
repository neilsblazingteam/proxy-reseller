<?php
/**
 * @copyright 2016 Blazing SEO
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */

/**
 * Converts bool to icon HTML for check results
 * @param bool $bool Positive/Negative switch
 * @return string
 */
function getCheckCross($bool) {
    $icon = $bool ? 'fa fa-check color-positive text-success' : 'fa fa-close color-negative text-danger';
    return '<i class="'.$icon.'"></i>';
}

/**
 * Write content to file
 * @param string $path Path to file
 * @param string $content Content to write
 * @return bool
 */
function writeFile($path,$content) {
    return file_put_contents($path, $content);
}

/**
 * Converts error messages to user friendly ones
 * @param int $code PDO Error code
 * @return string User friendly message
 */
function getPdoError($code) {
    $error = '';
    switch($code) {
        case 1045:
            $error = 'Access denied. Make sure your credentials are correct and mysql user has proper privileges.';
            break;
        case 1049:
            $error = 'Unknown database. Please make sure you created the necessary database.';
            break;
    }
    return $error;
}

function getPublicUrl() {
    return "http://" . $_SERVER['SERVER_NAME'];
}
function getPublicPath() {
    return defined('PATH_APP') ? (PATH_APP . '/public') : __DIR__.'/..';
}
function getInstallPath() {
    return getPublicPath().'/install/';
}
function getImgPath() {
    return getPublicPath()."/gui/img/";
}
function getDashboardUrl() {
    return getPublicUrl() . '/dashboard';
}
function getInstallUrl() {
    return getPublicUrl() . '/install/';
}
function getLoginUrl() {
    return getPublicUrl()."/login";
}
function uploadFile($file) {
/*
    $error = '';

    $target_dir = getImgPath();
    $target_file = $target_dir . basename($file["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    $check = getimagesize($file["tmp_name"]);
    if($check !== false) {
        $error = "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        $error = "File is not an image.";
        $uploadOk = 0;
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        $error = "Sorry, file already exists.";
        $uploadOk = 0;
    }
    // Check file size
    if ($file["size"] > 500000) {
        $error = "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
        $error = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $error = "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        $fileDest = $target_dir.'logo.'.$imageFileType;
        if (move_uploaded_file($file["tmp_name"], $fileDest)) {
            return true;
        } else {
            $error = "Sorry, there was an error uploading your file.";
        }
    }

    return $error;*/
    if(!is_null($file)) {
        $logoFile = $file;
        $error = "";

        $target_file =
        $uploadOk = 1;
        $imageFileType = pathinfo($logoFile['name'],PATHINFO_EXTENSION);
        $fileDest = getImgPath().'/logo.'.$imageFileType;
        // Check if image file is a actual image or fake image

        $uploadOk = 1;
        // Check if file already exists
//        if (file_exists($target_file)) {
//            $error = "Sorry, file already exists.";
//            $uploadOk = 0;
//        }
        // Check file size
        if ($logoFile["size"] > 1 * 1024 * 1024) {
            $error = "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
            $error = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 1) {
            if (move_uploaded_file($logoFile["tmp_name"], $fileDest)) {
                return true;
            } else {
                $error = "Sorry, there was an error uploading your file.";
            }
        }

        if(!empty($error)) {
            return $error;
        }

    }
}