-- Tabellenstruktur für Tabelle `bp_api_credentials`
--

CREATE TABLE `bp_api_credentials` (
  `id` int(11) NOT NULL,
  `api_key` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bp_messages`
--

CREATE TABLE `bp_messages` (
  `id` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_on` datetime NOT NULL,
  `is_read_receiver` tinyint(1) NOT NULL DEFAULT '0',
  `is_important_receiver` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted_receiver` tinyint(1) NOT NULL DEFAULT '0',
  `is_purged_receiver` tinyint(1) NOT NULL DEFAULT '0',
  `is_read_sender` tinyint(1) NOT NULL DEFAULT '0',
  `is_important_sender` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted_sender` tinyint(1) NOT NULL DEFAULT '0',
  `is_purged_sender` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bp_paypal_agreement`
--

CREATE TABLE `bp_paypal_agreement` (
  `id` int(11) NOT NULL,
  `bp_id` int(11) NOT NULL,
  `bp_plan_id` int(11) NOT NULL,
  `agreement_id` varchar(255) NOT NULL,
  `agreement_name` varchar(255) NOT NULL,
  `agreement_desc` varchar(255) NOT NULL,
  `agreement_state` varchar(45) NOT NULL,
  `agreement_start_date` datetime NOT NULL,
  `agreement_end_date` datetime NOT NULL,
  `plan_id` varchar(255) NOT NULL,
  `payer_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bp_paypal_ipn`
--

CREATE TABLE `bp_paypal_ipn` (
  `id` int(11) NOT NULL,
  `txn_id` varchar(255) NOT NULL,
  `payment_status` varchar(45) NOT NULL,
  `payer_id` varchar(255) NOT NULL,
  `recurring_payment_id` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bp_paypal_plan`
--

CREATE TABLE `bp_paypal_plan` (
  `id` int(11) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `buyer_id` varchar(255) NOT NULL,
  `plan_id` varchar(255) NOT NULL,
  `plan_state` varchar(255) NOT NULL,
  `plan_create_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bp_paypal_webhook`
--

CREATE TABLE `bp_paypal_webhook` (
  `id` int(11) NOT NULL,
  `webhook_id` varchar(255) NOT NULL,
  `webhook_url` varchar(255) NOT NULL,
  `webhook_event_types` varchar(255) NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bp_paypal_webhook_event`
--

CREATE TABLE `bp_paypal_webhook_event` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) NOT NULL,
  `event_type` varchar(255) NOT NULL,
  `agreement_id` varchar(255) NOT NULL,
  `payer_id` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bp_pricing_original`
--

CREATE TABLE `bp_pricing_original` (
  `id` int(11) NOT NULL,
  `country` varchar(2) NOT NULL,
  `category` varchar(45) NOT NULL,
  `price_tier_1` decimal(10,6) NOT NULL,
  `price_tier_2` decimal(10,6) NOT NULL,
  `price_tier_3` decimal(10,6) NOT NULL,
  `price_tier_4` decimal(10,6) NOT NULL,
  `price_tier_5` decimal(10,6) NOT NULL,
  `price_tier_6` decimal(10,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `bp_pricing_original`
--

INSERT INTO `bp_pricing_original` (`id`, `country`, `category`, `price_tier_1`, `price_tier_2`, `price_tier_3`, `price_tier_4`, `price_tier_5`, `price_tier_6`) VALUES
(1, 'us', 'dedicated', '0.798000', '0.735000', '0.630000', '0.560000', '0.490000', '0.420000'),
(2, 'us', 'semi-3', '0.322000', '0.294000', '0.287000', '0.259000', '0.231000', '0.210000'),
(3, 'us', 'rotate', '1.400000', '1.358000', '1.295000', '1.260000', '1.225000', '1.190000'),
(4, 'de', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(5, 'de', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(6, 'de', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(7, 'br', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(8, 'br', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(9, 'br', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(10, 'za', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(11, 'za', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(12, 'za', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(13, 'gb', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(14, 'gb', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(15, 'gb', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(16, 'fr', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(17, 'fr', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(18, 'fr', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(19, 'th', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(20, 'th', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(21, 'th', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(22, 'es', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(23, 'es', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(24, 'es', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(25, 'be', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(26, 'be', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(27, 'be', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(28, 'jp', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(29, 'jp', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(30, 'jp', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(31, 'kr', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(32, 'kr', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(33, 'kr', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(34, 'se', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(35, 'se', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(36, 'se', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(37, 'nl', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(38, 'nl', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(39, 'nl', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(40, 'au', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(41, 'au', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(42, 'au', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(43, 'ch', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(44, 'ch', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(45, 'ch', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(46, 'fj', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(47, 'fj', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(48, 'fj', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(49, 'pt', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(50, 'pt', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(51, 'pt', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(52, 'it', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(53, 'it', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(54, 'it', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(55, 'gr', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(56, 'gr', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(57, 'gr', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(58, 'dk', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(59, 'dk', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(60, 'dk', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(61, 'no', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(62, 'no', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(63, 'no', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(64, 'pe', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(65, 'pe', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(66, 'pe', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(67, 'ar', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(68, 'ar', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(69, 'ar', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(70, 'ie', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(71, 'ie', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(72, 'ie', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(73, 'at', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(74, 'at', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(75, 'at', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bp_pricing_reseller`
--

CREATE TABLE `bp_pricing_reseller` (
  `id` int(11) NOT NULL,
  `country` varchar(2) NOT NULL,
  `category` varchar(45) NOT NULL,
  `price_tier_1` decimal(10,6) NOT NULL,
  `price_tier_2` decimal(10,6) NOT NULL,
  `price_tier_3` decimal(10,6) NOT NULL,
  `price_tier_4` decimal(10,6) NOT NULL,
  `price_tier_5` decimal(10,6) NOT NULL,
  `price_tier_6` decimal(10,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `bp_pricing_reseller`
--

INSERT INTO `bp_pricing_reseller` (`id`, `country`, `category`, `price_tier_1`, `price_tier_2`, `price_tier_3`, `price_tier_4`, `price_tier_5`, `price_tier_6`) VALUES
(1, 'us', 'dedicated', '0.798000', '0.735000', '0.630000', '0.560000', '0.490000', '0.420000'),
(2, 'us', 'semi-3', '0.322000', '0.294000', '0.287000', '0.259000', '0.231000', '0.210000'),
(3, 'us', 'rotate', '1.400000', '1.358000', '1.295000', '1.260000', '1.225000', '1.190000'),
(4, 'de', 'dedicated', '10.910000', '0.690000', '0.670000', '0.630000', '0.600000', '0.560000'),
(5, 'de', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(6, 'de', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(7, 'br', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(8, 'br', 'semi-3', '3.350000', '0.340000', '0.320000', '0.290000', '0.280000', '0.260000'),
(9, 'br', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(10, 'za', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(11, 'za', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(12, 'za', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(13, 'gb', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(14, 'gb', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(15, 'gb', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(16, 'fr', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(17, 'fr', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(18, 'fr', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(19, 'th', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(20, 'th', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(21, 'th', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(22, 'es', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(23, 'es', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(24, 'es', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(25, 'be', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(26, 'be', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(27, 'be', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(28, 'jp', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(29, 'jp', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(30, 'jp', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(31, 'kr', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(32, 'kr', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(33, 'kr', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(34, 'se', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(35, 'se', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(36, 'se', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(37, 'nl', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(38, 'nl', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(39, 'nl', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(40, 'au', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(41, 'au', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(42, 'au', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(43, 'ch', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(44, 'ch', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(45, 'ch', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(46, 'fj', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(47, 'fj', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(48, 'fj', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(49, 'pt', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(50, 'pt', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(51, 'pt', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(52, 'it', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(53, 'it', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(54, 'it', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(55, 'gr', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(56, 'gr', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(57, 'gr', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(58, 'dk', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(59, 'dk', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(60, 'dk', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(61, 'no', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(62, 'no', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(63, 'no', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(64, 'pe', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(65, 'pe', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(66, 'pe', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(67, 'ar', 'dedicated', '10.910000', '0.690000', '0.670000', '7.630000', '0.600000', '0.560000'),
(68, 'ar', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(69, 'ar', 'rotate', '2.000000', '1.470000', '1.400000', '1.500000', '1.260000', '1.230000'),
(70, 'ie', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(71, 'ie', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(72, 'ie', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(73, 'at', 'dedicated', '0.910000', '0.686000', '0.665000', '0.630000', '0.595000', '0.560000'),
(74, 'at', 'semi-3', '0.350000', '0.336000', '0.315000', '0.294000', '0.280000', '0.259000'),
(75, 'at', 'rotate', '1.610000', '1.470000', '1.400000', '1.330000', '1.260000', '1.225000'),
(76, 'us', 'sneaker', '0.798000', '0.735000', '0.630000', '0.560000', '0.490000', '0.420000' ),
(77, 'us', 'supreme', '0.798000', '0.735000', '0.630000', '0.560000', '0.490000', '0.420000' );

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bp_pricing_tier`
--

CREATE TABLE `bp_pricing_tier` (
  `id` int(11) NOT NULL,
  `tier` int(11) NOT NULL,
  `from_val` int(11) NOT NULL,
  `to_val` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `bp_pricing_tier`
--

INSERT INTO `bp_pricing_tier` (`id`, `tier`, `from_val`, `to_val`) VALUES
(1, 1, 1, 100),
(2, 2, 101, 500),
(3, 3, 501, 1000),
(4, 4, 1001, 2500),
(5, 5, 2501, 5000),
(6, 6, 5001, 10000);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bp_reseller`
--

CREATE TABLE `bp_reseller` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` varchar(3) NOT NULL COMMENT 'Logo image type',
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bp_terms`
--

CREATE TABLE `bp_terms` (
  `id` int(11) NOT NULL,
  `terms_content` text NOT NULL,
  `created_on` datetime NOT NULL,
  `valid_until` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bp_user`
--

CREATE TABLE `bp_user` (
  `id` int(11) NOT NULL COMMENT 'User id of app',
  `bp_id` int(11) NOT NULL COMMENT 'User id at BP',
  `email` varchar(255) NOT NULL,
  `username` varchar(45) NOT NULL,
  `passphrase` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `roles` varchar(255) NOT NULL DEFAULT 'USER',
  `state` varchar(24) NOT NULL DEFAULT 'active',
  `auth_token` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bp_user_fp`
--

CREATE TABLE `bp_user_fp` (
  `id` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `is_reset` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bp_user_plan`
--

CREATE TABLE `bp_user_plan` (
  `id` int(11) NOT NULL,
  `bp_id` int(11) NOT NULL,
  `bp_plan_id` int(11) NOT NULL,
  `payment_type` varchar(25) NOT NULL,
  `payment_plan_id` varchar(255) NOT NULL,
  `payment_agreement_id` varchar(255) NOT NULL,
  `plan_country` varchar(2) NOT NULL,
  `plan_category` varchar(45) NOT NULL,
  `plan_count` int(11) NOT NULL,
  `plan_days` int(11) NOT NULL,
  `plan_price` decimal(10,2) NOT NULL,
  `plan_start` datetime NOT NULL,
  `plan_expiry` datetime NOT NULL,
  `plan_status` varchar(24) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `bp_api_credentials`
--
ALTER TABLE `bp_api_credentials`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bp_messages`
--
ALTER TABLE `bp_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bp_paypal_agreement`
--
ALTER TABLE `bp_paypal_agreement`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bp_paypal_ipn`
--
ALTER TABLE `bp_paypal_ipn`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bp_paypal_plan`
--
ALTER TABLE `bp_paypal_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bp_paypal_webhook`
--
ALTER TABLE `bp_paypal_webhook`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bp_paypal_webhook_event`
--
ALTER TABLE `bp_paypal_webhook_event`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bp_pricing_original`
--
ALTER TABLE `bp_pricing_original`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bp_pricing_reseller`
--
ALTER TABLE `bp_pricing_reseller`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bp_pricing_tier`
--
ALTER TABLE `bp_pricing_tier`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bp_reseller`
--
ALTER TABLE `bp_reseller`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bp_terms`
--
ALTER TABLE `bp_terms`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bp_user`
--
ALTER TABLE `bp_user`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bp_user_fp`
--
ALTER TABLE `bp_user_fp`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bp_user_plan`
--
ALTER TABLE `bp_user_plan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `bp_api_credentials`
--
ALTER TABLE `bp_api_credentials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `bp_messages`
--
ALTER TABLE `bp_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `bp_paypal_agreement`
--
ALTER TABLE `bp_paypal_agreement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `bp_paypal_ipn`
--
ALTER TABLE `bp_paypal_ipn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `bp_paypal_plan`
--
ALTER TABLE `bp_paypal_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `bp_paypal_webhook`
--
ALTER TABLE `bp_paypal_webhook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `bp_paypal_webhook_event`
--
ALTER TABLE `bp_paypal_webhook_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `bp_pricing_original`
--
ALTER TABLE `bp_pricing_original`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `bp_pricing_reseller`
--
ALTER TABLE `bp_pricing_reseller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `bp_pricing_tier`
--
ALTER TABLE `bp_pricing_tier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `bp_reseller`
--
ALTER TABLE `bp_reseller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `bp_terms`
--
ALTER TABLE `bp_terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `bp_user`
--
ALTER TABLE `bp_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'User id of app';
--
-- AUTO_INCREMENT für Tabelle `bp_user_fp`
--
ALTER TABLE `bp_user_fp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `bp_user_plan`
--
ALTER TABLE `bp_user_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;