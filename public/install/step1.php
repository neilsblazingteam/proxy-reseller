    <h3>Initial Settings</h3>
    <div class="form">
        <form class="m-t" name="step1" role="form" method="POST" action="?step=1">
            <p>
                Enter your database server and credentials
            </p>
            <div class="form-group">
                <label>Database Name</label>
                <input class="form-control" type="text" name="db_name" value="<?php echo $fieldValues['db_name'];?>" required>
                <?php if(!empty($fieldErrors['db_name'])) echo '<p>'.$fieldErrors['db_name'].'</p>';?>
            </div>
            <div class="form-group">
                <label>Database Host</label>
                <input class="form-control" type="text" name="db_host" value="<?php echo $fieldValues['db_host'];?>" required>
                <?php if(!empty($fieldErrors['db_host'])) echo '<p>'.$fieldErrors['db_host'].'</p>';?>
            </div>
            <div class="form-group">
                <label>Database User</label>
                <input class="form-control" type="text" name="db_user" value="<?php echo $fieldValues['db_user'];?>" required>
                <?php if(!empty($fieldErrors['db_user'])) echo '<p>'.$fieldErrors['db_user'].'</p>';?>
            </div>
            <div class="form-group">
                <label>Database Password</label>
                <input class="form-control" type="password" name="db_pass" value="<?php echo $fieldValues['db_pass'];?>" required>
                <?php if(!empty($fieldErrors['db_pass'])) echo '<p>'.$fieldErrors['db_pass'].'</p>';?>
            </div>
            <?php if($sureToDeleteDatabase) echo '<input type="hidden" name="sureToDeleteDatabase" value="true">';?>
            <button type="submit" class="btn btn-primary block full-width m-b">Go to Step 2</button>
        </form>
    </div>