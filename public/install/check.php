<?php
/**
 * @copyright 2016 Blazing SEO
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */

/**
 * Get minimum installation requirements
 * @return array Requirement types
 */
function getMinimumRequirements() {
    return [
        'php'=>'5.5.0',
        'mysql'=>'5.5.0',
    ];
}

/**
 * Get requirement extensions
 * @return array Extensions
 */
function getRequiredPhpExtensions() {
    return [
        'curl' => 'Curl',
        'imap' => 'IMAP',
        'pdo_mysql' => 'PDO Mysql',
    ];
}
/**
 * Get PDO Instance
 * @param $host
 * @param $user
 * @param $pass
 * @param $db
 * @return PDO
 */
function getPdoInstance($host, $user, $pass, $db)
{
    return new PDO(
        'mysql:host=' . $host . ';dbname=' . $db,
        $user,
        $pass
    );
}

/**
 * Get PDO Instance
 * @param $host
 * @param $user
 * @param $pass
 * @param $db
 * @return bool|string
 */
function checkPdoInstance($host,$user,$pass,$db) {
    try {
        $db = new PDO(
            'mysql:host='.$host.';dbname='.$db,
            $user,
            $pass
        );
        return !is_null($db);
    }
    catch( PDOException $e ) {
        return $e->getCode();
    }
}

/**
 * To check minimum requirements
 * @param string $type Requirement to check
 * @param string $version Version to check
 * @param array $params
 * @return bool
 */
function checkVersion($type,$version,$params=[]) {

    switch($type) {

        case 'php':
            return (version_compare(PHP_VERSION, $version, '>='));
            break;
        case 'mysql':
            $pdo = getPdoInstance($params['db_host'],$params['db_user'],$params['db_pass'],$params['database']);
            if($pdo) {
                $info = $pdo->query('SHOW VARIABLES LIKE "%VERSION%"')->fetch(PDO::FETCH_NUM);
                return (version_compare($info[1], $version, '>='));
            }
            break;

    }

}

/**
 * Check if PHP extension is loaded
 * @param $extension
 * @return bool
 */
function checkExtension($extension) {
    return extension_loaded($extension);
}