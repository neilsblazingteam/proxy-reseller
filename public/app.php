<?php
include dirname(__FILE__).'/../vendor/autoload.php';

// Default env
$env = 'prod';

$request = Symfony\Component\HttpFoundation\Request::createFromGlobals();
if (in_array($request->getHost(), [
    'blazing.reseller'
]) or (isset($_COOKIE[ 'debugger' ]) AND 'VmhtvYlVLI8X' == $_COOKIE[ 'debugger' ])) {
    $env = 'dev';
}

error_reporting(E_ALL &~ E_NOTICE);
date_default_timezone_set('America/Chicago'); // Nebraska CST timezone

switch ($env) {
    case 'dev':
        ini_set('display_errors', 1);
        break;

    case 'prod':
        error_reporting(E_ALL &~ E_NOTICE & ~E_WARNING);
        ini_set('display_errors', 1);
        break;
}

$app = new \ResellerApp\AppFramework();
$app->setup();