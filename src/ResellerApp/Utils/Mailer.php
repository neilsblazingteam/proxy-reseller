<?php
namespace ResellerApp\Utils;

use PHPMailer;
use ResellerApp\Templating\TwigEngine;

/**
 * Class Mailer
 * @package ResellerApp\Utils
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class Mailer
{
    /**
     * Mailer constructor.
     * @param TwigEngine $twigEngine
     * @param $config
     */
    function __construct(TwigEngine $twigEngine,$config)
    {
        $this->twigEngine = $twigEngine;
        $this->config = $config;

    }

    /**
     * Send mail
     * @param string $toEmail
     * @param string $toName
     * @param string $subject
     * @param string $template
     * @return bool
     * @throws \phpmailerException
     */
    function sendMail($toEmail,$toName,$subject,$template) {


            $mail = new PHPMailer;

            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->Host = $this->config['smtp_server'];
            $mail->Username = $this->config['smtp_user'];
            $mail->Password = $this->config['smtp_pass'];
            $mail->Port = $this->config['smtp_port'];
            $mail->SMTPSecure = $this->config['smtp_security'];

            $mail->setFrom($this->config['from_email'], $this->config['from_name']);
            $mail->addAddress($toEmail, $toName);

            $mail->isHTML(true);
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->Subject = $subject;
            $mail->Body    = $template;

            $result = $mail->send();

            if(!$result) {

                $headers = "MIME-Version: 1.0\r\n";
                $headers .= "X-Mailer: PHP/" . phpversion()."\r\n";
                $headers .= "From: ".$this->config['from_name']." <".$this->config['from_email'].">\r\n";
                $headers .= "Subject:".$subject."\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8\r\n";

                return mail($toEmail,$subject,$template,$headers);

            } else {
                $result = true;
            }


        return $result;

    }
}