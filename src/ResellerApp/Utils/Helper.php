<?php
namespace ResellerApp\Utils;

/**
 * Class Helper
 * @package ResellerApp\Utils
 */
class Helper
{
    /**
     * Extract cookie string from header string
     * @param string $headerStr
     * @param string $cookieKey
     * @return mixed
     */
    static function getCookieStringFromHeaderString($headerStr,$cookieKey) {

        $cookies = explode(';',$headerStr);
        $ret = explode(' ',$cookies[$cookieKey]);
        return $ret[1];

    }

}