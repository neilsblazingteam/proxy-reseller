<?php
namespace ResellerApp\Utils;

/**
 * Class Format
 * @package ResellerApp\Utils
 */
class Format
{
    /**
     * Formats given date to a human readable one
     *
     * @param null $stamp
     * @param string $format
     * @return bool|string
     */
    static function dateHuman($stamp = null, $format = 'Y/m/d') {
        if (is_null($stamp))
            $stamp = time();
        if (!is_int($stamp))
            $stamp = strtotime($stamp);
        $date = date($format,$stamp);
        return $date;
    }
    /**
     * Formats given date to a human readable one
     *
     * @param null $stamp
     * @param string $format
     * @return bool|string
     */
    static function dateTimeHuman($stamp = null, $format = 'Y/m/d H:i:s') {
        if (is_null($stamp))
            $stamp = time();
        if (!is_int($stamp))
            $stamp = strtotime($stamp);
        $date = date($format,$stamp);
        return $date;
    }
    /**
     * Formats given date to a database one
     *
     * @param null $stamp
     * @param string $format
     * @return bool|string
     */
    static function dateDb($stamp = null, $format = 'Y-m-d H:i:s') {
        if (is_null($stamp))
            $stamp = time();
        if (!is_int($stamp))
            $stamp = strtotime($stamp);
        $date = date($format,$stamp);
        return $date;
    }

    /**
     * @param $time
     * @return string
     */
    static function getTimeElapsed($time)
    {

        $time = time() - strtotime($time); // to get the time since that moment
        $time = ($time<1)? 1 : $time;
        $tokens = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'').' ago';
        }

    }

    /**
     * Format number
     * @param $number
     * @param $decimals
     * @param string $decimal_point
     * @param string $thousand_seperator
     * @return string
     */
    static function number($number,$decimals=2,$decimal_point='.',$thousand_seperator=',') {
        return number_format($number,$decimals,$decimal_point,$thousand_seperator);
    }

    /**
     * Generate random token
     * @param $length
     * @return string
     */
    static function generateToken($length=32) {
        return bin2hex(random_bytes($length));
    }
}