<?php
namespace ResellerApp\Utils;

/**
 * Class Paging
 * @package ResellerApp\Utils
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class Paging
{
    /**
     * Returns paginated data with settings
     * for rendering pagination within templates
     *
     * @param $ipp
     * @param $page
     * @param $route
     * @param $dataArray
     * @param int $items optional if data array is a limited result set
     * @return array
     */
    static function get($ipp,$page,$route,$dataArray,$items=0) {

        $settings = [];
        $itemsTotal = $items>0 ? $items : count($dataArray);
        $pages = $itemsTotal > 0 ? (int)ceil($itemsTotal/$ipp) : 0;

        if($items===0) {
            $chunkData = array_chunk($dataArray, $ipp);
            if(!empty($chunkData))
                $chunkData = array_combine(range(1, count($chunkData)), array_values($chunkData));
        } else {
            $chunkData[$page] = $dataArray;
        }
        $pagePrev = $page >= 2 ? $page - 1 : 1;
        $pageNext = $page < $pages ? $page + 1 : $pages;

        $linkParts = explode('?',$route);
        $linkOp = isset($linkParts[1]) ? '&' : '?';
        $link = isset($linkParts[1]) ? $linkParts[0] .'?'. $linkParts[1] : $linkParts[0];

        $rangeLinks = [];

        for($i=1; $i <= $pages; $i++) {
            $rangeLinks[] = ['page'=>$i,'url'=>$link.$linkOp."ipp={$ipp}&page={$i}"];
        }
        $settings['paging'] = [
            'current'=>$page,
            'ipp'=>$ipp,
            'range'=>[1,$pages],
            'rangeLinks'=>$rangeLinks,
            'total'=>$itemsTotal,
            'prev'=>$pagePrev,
            'prevLink'=>$link.$linkOp."ipp={$ipp}&page={$pagePrev}",
            'next'=>$pageNext,
            'nextLink'=>$link.$linkOp."ipp={$ipp}&page={$pageNext}",
            'route'=>$route,
            'show_paging'=>$itemsTotal>$ipp,
        ];
        $settings['chunk_data'] = $chunkData;

        return $settings;
    }
}