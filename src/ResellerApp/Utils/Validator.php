<?php
namespace ResellerApp\Utils;

/**
 * Class Validator
 * Wrapper class for GUMP validator
 * @package ResellerApp\Utils
 */
class Validator extends \GUMP
{

    /**
     * Override to make keys lcfirst
     *
     * @param null $convert_to_string
     * @return array|null
     */
    function get_errors_array($convert_to_string = null) {

        $errors = parent::get_errors_array($convert_to_string);
        return array_map('lcfirst',$errors);

    }

    /**
     * Verify password hash
     * @param string $raw
     * @param string $hash
     * @return bool
     */
    function checkPasswordHash($raw,$hash) {
        return password_verify($raw,$hash);
    }

}