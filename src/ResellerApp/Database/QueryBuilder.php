<?php
namespace ResellerApp\Database;

/**
 * Class QueryBuilder
 * @package ResellerApp\Database
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class QueryBuilder {

	/**
	 * @var Connection
     */
	protected $db;
	/**
	 * @var object
     */
	protected $statement;

	/**
	 * @var array
     */
	protected $bind_params = array();
	/**
	 * Convenience property for connection management
	 * @var string
	 */
	public $conn_name = "";

	/**
	 * Contructor
	 * @codeCoverageIgnore
	 * @param Connection $connection
	 */
	public function __construct(Connection $connection) {
		$this->db = $connection;
		return $this;
	}
	/**
	 * Destructor
	 * @codeCoverageIgnore
	 */
	public function __destruct() {
		$this->db = NULL;
	}
	/**
	 * @return $this
     */
	public function setUTF8() {
		$sql = "SET NAMES utf8";
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $table
	 * @param string $fields
	 * @return $this
     */
	public function select($table,$fields='') {
	
		$sql = "SELECT ";
		if(!empty($fields))
		$sql .= $fields;
		else 
		$sql .= "* ";
		$sql .= " FROM ".$table." ";

		$this->appendStatement($sql);
		return $this;
		
	}

	/**
	 * @param $table
	 * @param string $col
	 * @return $this
     */
	public function select_count($table,$col='*') {
		$sql = "SELECT COUNT({$col}) AS total ";
		$sql .= " FROM ".$table." ";
		
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $table
	 * @param $values
	 * @return $this
     */
	function update($table,$values) {
			
		$sql = "UPDATE ".$table." ";
		$sql .= "SET ";
		foreach($values as $k=>$v) {
		$vals[] = $k." = ?";
		$this->addBindParam($v);
		}
		$sql .= implode(",",$vals);
		
		$this->appendStatement($sql);
		
		return $this;
		
	}

	/**
	 * @param $table
	 * @return $this
     */
	function delete($table) {
			
		$sql = 'DELETE FROM '.$table.' ';
		
		$this->appendStatement($sql);
		
		return $this;
		
	}

	/**
	 * @param $table
	 * @param $values
	 * @return $this
     */
	function insert($table,$values) {
			
		$sql = "INSERT INTO ".$table;
		$sql .= " SET ";
		foreach($values as $k=>$v) {
		$vals[] = $k." = ?";
		$this->addBindParam($v);
		}
		$sql .= implode(",",$vals);
		
		$this->appendStatement($sql);
		
		return $this;
		
	}

	/**
	 * @param $col
	 * @param $op
	 * @param $val
	 * @return $this
     */
	function where($col,$op,$val) {
		
		$sql = "WHERE ".$col." ".$op." ?";
		$this->appendStatement($sql);
		$this->addBindParam($val);
		return $this;
	
	}

	/**
	 * @param $arr
	 * @param bool|false $op
	 * @param string $andor
	 * @return $this
     */
	function where_array($arr,$op=false,$andor="AND") {
		
		if($op) { // op to use on all items
		foreach($arr as $key=>$val) 
		$sqlClause[] = $key.$op."'".$val;
		$this->addBindParam($val); // bind second array value
		} else { // op is set within array
		foreach($arr as $clause) {
			$sqlClause[] = $clause[0].$clause[1]."'".$clause[2];
			$this->addBindParam($clause[2]); // bind third array value
		}
		}
		$sql = "WHERE ";
		$sql .= implode(" {$andor} ",$sqlClause);
		
		$this->appendStatement($sql);
		return $this;
	
	}

	/**
	 * @param $col
	 * @param $op
	 * @param $val
	 * @return $this
     */
	function or_where($col,$op,$val) {
		$sql = "OR ".$col." ".$op." ? ";
		$this->addBindParam($val);
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $col
	 * @param $op
	 * @param $val
	 * @return $this
     */
	function and_where($col,$op,$val) {
		$sql = "AND ".$col." ".$op." ? ";
		$this->addBindParam($val);
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $col
	 * @param $container
	 * @return $this
     */
	function where_in($col,$container) {
		$bindStr = implode(",", array_map(function($val) { return "?"; }, $container));
		$sql = "WHERE {$col} IN({$bindStr}) ";
		foreach($container as $param)
		$this->addBindParam($param);
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $col
	 * @param $container
	 * @return $this
     */
	function in($col,$container) {
		$bindStr = implode(",", array_map(function($val) { return "?"; }, $container));
		$sql = " {$col} IN({$bindStr}) ";
		foreach($container as $param)
			$this->addBindParam($param);
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $col
	 * @param $start
	 * @param $end
	 * @return $this
     */
	function between($col,$start,$end) {
		$sql = $col." BETWEEN '".$start."' AND '".$end."'";
		$this->addBindParam($start);
		$this->addBindParam($end);
		
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $limit
	 * @param int $offset
	 * @return $this
     */
	function limit($limit,$offset=0) {
		$sql = "LIMIT {$offset},{$limit} ";
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $tabletojoin
	 * @param $clause1
	 * @param $op
	 * @param $clause2
	 * @param string $type
	 * @return $this
     */
	public function join($tabletojoin,$clause1,$op,$clause2, $type='')	{
		$sql = $type." JOIN ".$tabletojoin." ON ".$clause1." ".$op." ".$clause2;
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $tabletojoin
	 * @param $clause1
	 * @param $op
	 * @param $clause2
	 * @return $this
     */
	public function left_join($tabletojoin,$clause1,$op,$clause2)	{
		$sql = " LEFT JOIN ".$tabletojoin." ON ".$clause1." ".$op." ".$clause2;
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $tabletojoin
	 * @param $clause1
	 * @param $op
	 * @param $clause2
	 * @return $this
     */
	public function right_join($tabletojoin,$clause1,$op,$clause2)	{
		$sql = " RIGHT JOIN ".$tabletojoin." ON ".$clause1." ".$op." ".$clause2;
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $tabletojoin
	 * @param $clause1
	 * @param $op
	 * @param $clause2
	 * @return $this
     */
	public function inner_join($tabletojoin,$clause1,$op,$clause2)	{
		$sql = " INNER JOIN ".$tabletojoin." ON ".$clause1." ".$op." ".$clause2;
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $tabletojoin
	 * @param $clause1
	 * @param $op
	 * @param $clause2
	 * @return $this
     */
	public function cross_join($tabletojoin,$clause1,$op,$clause2)	{
		$sql = " CROSS JOIN ".$tabletojoin." ON ".$clause1." ".$op." ".$clause2;
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $tabletojoin
	 * @param $clause1
	 * @param $op
	 * @param $clause2
	 * @return $this
     */
	public function left_outer_join($tabletojoin,$clause1,$op,$clause2)	{
		$sql = " LEFT OUTER JOIN ".$tabletojoin." ON ".$clause1." ".$op." ".$clause2;
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $tabletojoin
	 * @param $clause1
	 * @param $op
	 * @param $clause2
	 * @return $this
     */
	public function right_outer_join($tabletojoin,$clause1,$op,$clause2)	{
		$sql = " RIGHT OUTER JOIN ".$tabletojoin." ON ".$clause1." ".$op." ".$clause2;
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $tabletojoin
	 * @param $clause1
	 * @param $op
	 * @param $clause2
	 * @return $this
     */
	public function union_join($tabletojoin,$clause1,$op,$clause2)	{
		$sql = " UNION JOIN ".$tabletojoin." ON ".$clause1." ".$op." ".$clause2;
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $col
	 * @return $this
     */
	public function and_is_null($col)	{
		$sql = "AND ".$col." IS NULL";
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $col
	 * @return $this
     */
	public function and_is_not_null($col)	{
		$sql = "AND ".$col." IS NOT NULL";
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $col
	 * @param $date1
	 * @param $date2
	 * @param string $and
	 * @return $this
     */
	public function where_between($col,$date1,$date2,$and='')	{
		$sql = "WHERE ".$col." BETWEEN '".$date1."' AND '".$date2."'";
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $col
	 * @param $date1
	 * @param $date2
	 * @param string $and
	 * @return $this
     */
	public function and_where_between($col,$date1,$date2,$and='AND')	{
		$sql = "AND ".$col." BETWEEN '".$date1."' AND '".$date2."'";
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $cols
	 * @param string $direction
	 * @return $this
     */
	function order_by($cols,$direction="ASC") {
		$sql = "ORDER BY ".$cols." ".$direction;
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $cols
	 * @return $this
     */
	function group_by($cols) {
		$sql = "GROUP BY ".$cols;
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $ip
	 * @return int
     */
	function _wrap_inet_aton($ip) {
		return ip2long($ip);
	}

	/**
	 * @param $int
	 * @return string
     */
	function _wrap_inet_ntoa($int) {
		return long2ip($int);
	}

	/**
	 * @param $sql
	 * @return $this
     */
	function customQuery($sql) {
		$this->appendStatement($sql);
		return $this;
	}

	/**
	 * @param $val
     */
	function addBindParam($val) {
		$this->bind_params[] = $val;
	}

	/**
	 * @param $part
     */
	function appendStatement($part) {
		$this->statement[] = $part;
	}

	/**
	 * @return string
     */
	function getStatement() {
		$stmt = implode(" ",$this->statement);
		return $stmt;
	}

	/**
	 * @param bool|false $first
	 * @return mixed
     */
	function pull($first=false) {
		$res = $this->db->_pull($this->getStatement(),$this->bind_params,$first);
		$this->reset();
		return $res;
	}
	/**
	 * @return mixed
     */
	function go() {
		$res = $this->db->_go($this->getStatement(),$this->bind_params);
		$this->reset();
		return $res;
	}
	/**
	 * @return int
     */
	function records() {
		$res = $this->db->_records($this->getStatement(),$this->bind_params);
		$this->reset();
		return $res;
	}
	/**
	 * Clear class vars so next query can be run
	 *
	 * @return void
	 */
	public function reset() {
		$this->bind_params = null;
		$this->statement = null;
	}

	/**
	 * @return void
     */
	public function renderProfilingResult() {
		var_dump($this->db->getProfilingResult());
	}

}