<?php
namespace ResellerApp\Database;

use \PDO;

/**
 * Class Connection
 * @package ResellerApp\Database
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class Connection {


	/**
	 * Class instance variable
	 * @var PDO
	 */
	public $db;
	public $conn;
	protected $profiling_switch = false;
	protected $table_prefix;
	private $profiling_result = array();

	protected $statement;

	/**
	 * Map of named database connections
	 * @var array
	 */
	private $connections = array();

	/**
	 * Class instance variable
	 * @var Connection
	 */
	private static $instance = null;
	
	private function __construct() {}
	private function __clone() {}
	/**
	 * Make sure serialize/deserialize doesn't work
	 * @codeCoverageIgnore
	 * @throws \DomainException
	 */
	private function __wakeup()	{
		throw new \DomainException("Can't deserialize singleton");
	}
	/**
	 * Return  a connection manager instance
	 *
	 * @staticvar null $instance
	 * @return Connection
	 */
	public static function get_instance() {
		// @codeCoverageIgnoreStart
		if (self::$instance === null){
			self::$instance = new self();
		}
		// @codeCoverageIgnoreEnd

		return self::$instance;
	}
	/**
	 * Returns the connection specified by the name given
	 *
	 * @param string|array|object $name
	 * @return QueryBuilder
	 * @throws \InvalidArgumentException
	 */
	public function get_connection($name = '')
	{
		// If the parameter is a string, use it as an array index
		if (is_scalar($name) && isset($this->connections[$name]))
		{
			return $this->connections[$name];
		}
		elseif (empty($name) && ! empty($this->connections)) // Otherwise, return the last one
		{
			return end($this->connections);
		}

		// You should actually connect before trying to get a connection...
		echo ("The specified connection does not exist");
		return false;
	}

	/**
	 * Parse the passed parameters and return a connection
	 *
	 * @param \stdClass|string $params
	 * @return QueryBuilder
	 */
	 public function connect($params='')	{

		list($dsn, $dbtype, $params, $options) = $this->parse_params($params);

		// Create the database connection
		try {
			$this->db = new PDO($dsn, $params->user, $params->pass);
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			$this->db->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8');
		} catch(\PDOException $e) {
			echo 'ERROR: ' . $e->getMessage();
		}

		// Set the table prefix, if it exists
		if (isset($params->prefix))
		{
            $this->table_prefix = $params->prefix;
		}

		// Create Query Builder object
		$conn = new QueryBuilder($this);

		// Save it for later
		if (isset($params->alias))
		{
			$this->connections[$params->alias] = $conn;
		}
		else
		{
			$this->connections[] = $conn;
		}

		return $conn;

	}
	public function switchProfilingOn() {
		$this->profiling_switch = true;
	}
	public function switchProfilingOff() {
		$this->profiling_switch = false;
	}
	/**
	 * Parses params into a dsn and option array
	 *
	 * @param \stdClass $params
	 * @return array
	 */
	private function parse_params(\stdClass $params)
	{
		$params->type = strtolower($params->type);
		$dbtype = "mysql";

		// Set additional PDO options
		$options = array();

		if (isset($params->options))
		{
			$options = (array) $params->options;
		}

		// Create the dsn for the database to connect to
		$dsn = $this->create_dsn($dbtype, $params);

		return array($dsn, $dbtype, $params, $options);
	}

	// --------------------------------------------------------------------------

	/**
	 * Create the dsn from the db type and params
	 *
	 * @param string $dbtype
	 * @param \stdClass $params
	 * @return string
	 */
	private function create_dsn($dbtype, \stdClass $params)
	{
		if ($dbtype === 'firebird') $dsn = "{$params->host}:{$params->file}";
		elseif ($dbtype === 'sqlite') $dsn = $params->file;
		else
		{
			$dsn = strtolower($dbtype) . ':';

			if ( ! empty($params->database))
			{
				$dsn .= "dbname={$params->database}";
			}

			$skip = array(
				'name' => 'name',
				'pass' => 'pass',
				'user' => 'user',
				'file' => 'file',
				'type' => 'type',
				'prefix' => 'prefix',
				'options' => 'options',
				'database' => 'database',
				'alias' => 'alias'
			);

			foreach($params as $key => $val)
			{
				if ( ! isset($skip[$key]))
				{
					$dsn .= ";{$key}={$val}";
				}
			}
		}

		return $dsn;
	}
	public function prepare_execute($sql, $bindparams) {

		$this->statement = $this->db->prepare($sql);
		return $this->statement->execute($bindparams);

	}
	public function fetch_all() {
		return $this->statement->fetch();
	}
	public function _pull($stmt,$bindParams,$first=false) {
	
		if($bindParams == null) {

			if($this->profiling_switch === true)
				$this->db->query('set profiling=1');

			$qry = $this->db->query($stmt);
			if($first == false) {
			$res = $qry->fetchAll(PDO::FETCH_ASSOC);
			} else { 
			$res = $qry->fetch(PDO::FETCH_ASSOC);
			}

			if($this->profiling_switch === true) {
				$profiling_stmt = $this->db->query('show profiles');
				$profiling_res = $profiling_stmt->fetchAll(PDO::FETCH_ASSOC);
				$this->profiling_result = $profiling_res;

				$this->db->query('set profiling=0');
			}
		} else {
			$this->prepare_execute($stmt,array_values($bindParams)); 
			if($first == false) {
			$res = $this->statement->fetchAll(PDO::FETCH_ASSOC);
			} else { 
			$res = $this->statement->fetch(PDO::FETCH_ASSOC);
			}
		}
		$this->statement = null;

		return $res;

	}	

	public function _go($stmt,$bindParams) {
		return $this->prepare_execute($stmt,array_values($bindParams));
	}
	public function _records($stmt,$bindParams) {
		$res = $this->_pull($stmt,$bindParams,1); 
		return (int)$res;
	}
	public function getProfilingResult() {
		return $this->profiling_result;
	}

}