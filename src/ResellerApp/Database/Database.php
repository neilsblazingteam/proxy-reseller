<?php
namespace ResellerApp\Database;

/**
 * Class Database
 * @package ResellerApp\Database
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class Database {

    function  __construct($params='') {

        $conn = Connection::get_instance();

        // If you are getting a previously created connection
        if (is_scalar($params))	{
            return $conn->get_connection($params);
        } elseif ( ! is_scalar($params) && ! is_null($params)) {

            $params_object = new \stdClass();

            foreach($params as $k => $v)
            {
                $params_object->$k = $v;
            }

            // Otherwise, return a new connection
            return $conn->connect($params_object);

        }
    }


}