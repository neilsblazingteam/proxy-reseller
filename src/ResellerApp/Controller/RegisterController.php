<?php
namespace ResellerApp\Controller;

use ResellerApp\AppFramework;
use ResellerApp\Common\Helper;
use ResellerApp\Common\Plan;
use ResellerApp\Utils\Validator;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class RegisterController
 * @package ResellerApp\Controller
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class RegisterController extends AppFramework
{

    /**
     * Default action
     * @route /register
     */
    function indexAction()
    {

        $commonHelper = new Helper();

        $params = [];
        $commonPlan = new Plan($this);

        $params['locations'] = $this->getApi()->getInfo()->getLocations();

        // Filter allowed proxies
        $allowedProxies        = $this->getResellerRepo()->getFromSettings('proxySettings.enabled', []);
        $params[ 'countries' ] = array_keys(array_filter($params[ 'locations' ], function ($data, $key) use ($allowedProxies) {
            return
                // default settings
                !isset($allowedProxies[ 'countries' ]) or
                // enabled
                !empty($allowedProxies[ 'countries' ][ $key ]);
        }, ARRAY_FILTER_USE_BOTH));
        $params['allowed_country_categories'] = $allowedProxies['countries'];

        // Some categories allow to change country after purchase,
        // so they act as category over countries, not under countries as other categories
        $params[ 'additional_locations_opts' ] = array_filter($this->getApi()->getInfo()->getMetaCategories(),
            function ($data, $key) use ($allowedProxies) {
                return
                    // default settings
                    !isset($allowedProxies[ 'metaCategories' ]) or
                    // enabled
                    !empty($allowedProxies[ 'metaCategories' ][ $key ]);
            }, ARRAY_FILTER_USE_BOTH);
        $params[ 'countries' ] = array_merge($params[ 'countries' ], array_keys($params[ 'additional_locations_opts' ]));

        unset($params['locations']['intl']);

        $pricing = $this->getPricingRepo()->getPrices();
        $tiers = $this->getPricingRepo()->getTiers();

        $params['terms'] = $this->getResellerRepo()->getTerms()['terms_content'];
        $params['pricing'] = $commonHelper->extractPricesFromDatabaseResult($pricing);
        $params['tiers'] = $commonHelper->extractTiersFromDatabaseResult($tiers);
        $params['pricing_json'] = json_encode($params['pricing']);
        $params['tiers_json'] = json_encode($params['tiers']);

        if (isset($_GET['paymentPlan']) && $_GET['paymentPlan'] == 'true') {

            $token = $_GET['token'];
            $registerData = $this->getSession()->get('register_data');

            $registerData['tiers'] = $params['tiers'];
            $registerData['pricing'] = $params['pricing'];

            $result = $commonPlan->finalizeFirstPlan($token,$registerData);

            if(true === $result) {

                $this->redirectWithNotification('/login','success','Congratulations! You can now log into your account');

            } else {

                $params['error_msg'] = implode('<br/>',$result);

            }


        } elseif (isset($_GET['paymentPlan']) && $_GET['paymentPlan'] == 'false') {

            $this->getSession()->getFlashBag()->add('info','Account creation aborted');

            $response = new RedirectResponse($this->getAppUrl().'/register');
            $response->send();

        } else {

            // do form stuff first so we always pull real info after
            $action = $this->getRequest()->get('action');

            if ($action) {

                switch ($action) {

                    case 'add_user':

                        $requestData = $this->getRequest()->request->all();

                        $validator = new Validator();
                        $validator->validation_rules(array(
                            'username' => 'required',
                            'email' => 'required',
                            'password' => 'required',
                            'password_confirm' => 'required',
                            'first_name' => 'required',
                            'last_name' => 'required',
                            'plan' => 'required',
                            'locations' => 'required',
                            'acceptTerms' => 'required',
                        ));

                        $validator->filter_rules(array(
                            'username' => 'trim',
                            'email' => 'trim',
                            'password' => 'trim',
                            'password_confirm' => 'trim',
                            'first_name' => 'trim',
                            'last_name' => 'trim',
                        ));

                        $validated_data = $validator->run($requestData);

                        if ($validated_data === false) {

                            $errors = $validator->get_errors_array();

                            $params = ['errors' => $errors];

                            $this->output('register.html.twig', $params);

                            return false;

                        } else {

                            if (!isset($requestData['plan'])) {

                                $params = ['error_msg' => 'You must at least pick one plan'];

                            } else {

                                $params['posted_data'] = $validated_data;

                                // check locations
                                $preferredLocationsCount = $this->getApi()->getHelper()->extractLocationsCount($this->getApi(), $requestData['plan'],$requestData['locations']);

                                // any locations assigned
                                if ($preferredLocationsCount == 0) {

                                    $params['wizard_step'] = 1;
                                    $params['error'] = true;
                                    $params['error_msg'] = 'You must assign your locations first';
                                    $this->output('register.html.twig', $params);
                                    return false;

                                }

                                // locations count matches plan proxy count
                                if ($preferredLocationsCount != $validated_data['plan']['count']) {

                                    $params['wizard_step'] = 1;
                                    $params['error'] = true;
                                    $moreLess = $preferredLocationsCount > $validated_data['plan']['count'] ? 'less' : 'more';
                                    $params['error_msg'] = 'You must assign '.$moreLess.' locations';
                                    $this->output('register.html.twig', $params);
                                    return false;

                                }

                                // check if email exists
                                $emailCheck = $this->getUserRepo()->loadUserByEmail($validated_data['email']);

                                if ($emailCheck) {
                                    $params['wizard_step'] = 2;
                                    $params['error'] = true;
                                    $params['validation']['email'] = 'Email exists';
                                    $this->output('register.html.twig', $params);
                                    return false;
                                }

                                // check if username exists
                                $userCheck = $this->getUserRepo()->loadUserByUsername($validated_data['username']);

                                if ($userCheck) {
                                    $params['wizard_step'] = 2;
                                    $params['error'] = true;
                                    $params['validation']['username'] = 'Username exists';
                                    $this->output('register.html.twig', $params);
                                    return false;
                                }

                                // all good start paypal process
                                $validated_data['tiers'] = $params['tiers'];
                                $validated_data['pricing'] = $params['pricing'];

                                // store entered user data to session to use on next step
                                $this->getSession()->set('register_data', $validated_data);

                                $commonPlan->createPlan($validated_data,'new');

                            }

                        }

                        break;
                }

            }

        }

        $this->output('register.html.twig', $params);
        exit;

    }

}