<?php
namespace ResellerApp\Controller;

use ResellerApp\AppFramework;
use ResellerApp\Utils\Format;
use ResellerApp\Utils\Validator;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class DefaultController
 *
 * Default application controller
 *
 * @package ResellerApp\Controller
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class DefaultController extends AppFramework
{

    /**
     * Default action
     * @route /
     */
    function indexAction() {

        $params = [];

        $this->output('index.html.twig',$params);

    }
    /**
     * Reset password action
     * @route /passwordReset
     */
    function resetPasswordAction() {

        $params = [];

        $requestData = $this->getRequest()->request->all();
        $queryData = $this->getRequest()->query->all();

        if(isset($queryData['token'])) {

            $validator = new Validator();
            $validator->validation_rules(array(
                'token' => 'required',
            ));

            $validator->filter_rules(array(
                'token' => 'trim',
            ));

            $validated_token = $validator->run($queryData);

            if ($validated_token === false) {

                $errors = $validator->get_errors_array();

                $params = ['errors' => $errors];

            } else {

                $token = $validated_token['token'];

                $res = $this->getUserRepo()->getEmailFromPasswordToken($token);

                if(!$res) {
                    $params['error_msg'] = 'Invalid token';
                } else {

                    if(isset($requestData['action']) && $requestData['action'] == 'resetPassword') {

                        $validator = new Validator();
                        $validator->validation_rules(array(
                            'password' => 'required',
                            'password_confirm' => 'required',
                        ));

                        $validator->filter_rules(array(
                            'password' => 'trim',
                            'password_confirm' => 'trim',
                        ));

                        $validated_data = $validator->run($requestData);

                        if ($validated_data === false) {

                            $errors = $validator->get_errors_array();

                            $params = ['errors' => $errors];

                        } else {

                            $email = $res['email'];

                            $newPass = password_hash($validated_data['password_confirm'], PASSWORD_DEFAULT);

                            $ret = $this->getUserRepo()->updateUserByEmail($email, ['passphrase' => $newPass]);

                            if ($ret) {
                                $this->getUserRepo()->setPasswordReset($token,1);
                                $this->getSession()->getFlashBag()->add('success','Password reset successful');

                                $response = new RedirectResponse($this->getAppUrl().'/login');
                                $response->send();

                            } else {
                                $params['error_msg'] = 'Could not store new password. Please contact system administrator';
                            }

                        }
                    }

                }

                $this->output('password-reset.html.twig', $params);
                return;
            }

        }

        if(isset($requestData['action']) && $requestData['action'] == 'requestPassword') {

            $params['posted_data'] = $requestData;

            $validator = new Validator();
            $validator->validation_rules(array(
                'email' => 'required',
            ));

            $validator->filter_rules(array(
                'email' => 'trim',
            ));

            $validated_data = $validator->run($requestData);

            if ($validated_data === false) {

                $errors = $validator->get_errors_array();

                $params = ['errors' => $errors];

                $this->output('password-request.html.twig', $params);

            } else {

                $userData = $this->getUserRepo()->loadUserByEmail($validated_data['email']);
                if ($userData) {

                    $resetToken = Format::generateToken(32);

                    $this->getUserRepo()->insertResetPasswordToken($this->getUserRepo()->getEmail(),$resetToken);

                    $fullName = $userData['first_name'].' '.$userData['last_name'];
                    $ret = $this->getMailer()->sendMail($userData['email'],$fullName,'Reset Password',$this->render('email/password-reset.html.twig',['full_name'=>$fullName,'reset_token'=>$resetToken]));

                    if($ret) {

                        $this->getSession()->getFlashBag()->add('success','We have sent a reset link to your email');

                        $response = new RedirectResponse($this->getAppUrl().'/passwordReset');
                        $response->send();

                    } else {
                        $params['error_msg'] = 'Could not send reset email. Please contact system administrator.';
                    }

                } else {

                    $params['error_msg'] = 'No account found with email '.$validated_data['email'];

                }
            }

        }

        $this->output('password-request.html.twig',$params);

    }

    function checkUsernameAction($username) {

        $this->getUserRepo()->loadUserByUsername($username);
        $userId = $this->getUserRepo()->getId();

        if(!is_null($userId)){
            $ret = ['error'=>'Username exists'];
        } else {
            $ret = ['success'=>'Username available'];
        }

        $this->getResponse()->setContent(json_encode($ret));
        $this->getResponse()->headers->set('Content-Type', 'application/json');
        $this->getResponse()->send();

    }

    function checkEmailAction($email) {

        $this->getUserRepo()->loadUserByEmail($email);
        $userId = $this->getUserRepo()->getId();

        if(!is_null($userId)){
            $ret = ['error'=>'Email exists'];
        } else {
            $ret = ['success'=>'No account associated with email'];
        }

        $this->getResponse()->setContent(json_encode($ret));
        $this->getResponse()->headers->set('Content-Type', 'application/json');
        $this->getResponse()->send();

    }


}