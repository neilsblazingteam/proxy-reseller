<?php
namespace ResellerApp\Controller;

use ResellerApp\AppFramework;
use ResellerApp\Common\Plan;
use ResellerApp\Repo\PaypalPlanRepo;
use ResellerApp\Repo\PricingRepo;
use ResellerApp\Repo\UserPlanRepo;
use ResellerApp\Security\TokenService;
use ResellerApp\Templating\LayoutExtension;
use ResellerApp\Utils\Format;
use ResellerApp\Utils\Paging;
use ResellerApp\Utils\Validator;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class AdminController
 * @package ResellerApp\Controller
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class AdminController extends AppFramework
{

    function __construct()
    {

        $tokenService = new TokenService();
        $cookie = $this->getRequest()->cookies->get('auth');

        if(is_null($cookie)) {

            $response = new RedirectResponse($this->getAppUrl().'/login');
            $response->send();

        } else {

            // load user data
            $repo = $this->getUserRepo();

            $data = $tokenService->getTokenData($cookie);

            if(!$data) {
                $this->getRequest()->cookies->remove('auth');
                $response = new RedirectResponse($this->getAppUrl().'/login');
                $response->send();
            }

            $repo->loadUserById($data->aud->user_id);

            // check for privileges
            $roles = $repo->getRoles();

            if(!in_array('ROLE_ADMIN',$roles)) {
                die('Access denied!');
            }

            // Check the initial settings
            if ($this->getRequest()->getRequestUri() !== '/admin/edit/app') {
                foreach ([
                    [
                        'defined' => !!$this->getConfig('mail'),
                        'message' => 'You must enter mail settings before you can start selling'
                    ],
                    [
                        'defined' => !!$this->getResellerRepo()->getFromSettings('proxySettings.enabled'),
                        'message' => 'You must set proxy settings before you can start selling'
                    ]
                ] as $data) {
                    if (!$data[ 'defined' ]) {
                        $this->redirectWithNotification('/admin/edit/app', 'info', $data[ 'message' ]);
                    }
                }
            }
        }

    }

    /**
     * Default action
     * @route /admin/dashboard
     */
    function indexAction() {

        $dateRangeToday = [0=>date('Y-m-d 00:00:00'),1=>date('Y-m-d 23:59:59',time())];
        $dateRangeMonth = [0=>date('Y-m-01 00:00:00'),1=>date('Y-m-31 23:59:59',time())];
        $dateRangeYear = [0=>date('Y-01-01 00:00:00'),1=>date('Y-m-31 23:59:59',time())];

        $bpUsers = $this->getApi()->getUser()->all();
        $totalUsers = count($bpUsers);
        $totalUsersD = $this->getUserRepo()->countUsersByDateRange($dateRangeToday[0],$dateRangeToday[1]);
        $totalUsersM = $this->getUserRepo()->countUsersByDateRange($dateRangeMonth[0],$dateRangeMonth[1]);
        $planRepo = new UserPlanRepo($this->getDatabase());
        $totalPlans = $planRepo->countPlans();
        $totalPlansD = $planRepo->countPlansByDateRange($dateRangeToday[0],$dateRangeToday[1]);
        $totalPlansM = $planRepo->countPlansByDateRange($dateRangeMonth[0],$dateRangeMonth[1]);
        $totalPlansY = $planRepo->countPlansByDateRange($dateRangeYear[0],$dateRangeYear[1]);
        $sumPlans = $planRepo->sumPlanAmount();
        $sumPlansD = $planRepo->sumPlanAmountByDateRange($dateRangeToday[0],$dateRangeToday[1]);
        $sumPlansM = $planRepo->sumPlanAmountByDateRange($dateRangeMonth[0],$dateRangeMonth[1]);
        $sumPlansY = $planRepo->sumPlanAmountByDateRange($dateRangeYear[0],$dateRangeYear[1]);

        $latestPlans = $planRepo->loadPlans([10,0]);
        $latestUsers = $this->getUserRepo()->loadUsers([10,0]);
        $bpBalance = $this->getApi()->getReseller()->getBalance();

        $params = [
            'bp_balance'=>$bpBalance['credits'],
            'total_users'=>$totalUsers,
            'total_users_d'=>$totalUsersD,
            'total_users_m'=>$totalUsersM,
            'total_plans'=>$totalPlans,
            'total_plans_d'=>$totalPlansD,
            'total_plans_m'=>$totalPlansM,
            'total_plans_y'=>$totalPlansY,
            'total_plans_sum'=>$sumPlans,
            'total_plans_sum_d'=>$sumPlansD,
            'total_plans_sum_m'=>$sumPlansM,
            'total_plans_sum_y'=>$sumPlansY,
            'latest_plans'=>$latestPlans,
            'latest_users'=>$latestUsers,

        ];

        $this->output('dashboard/admin/dashboard.html.twig',$params);

    }

    /**
     * Manage users action
     * @route /admin/users
     */
    function manageUsersAction() {

        $requestData = $this->getRequest()->request->all();

        $page = $this->getRequest()->query->get('page') ? $this->getRequest()->query->get('page') : 1;
        $ipp = $this->getRequest()->query->get('page') ? $this->getRequest()->query->get('ipp') : 10;

        if(isset($requestData['action'])) {

            switch($requestData['action']) {

                case 'search_user':

                    $validator = new Validator();
                    $validator->validation_rules(array(
                        'search_user' => 'required',
                    ));

                    $validator->filter_rules(array(
                        'search_user' => 'trim',
                    ));

                    $validated_data = $validator->run($requestData);

                    if ($validated_data === false) {

                        $errors = $validator->get_errors_array();

                        $params['posted_data'] = $requestData;
                        $params['errors'] = $errors;

                        $this->output('dashboard/admin/manage-users.html.twig',$params);

                        return false;

                    } else {

                        $uData = $this->getUserRepo()->searchUsersByUsername($validated_data['search_user']);

                        if ($uData) {

                            $params = Paging::get($ipp,$page,$this->getRouter()->getRoutes()->get('admin_manage_users')->getPath(),$uData);

                            $params['show_paging'] = false;

                        } else {
                            $params['user_not_found'] = true;
                        }
                        $params['posted_data'] = $validated_data;

                        $this->output('dashboard/admin/manage-users.html.twig', $params);

                        return false;

                    }

                    break;
            }
        }

        $users = $this->getApi()->getUser()->all();
        $userData = [];

        // collect reseller user data
        foreach($users as $user) {

            $bpId = (int)$user['user_id'];
            $uData = $this->getUserRepo()->getUserByBpId($bpId);

            if($uData) {
                unset($uData['passphrase']);
                $userData[$bpId] = $uData;
            } else {
                $userData[$bpId] = ['bp_id'=>$bpId,'username'=>$user['username']];
            }

        }

        $params = Paging::get($ipp,$page,$this->getRouter()->getRoutes()->get('admin_manage_users')->getPath(),$userData);

        $this->output('dashboard/admin/manage-users.html.twig',$params);

    }

    /**
     * Personalize app action
     * @param int $bpId
     * @param string $state
     * @route /admin/users/edit/{bpId}/state/{state}
     */
    function userStateAction($bpId,$state) {

        $this->getUserRepo()->updateUser($bpId,['state'=>$state]);

        //@todo Delete user if state deleted

        $this->redirectWithNotification('/admin/users','success','User state changed successfully');

    }
    /**
     * Personalize app action
     * @route /admin/edit/app
     */
    function editAppAction()
    {
        $requestData = $this->getRequest()->request->all();

        if(isset($requestData['action'])) {

            switch ($requestData['action']) {
                case 'save_data':
                    $result = $this->getResellerRepo()->updateAppData(['name'=>$requestData['app_name']]);
                    $this->getResellerRepo()->updateTerms($requestData['app_terms']);
                    $logoFile = $_FILES['app_logo'];

                    if(!is_null($logoFile['name']) && !empty($logoFile['name'])) {

                        $error = '';

                        $target_file =
                        $uploadOk = 1;
                        $imageFileType = pathinfo($logoFile['name'],PATHINFO_EXTENSION);
                        $fileDest = $this->getAppPath() . '/public/gui/img/logo.'.$imageFileType;
                        // Check if image file is a actual image or fake image

                        $uploadOk = 1;
                        // Check if file already exists
                        if (file_exists($target_file)) {
                            $error = "Sorry, file already exists.";
                            $uploadOk = 0;
                        }
                        // Check file size
                        if ($logoFile["size"] > 3 * 1024 * 1024) {
                            $error = "Sorry, your file is too large.";
                            $uploadOk = 0;
                        }
                        // Allow certain file formats
                        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                            && $imageFileType != "gif" ) {
                            $error = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                            $uploadOk = 0;
                        }
                        // Check if $uploadOk is set to 0 by an error
                        if ($uploadOk) { // if everything is ok, try to upload file
                            if (move_uploaded_file($logoFile["tmp_name"], $fileDest)) {
                                $result = $this->getResellerRepo()->updateAppData(['logo'=>$imageFileType]);
                            } else {
                                $error = "Sorry, there was an error uploading your file.";
                            }
                        }

                        if($error) {
                            $result = false;
                        }

                    }
                    if($result) {

                        $this->redirectWithNotification('/admin/edit/app','success','App data saved successfully');

                    } elseif(!empty($error)) {

                        $params['error_msg'] = $error;

                    } else {
                        $params['error_msg'] = 'Could not store app data';

                    }
                    break;

                case 'save_mail_settings':
                case 'test_mail_settings':
                    $configFile = "<?php
    return [
        'smtp_server'=>'" . $requestData['mail']['smtp_server'] . "',
        'smtp_user'=>'" . $requestData['mail']['smtp_user'] . "',
        'smtp_pass'=>'" . $requestData['mail']['smtp_pass'] . "',
        'smtp_port'=>'" . $requestData['mail']['smtp_port'] . "',
        'smtp_security'=>'" . $requestData['mail']['smtp_security'] . "',
        'from_email'=>'" . $requestData['mail']['from_email'] . "',
        'from_name'=>'" . $requestData['mail']['from_name'] . "',
    ];";

                    $result = file_put_contents($this->getPath('config').'/mail.php', $configFile);

                    $this->setConfigKey('mail',$requestData['mail']);

                    if ($requestData['action'] == 'save_mail_settings') {
                        if($result) {
                            //$this->redirectWithNotification('/admin/edit/app','success','Mail settings saved successfully');
                            $params['success_msg'] = 'Mail settings saved successfully';
                        } else {
                            $params['error_msg'] = 'Could not store mail settings';
                        }
                    }
                    // Test mailer
                    elseif ($result) {
                         $result =$this->getMailer()->sendMail(
                            $this->getUserRepo()->getEmail(),
                            $this->getUserRepo()->getUsername(),
                            'Test email',
                            $this->render('email/test.html.twig',
                                ['full_name' => $this->getUserRepo()->getUsername()])
                        );

                        if($result) {
                            $params['success_msg'] = 'Mail sent successfully';
                        } else {
                            $params['error_msg'] = 'Could not send mail';
                        }
                    }
                    break;

                case 'save_paypal_settings':
                    $configFile = "<?php
    return [
        'credentials'=>[
            'sandbox'=>[
                'clientId'=>'" . $requestData['paypal']['credentials']['sandbox']['clientId'] . "',
                'secret'=>'" . $requestData['paypal']['credentials']['sandbox']['secret'] . "',
            ],
            'live'=>[
                'clientId'=>'" . $requestData['paypal']['credentials']['live']['clientId'] . "',
                'secret'=>'" . $requestData['paypal']['credentials']['live']['secret'] . "',
            ],
        ],
        'mode'=>'" . $requestData['paypal']['mode'] . "'
    ];";


                    $result = file_put_contents($this->getPath('config').'/paypal.php', $configFile);

                    $this->setConfigKey('paypal',$requestData['paypal']);

                    if($result) {

                        //$this->redirectWithNotification('/admin/edit/app','success','Mail settings saved successfully');

                        $params['success_msg'] = 'PayPal settings saved successfully';

                    } else {

                        $params['error_msg'] = 'Could not store PayPal settings';

                    }

                break;

                case 'save_proxy_settings':
                    $proxiesTypes = (array) $requestData['proxies_types'];
                    $locations = $this->getApi()->getInfo()->getLocations();
                    $metaCategories = $this->getApi()->getInfo()->getMetaCategories();

                    $parsedData = [];
                    foreach ($proxiesTypes as $proxy) {
                        $proxy = explode('.', $proxy);

                        // 2 level max to simplify that task
                        if (1 == count($proxy)) {
                            if (empty($parsedData[$proxy[0]])) {
                                $parsedData[$proxy[0]] = [];
                            }
                        }
                        elseif (2 == count($proxy)) {
                            $parsedData[$proxy[0]][] = $proxy[1];
                        }
                    }

                    $preparedData = [
                        'countries' => [],
                        'metaCategories' => []
                    ];
                    foreach ($parsedData as $level1 => $levels2) {
                        // Country
                        if (!empty($locations[$level1])) {
                            // If no categories - disable country
                            if ($levels2) {
                                $preparedData['countries'][$level1] = $levels2;
                            }
                        }
                        // MetaCategory
                        elseif (!empty($metaCategories[$level1])) {
                            $preparedData['metaCategories'][$level1] = true; // there is no settings for meta currently
                        }
                    }
                    // Cleanup
                    if (!$parsedData) {
                        $preparedData = [];
                    }

                    $this->getResellerRepo()->setToSettings('proxySettings.enabled', $preparedData)->persistSettings();

                    $params['success_msg'] = 'Proxy settings saved successfully';
                    break;
            }
        }


        $appData = $this->getResellerRepo()->getAppData();
        $appTerms = $this->getResellerRepo()->getTerms();

        $params['app_name'] = $appData['name'];
        $params['app_terms'] = $appTerms['terms_content'];

        $params['mail'] = $this->getConfig('mail');
        $params['paypal'] = $this->getConfig('paypal');

        $proxySettings = $this->getResellerRepo()->getFromSettings('proxySettings.enabled', []);
        $params['proxies_types'] = [];
        $params['proxies_types_count'] = 0;
        $twigHelper = new LayoutExtension();
        foreach ($this->getApi()->getInfo()->getLocations() as $countryCode => $data) {
            $info = [
                'value'      => $countryCode,
                'title'      => $twigHelper->getCountryLabel($countryCode),
                'selected'   => !empty($proxySettings[ 'countries' ][ $countryCode ]),
                'categories' => []
            ];

            foreach ([
                ['value' => 'semi-3', 'title' => 'Semi-dedicated'],
                ['value' => 'dedicated', 'title' => 'Dedicated'],
                ['value' => 'rotate', 'title' => 'Rotating']
            ] as $category) {
                $info[ 'categories' ][] = array_merge($category, [
                    'selected' => $info[ 'selected' ] and in_array($category[ 'value' ],
                            $proxySettings[ 'countries' ][ $countryCode ])
                ]);
                $params['proxies_types_count']++;
            }

            $params[ 'proxies_types' ][] = $info;
            $params['proxies_types_count']++;
        }
        foreach ($this->getApi()->getInfo()->getMetaCategories() as $key => $data) {
            $params[ 'proxies_types' ][] = [
                'value' => $key,
                'title' => $data[ 'title' ],
                'selected' => !empty($proxySettings[ 'metaCategories' ][ $key ])
            ];
            $params['proxies_types_count']++;
        }

        $this->output('dashboard/admin/edit-app.html.twig',$params);

    }
    /**
     * Reset user password request
     * @route /admin/users/edit/{bpId}/passwordReset
     * @param $bpId
     */
    function passwordResetAction($bpId)
    {

        $this->getUserRepo()->loadUserByBpId($bpId);

        if ($userData = $this->getUserRepo()->all()) {

            $resetToken = Format::generateToken(32);

            $this->getUserRepo()->insertResetPasswordToken($this->getUserRepo()->getEmail(),$resetToken);

            $fullName = $userData['first_name'].' '.$userData['last_name'];
            $ret = $this->getMailer()->sendMail($userData['email'],$fullName,'Reset Password',$this->render('email/password-reset.html.twig',['full_name'=>$fullName,'reset_token'=>$resetToken]));

            if($ret) {

                $this->redirectWithNotification('/admin/users/edit/'.$bpId,'success','Reset email sent');

            } else {
                $params['error_msg'] = 'Could not send reset email. Please contact system administrator.';
            }

        } else {

            $params['error_msg'] = 'No account found with id'. $bpId;

        }


        $this->output('dashboard/admin/manage-user.html.twig',$params);

    }
    /**
     * Manage/Edit user
     * @route /admin/users/edit/{id}
     * @param $bpId
     */
    function manageUserAction($bpId) {


        $requestData = $this->getRequest()->request->all();

        if(isset($requestData['action'])) {

            switch($requestData['action']) {

                case 'save_user_data':

                    $userData = $requestData['user_data'];

                    $params = [
                        'first_name'=>$userData['first_name'],
                        'last_name'=>$userData['last_name'],
                        'email'=>$userData['email'],
                        'roles'=>implode(',',$userData['roles']),
                    ];

                    $this->getUserRepo()->updateUser($bpId,$params);
                    $res = $this->getUserRepo()->all();

                    if($res) {

                        $params['success_msg'] = 'User updated successfully';

                    } else {

                        $params['error_msg'] = 'Could not update user';

                    }

                    break;


            }
        }

        $uData = $this->getUserRepo()->getUserByBpId($bpId);
        $bpData = $this->getApi()->getUser()->get($bpId);

        $uData['roles'] = explode(',',$uData['roles']);

        $params['user_data'] = $uData;
        $params['bp_data'] = $bpData;

        $userPlanRepo = new UserPlanRepo($this->getDatabase());
        $params['user']['plans'] = $userPlanRepo->loadPlansByBpId($bpId);

        $this->output('dashboard/admin/manage-user.html.twig',$params);

    }

    /**
     * Manage packages
     * @route /admin/packages
     */
    function managePackagesAction() {

        $page = $this->getRequest()->query->get('page') ? $this->getRequest()->query->get('page') : 1;
        $ipp = $this->getRequest()->query->get('page') ? $this->getRequest()->query->get('ipp') : 50;

        $countries = $this->getApi()->getInfo()->getCountries();
        $planRepo = new PricingRepo($this->getDatabase());
        empty($countries) ? $planRepo->getPrices() : $planRepo->getPricesByCountriesCodes(array_keys($countries));

        $params = Paging::get($ipp,$page,$this->getRouter()->getRoutes()->get('admin_manage_packages')->getPath(),$planRepo->all());
        $params['tiers']=$planRepo->getTiers();
        $params['countries']=$countries;

        $this->output('dashboard/admin/manage-packages.html.twig',$params);

    }

    /**
     * Manage package
     * @route /admin/packages
     * @param $id
     */
    function managePackageAction($id) {

        $pricingRepo = new PricingRepo($this->getDatabase());
        $planRepo    = new PricingRepo($this->getDatabase());

        $requestData = $this->getRequest()->request->all();

        if(isset($requestData['action'])) {

            switch($requestData['action']) {

                case 'save_data':

                    $data = $requestData['package'];

                    $params = [
                        'price_tier_1'=>$data['price_tier_1'],
                        'price_tier_2'=>$data['price_tier_2'],
                        'price_tier_3'=>$data['price_tier_3'],
                        'price_tier_4'=>$data['price_tier_4'],
                        'price_tier_5'=>$data['price_tier_5'],
                        'price_tier_6'=>$data['price_tier_6'],
                    ];

                    $res = $pricingRepo->updatePrice($id,$params);

                    if($res) {

                        $params['success_msg'] = 'Package updated successfully';

                    } else {

                        $params['error_msg'] = 'Could not update package';

                    }

                    break;

            }
        }

        $params[ 'package' ]  = $pricingRepo->getPrice($id);
        $params[ 'tiers' ]    = array_values($planRepo->getTiers());

        $originalPricing = $this->getApi()->getReseller()->getPricing();
        if (!empty($originalPricing[ 'pricing' ][ $params[ 'package' ][ 'country' ] ][ $params[ 'package' ][ 'category' ] ])) {
            $originalPricing = $originalPricing[ 'pricing' ][ $params[ 'package' ][ 'country' ] ][ $params[ 'package' ][ 'category' ] ];

            // Compare resellers packages and api packages
            foreach ($params[ 'tiers' ] as $i => $resellerTier) {
                foreach ($originalPricing as $originalTier) {
                    if ($originalTier[ 'min' ] <= $resellerTier[ 'from_val' ] and
                        $resellerTier[ 'to_val' ] <= $originalTier[ 'max' ]
                    ) {
                        $params[ 'original_pricing' ][ $i ] = $originalTier[ 'price' ];
                        break;
                    }
                }
            }
        }

        $this->output('dashboard/admin/manage-package.html.twig',$params);
    }

    /**
     * Manage plans
     * @route /admin/plans
     */
    function managePlansAction() {

        $page = $this->getRequest()->query->get('page') ? $this->getRequest()->query->get('page') : 1;
        $ipp = $this->getRequest()->query->get('page') ? $this->getRequest()->query->get('ipp') : 10;

        $planRepo = new UserPlanRepo($this->getDatabase());
        $planRepo->loadAllPlans();

        $params = Paging::get($ipp,$page,$this->getRouter()->getRoutes()->get('admin_manage_plans')->getPath(),$planRepo->all());

        $this->output('dashboard/admin/plan-manage.html.twig',$params);

    }

    /**
     * Add plan action
     * @route /admin/plans/add/{bpid}
     * @param $bpId
     */
    function addPlanAction($bpId) {

        $locations = $this->getApi()->getInfo()->getLocations();

        $selectionCountries = [];
        $selectionCategories = [];

        foreach($locations as $country=>$regions) {
            $selectionCountries[$country] = $country;
            foreach($regions as $region=>$categories) {
                foreach($categories as $category=>$data) {
                    $selectionCategories[$category] = $category;
                }
            }
        }
        $params['countries'] = $selectionCountries;
        unset($params['countries']['intl']);
        $params['categories'] = $selectionCategories;

        $requestData = $this->getRequest()->request->all();

        $this->getUserRepo()->loadUserByBpId($bpId);
        $uData = $this->getUserRepo()->all();

        if(isset($requestData['action'])) {

            if($requestData['action'] == 'add_plan') {

                $formData = $requestData['plan'];

                $validator = new Validator();
                $validator->validation_rules(array(
                    'country' => 'required',
                    'category' => 'required',
                    'proxies' => 'required',
                ));

                $validator->filter_rules(array(
                    'country' => 'trim',
                    'category' => 'trim',
                    'proxies' => 'trim',
                    'days' => 'trim',
                    'start-date' => 'trim',
                ));

                $validated_data = $validator->run($formData);

                if ($validated_data === false) {

                    $errors = $validator->get_errors_array();

                    $params['posted_data'] = $formData;
                    $params['errors'] = $errors;

                    $this->output('dashboard/admin/plan-add.html.twig',$params);

                    return false;

                } else {

                    $formParams = [
                        'country' => $validated_data['country'],
                        'category' => $validated_data['category'],
                        'count' => (int)$validated_data['proxies'],
                        'days' => isset($validated_data['days']) && !empty($validated_data['days']) ? (int)$validated_data['days'] : 30,
                        'start-date' => !empty($validated_data['start-date']) && isset($validated_data['start-date']) ? $validated_data['start-date'] : 'NOW',
                    ];

                    $ret = $this->getApi()->getUser()->addPlan($bpId, $formParams);

                    if (!isset($ret['error'])) {

                        $params['success_msg'] = 'Plan added successfully';

                    } else {

                        $params = $ret;

                    }
                    $params['posted_data'] = $formParams;
                    $params['posted_data']['proxies'] = $params['posted_data']['count'];

                }

            }

        }

        $params['username'] = $uData['username'];

        $this->output('dashboard/admin/plan-add.html.twig',$params);

    }

    /**
     * Edit plan action
     * @route /admin/plans/edit/{bpId}/{country}/{category}
     * @param $bpId
     * @param $country
     * @param $category
     */
    function editPlanAction($bpId,$country,$category) {

        $locations = $this->getApi()->getInfo()->getLocations();

        $selectionCountries = [];
        $selectionCategories = [];

        foreach($locations as $loc=>$regions) {
            $selectionCountries[$loc] = $loc;
            foreach($regions as $region=>$categories) {
                foreach($categories as $cat=>$data) {
                    $selectionCategories[$cat] = $cat;
                }
            }
        }
        $params['countries'] = $selectionCountries;
        unset($params['countries']['intl']);
        $params['categories'] = $selectionCategories;

        $requestData = $this->getRequest()->request->all();

        $this->getUserRepo()->loadUserByBpId($bpId);
        $uData = $this->getUserRepo()->all();

        if(isset($requestData['action'])) {

            if($requestData['action'] == 'edit_plan') {

                $formData = $requestData['plan'];

                $validator = new Validator();
                $validator->validation_rules(array(
                    'country' => 'required',
                    'category' => 'required',
                    'proxies' => 'required',
                ));

                $validator->filter_rules(array(
                    'country' => 'trim',
                    'category' => 'trim',
                    'proxies' => 'trim',
                    'days' => 'trim',
                    'start-date' => 'trim',
                ));

                $validated_data = $validator->run($formData);

                if ($validated_data === false) {

                    $errors = $validator->get_errors_array();

                    $params['posted_data'] = $formData;
                    $params['errors'] = $errors;

                    $this->output('dashboard/admin/plan-edit.html.twig',$params);

                    return false;

                } else {

                    $formParams = [
                        'country' => $validated_data['country'],
                        'category' => $validated_data['category'],
                        'count' => (int)$validated_data['proxies'],
                        'days' => isset($validated_data['days']) && !empty($validated_data['days']) ? (int)$validated_data['days'] : 30,
                        'start-date' => !empty($validated_data['start-date']) && isset($validated_data['start-date']) ? $validated_data['start-date'] : 'NOW',
                    ];

                    $ret = $this->getApi()->getUser()->updatePlan($bpId, $formParams);

                    if (!isset($ret['error'])) {

                        $params['success_msg'] = 'Plan updated successfully';

                    } else {

                        $params = $ret;

                    }
                    $params['posted_data'] = $formParams;
                    $params['posted_data']['proxies'] = $params['posted_data']['count'];

                }

            }

        }

        $params['username'] = $uData['username'];

        $userData = $this->getApi()->getUser()->get($bpId);
        $plans = [];

        foreach($userData['plans'] as $plan) {
            $plans[$plan['country']][$plan['category']] = $plan;
        }
        $plan = $plans[$country][$category];

        $params['plan'] = [
            'country' => $country,
            'category' => $category,
            'count' => (int)$plan['count'],
            'days' => isset($plan['days']) && !empty($plan['days']) ? (int)$plan['days'] : 30,
            'start-date' => !empty($plan['start-date']) && isset($plan['start-date']) ? $plan['start-date'] : 'NOW',
        ];
        $params['plan']['proxies'] = $params['plan']['count'];
        $params['posted_data'] = $params['plan'];

        $this->output('dashboard/admin/plan-edit.html.twig',$params);

    }

    /**
     * Delete plan action
     * @route /admin/plans/delete/{bpId}/{country}/{category}
     * @param $bpId
     * @param $country
     * @param $category
     */
    function cancelPlanAction($bpId,$country,$category) {

        $planRepo = new UserPlanRepo($this->getDatabase());

        $oldPlanData = $planRepo->loadPlanByCriteria($bpId, $country, $category);

        $commonPlan = new Plan($this);
        $commonPlan->updatePlan($oldPlanData['payment_agreement_id'], 'Cancelled', 'old');

        if (isset($ret['error']))
            $this->redirectWithNotification('/admin/plans', 'error', $ret['error_msg']);

        $this->redirectWithNotification('/admin/plans', 'success', 'Plan deleted successfully');

    }

    /**
     * Delete plan action
     * @route /admin/plans/purge/{bpId}/{country}/{category}
     * @param $bpId
     * @param $country
     * @param $category
     */
    function purgePlanAction($bpId,$country,$category) {

        $requestData = $this->getRequest()->request->all();
        $deletedPlansCount = 0;

        if (isset($requestData['action']) && $requestData['action'] == 'bulk_action') {

            $deletedPlans = [];

            foreach ($requestData['bulk_actions'] as $country => $category) {

                $isDeleted = $this->deletePlan($bpId, $country, $category);
                $this->getApi()->getUser()->deletePlan($bpId, $country, $category);

                if ($isDeleted) {

                    $deletedPlansCount += 1;
                    $deletedPlans[] = "{$country}-{$category}";

                }

            }

            if ($deletedPlansCount === count($requestData['bulk_actions'])) {

                $this->redirectWithNotification('/admin/plans', 'success', 'Plan(s) deleted successfully');

            } else {
                $this->redirectWithNotification('/admin/plans', 'error', 'Only deleted following plans: '.implode(', '.$deletedPlans));
            }

        } else {

            $isDeleted = $this->deletePlan($bpId, $country, $category);
            $this->getApi()->getUser()->deletePlan($bpId, $country, $category);

            if ($isDeleted) {

                $this->redirectWithNotification('/admin/plans', 'success', 'Plan deleted successfully');

            } else {
                $this->redirectWithNotification('/admin/plans', 'error', "Could not fully delete plan {$country}-{$category}");
            }

        }

        $this->getUserRepo()->loadUserByBpId($bpId);
        $uData = $this->getUserRepo()->all();
        $bpData = $this->getApi()->getUser()->get($bpId);

        $uData['roles'] = explode(',',$uData['roles']);

        $params['user_data'] = $uData;
        $params['bp_data'] = $bpData;

        $this->output('dashboard/admin/plan-manage.html.twig',$params);

    }

    /**
     * Detele user action
     * @param $bpId
     */
    function deleteUserAction($bpId) {

        // get plan repo
        $userPlanRepo = new UserPlanRepo($this->getDatabase());

        // get plan data from database
        $userPlanRepo->loadPlanByUserId($bpId,'active');
        $plansData = $userPlanRepo->all();

        if($plansData)
            foreach($plansData as $planData) {
                try {

                    $this->updateSystemPlan($planData['payment_plan_id'],'deleted');

                    // get paypal plan object
                    $paymentPlan = $this->getPaymentApi()->getApi('GetPlan')->get($planData['payment_plan_id']);

                    if($paymentPlan) {

                        // delete old paypal plan
                        $this->getPaymentApi()->getApi('UpdatePlan')->update($paymentPlan,'DELETED');

                    }

                } catch (\Exception $pce) {
                    //@todo log failures so reseller can act on them accordingly
                }

            }

        //@todo API create user delete call
        //$res = $this->getApi()->getUser()->deleteUser($bpId);

        if (!isset($res['error'])) {

            $this->redirectWithNotification('/admin/users', 'success', 'User deleted successfully');

        } else {

            $this->redirectWithNotification('/admin/users', 'error', "Could not fully delete user");

        }
    }

    /**
     * Internal function to deletes plan
     * at blazing proxies and paypal.
     *
     * @param $bpId
     * @param $country
     * @param $category
     * @return bool
     */
    private function deletePlan($bpId, $country, $category){

        // get plan repo
        $userPlanRepo = new UserPlanRepo($this->getDatabase());

        // get plan data from database
        $plansData = $userPlanRepo->loadPlansByCriteria($bpId, $country, $category);

        if($plansData)
            foreach($plansData as $planData) {
                try {

                    $this->updateSystemPlan($planData['payment_plan_id'],'Cancelled');

                    // get paypal plan object
                    $paymentPlan = $this->getPaymentApi()->getApi('GetPlan')->get($planData['payment_plan_id']);

                    if($paymentPlan) {

                        // delete old paypal plan
                        $this->getPaymentApi()->getApi('UpdatePlan')->update($paymentPlan,'DELETED');

                    }
                } catch (\Exception $pce) {
                    //@todo log failures so reseller can act on them accordingly
                }

            }

        return true;

    }

    /**
     * Delete plan from app system and at blazing proxies
     * @param $planId
     * @param $internalState
     * @return bool
     */
    private function updateSystemPlan($planId,$internalState) {

        // get plan repo
        $userPlanRepo = new UserPlanRepo($this->getDatabase());

        // get plan data from database
        $planData = $userPlanRepo->loadPlanByPaymentPlanId($planId);

        if($internalState==='deleted' || $internalState==='inactive') {
            // delete plan at blazing proxies
            $res = $this->getApi()->getUser()->deletePlan($planData['bp_id'], $planData['plan_country'], $planData['plan_category']);
        }
        if($internalState==='active') {
            // delete plan at blazing proxies
            $res = $this->getApi()->getUser()->addPlan($planData['bp_id'], ['country' => $planData['plan_country'], 'category' => $planData['plan_category']]);
        }
        // return error if any
        if(isset($res['error']))
            return $res;

        // update plan state in database
        $planRepo = new PaypalPlanRepo($this->getDatabase());
        $planRepo->updatePlanState($planData['payment_plan_id'], strtoupper($internalState));
        $userPlanRepo->updatePlanStateByPaymentId($planData['payment_plan_id'], $internalState);

        return true;

    }
    /**
     * Adds a plan to system and blazing proxies
     * @param $planData
     * @return mixed
     */
    private function addPreviouslyExistingSystemPlan($planData) {

        $formParams = [
            'country' => $planData['plan_country'],
            'category' => $planData['plan_category'],
            'count' => (int)$planData['plan_count'],
            'days' => isset($planData['plan_days']) && !empty($planData['plan_days']) ? (int)$planData['plan_days'] : 30,
            'start-date' => !empty($planData['plan_start']) && isset($planData['plan_starte']) ? $planData['plan_start'] : 'NOW',
        ];

        $ret = $this->getApi()->getUser()->addPlan($planData['bp_id'], $formParams);
/*
        $planParams = [
            'bp_id'=>$planData['bp_id'],
            'bp_plan_id'=>$planData['bp_plan_id'],
            'payment_type'=>'paypal',
            'payment_plan_id'=>$paypalPlanId,
            'payment_agreement_id'=>$agreementParams['agreement_id'],
            'plan_country'=>$planData['plan']['country'],
            'plan_category'=>$planData['plan']['category'],
            'plan_count'=>$planData['plan']['count'],
            'plan_days'=>isset($planData['plan']['days']) ? $planData['plan']['days'] : 30,
            'plan_price'=>$planData['plan']['price'],
            'plan_start'=>$planStartDateTime,
            'plan_status'=>'active'
        ];*/
        $planParams = $planData;

        // insert plan to app database
        $planRepo = new UserPlanRepo($this->getDatabase());
        $planRepo->insertPlan($planParams);

        if (!isset($ret['error'])) {

            $params['success_msg'] = 'Plan added successfully';

        } else {

            $params['error_msg'] = 'Plan could not be added';

        }

        return $params;

    }
    /**
     * Default action
     * @route /stats
     */
    function statsUsageAction() {

        $params = [
            'plans'=>[]
        ];

        $this->output('dashboard/admin/stats-usage.html.twig',$params);

    }
    /**
     * List paypal plans
     */
    function paypalPlansAction() {

        $page = $this->getRequest()->query->get('page') ? $this->getRequest()->query->get('page') : 1;
        $ipp = $this->getRequest()->query->get('page') ? $this->getRequest()->query->get('ipp') : 10;
        $requestData = $this->getRequest()->request->all();

        if(isset($requestData['action']) && $requestData['action'] == 'search_plan_id') {

            try {

                $agreement = $this->getPaymentApi()->getApi('GetPlan')->get($requestData['plan_id']);

                $thisAgreement['id'] = $agreement->getId();
                $thisAgreement['state'] = $agreement->getState();
                $thisAgreement['name'] = $agreement->getName();
                $thisAgreement['description'] = $agreement->getDescription();
                $thisAgreement['created_on'] = $agreement->getCreateTime();
                $thisAgreement['type'] = $agreement->getType();
                $agreementsContainer[] = $thisAgreement;

                $params = Paging::get($ipp,$page,$this->getRouter()->getRoutes()->get('admin_paypal_plans')->getPath(),$agreementsContainer,1);

            } catch (\Exception $pce) {
                $params['error_msg'] = 'Plan not found at Paypal. Maybe deleted?';
            }

            $params['posted_data']['plan_id'] = $this->getRequest()->request->get('plan_id');

        } else {

            $state = $this->getRequest()->query->get('state') ? $this->getRequest()->query->get('state') : 'ACTIVE';

            $agreements = $this->getPaymentApi()->getApi('ListPlans')->get(['status'=>$state,'page'=>(int)$page-1,'page_size'=>(int)$ipp,'total_required'=>'yes']);

            $agreementsContainer = [];

            if($agreements->getPlans()) {

                foreach($agreements->getPlans() as $agreement) {
                    $thisAgreement['id'] = $agreement->getId();
                    $thisAgreement['state'] = $agreement->getState();
                    $thisAgreement['name'] = $agreement->getName();
                    $thisAgreement['description'] = $agreement->getDescription();
                    $thisAgreement['created_on'] = $agreement->getCreateTime();
                    $thisAgreement['type'] = $agreement->getType();
                    $agreementsContainer[] = $thisAgreement;
                }
            }

            // sort by date desc
            $agreementsContainer = array_reverse($agreementsContainer);

            $params = Paging::get($ipp,$page,$this->getRouter()->getRoutes()->get('admin_paypal_plans')->getPath().'?state='.$state,$agreementsContainer,(int)$agreements->getTotalItems());
            $params['state'] = ucfirst($state);

        }

        $this->output('dashboard/admin/paypal-plans.html.twig',$params);

    }

    /**
     * Update paypal plan
     * @param string $planId Paypal plan id
     * @param string $state created|deleted|inactive|active
     */
    function paypalUpdatePlanAction($planId,$state) {

        $createdPlan = $this->getPaymentApi()->getApi('GetPlan')->get($planId);
        $result = null;
        $action = '';
        // get plan repo
        $userPlanRepo = new UserPlanRepo($this->getDatabase());
        // get plan data from database
        $planData = $userPlanRepo->loadPlanByPaymentPlanId($planId);

        switch($state) {
            case 'active':
                if($planData['plan_status'] == 'inactive') {
                    $userPlanRepo->updatePlanStateByPaymentId($planData['payment_plan_id'], 'active');
                    $this->addPreviouslyExistingSystemPlan($planData);
                }
                $result = $this->getPaymentApi()->getApi('UpdatePlan')->update($createdPlan,$state);
                $action = 'activated';
                break;
            case 'inactive':
                $this->updateSystemPlan($planId,$state);
                $result = $this->getPaymentApi()->getApi('UpdatePlan')->update($createdPlan,$state);
                $action = 'deactivated';
                break;
            case 'deleted':
                $this->deletePlan($planData['bp_id'],$planData['plan_country'],$planData['plan_category']);
                $result = $this->getPaymentApi()->getApi('UpdatePlan')->update($createdPlan,$state);
                $action = 'deleted';
                break;
        }
        if(is_object($result)) {

            // update plan state in database
            $agreementRepo = new PaypalPlanRepo($this->getDatabase());
            $agreementRepo->updatePlanState($planId, 'deleted');

            $this->redirectWithNotification('/admin/payment/paypal/plans', 'success', 'Plan '.$action.' successfully');
        } else {
            $this->redirectWithNotification('/admin/payment/paypal/plans', 'error', 'Plan could not be '.$action);
        }

    }

}