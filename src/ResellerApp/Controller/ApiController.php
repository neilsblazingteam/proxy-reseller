<?php
namespace ResellerApp\Controller;

use ResellerApp\AppFramework;
use ResellerApp\Security\TokenService;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class ApiController
 *
 * Methods for frontend data
 *
 * @package ResellerApp\Controller
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class ApiController extends AppFramework
{

    /**
     * ApiController constructor.
     */
    function __construct() {

        $tokenService = new TokenService();
        $cookie = $this->getRequest()->cookies->get('auth');

        if(is_null($cookie)) {

            new AccessDeniedException('Unauthorized');

        } else {

            $data = $tokenService->getTokenData($cookie);

            if (!$data) {

                new AccessDeniedException('Unauthorized');

            }
        }

        $data = $tokenService->getTokenData($cookie);

        if($data) {

            $this->getUserRepo()->loadUserById($data->aud->user_id);

        } else {

            new AccessDeniedException('Unauthorized');

        }

    }

    /**
     * Get locations container
     */
    function locationsAction() {

        $user = $this->getApi()->getUser()->get($this->getUserRepo()->getBpId());
        $data = $this->getApi()->getHelper()->getAssignedLocationsCount($user['plans']);

        $this->response($data);

    }

    /**
     * Output response
     * @param $data
     */
    function response($data) {

        $this->getResponse()->setContent(json_encode($data));
        $this->getResponse()->headers->set('Content-Type', 'application/json');
        $this->getResponse()->send();

    }
}