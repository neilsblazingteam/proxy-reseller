<?php
namespace ResellerApp\Controller;

use PayPal\Api\WebhookEvent;
use ResellerApp\AppFramework;
use ResellerApp\Common\Plan;
use ResellerApp\Repo\PaypalAgreementRepo;
use ResellerApp\Repo\PaypalIpnRepo;
use ResellerApp\Repo\PaypalPlanRepo;
use ResellerApp\Repo\PaypalWebhookRepo;
use ResellerApp\Repo\UserPlanRepo;
use ResellerApp\Utils\Format;

/**
 * Class PaypalController
 * @package ResellerApp\Controller
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class PaypalController extends AppFramework
{

    /**
     * Event listener action
     * Listens for PayPal events on webhook route
     */
    function webhookAction() {

        /** @var String $bodyReceived */
        $bodyReceived = file_get_contents('php://input');
        // ### Validate Received Event Method
        // Call the validateReceivedEvent() method with provided body, and apiContext object to validate
        try {

            /** @var \PayPal\Api\WebhookEvent $output */
            $output = WebhookEvent::validateAndGetReceivedEvent($bodyReceived, $this->getPaymentApi()->getApiContext());
            $jsonOutput = $output->toJSON();
            //$jsonOutput = $bodyReceived;

            // $output would be of type WebhookEvent
            $result = json_decode($jsonOutput,true);
            // verify webhook id
            $webhookRepo = new PaypalWebhookRepo($this->getDatabase());

            // get agreement id from result
            $agreementId = $result['resource']['id'];

            $params = [
                'event_id'=>$result['id'],
                'event_type'=>$result['event_type'],
                'agreement_id'=>$agreementId,
                'payer_id'=>$result['resource']['payer']['payer_info']['payer_id'],
                'created_on'=>Format::dateDb()
            ];

            // if data found carry on
            if (isset($paypalPlanData['id'])) {

                // add webhook event to database
                $webhookRepo->addWebhookEvent($params);

                $planState = $result['resource']['state'];

                // do custom stuff if necessary
                // loop over resource type to determine what to do
                switch ($result['event_type']) {

                    case 'BILLING.SUBSCRIPTION.UPDATED':
                        if($planState=='Active') {

                            $expiryType = 'new';

                        } else {

                            $expiryType = 'cancel';

                        }
                        break;
                    default:
                        $expiryType = 'cancel';
                        break;

                }

                $this->updatePlan($agreementId,$planState,$expiryType);

            } else {
                $this->logWebhook('App Resource not Found: '.$agreementId.' not found!');
            }

        } catch (\InvalidArgumentException $ex) {
            // This catch is based on the bug fix required for proper validation for PHP. Please read the note below for more details.
            // If you receive an InvalidArgumentException, please return back with HTTP 503, to resend the webhooks. Returning HTTP Status code [is shown here](http://php.net/manual/en/function.http-response-code.php). However, for most application, the below code should work just fine.
            $this->logWebhook('Invalid Arguments: '.$ex->getMessage());
            http_response_code(503);
        } catch (\Exception $ex) {
            $this->logWebhook('Invalid Event: '.$ex->getMessage());
        }

    }

    /**
     * Listen to classic IPN transactions
     */
    function ipnAction() {

        $raw_post_data = file_get_contents('php://input');

        $this->logIpn("Incoming request: " . $raw_post_data);

        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode ('=', $keyval);
            if (count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);
        }

        $req = 'cmd=_notify-validate';
        if(function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }

        $this->logIpn("Sending verify:".$req);

        $result = $this->callApi($req);

        $tokens = explode("\r\n\r\n", trim($result));
        $result = trim(end($tokens));

        // inspect IPN validation result and act accordingly
        if (strcmp ($result, "VERIFIED") == 0) {

            $this->logIpn("Valid request. Response was: " . $result);

            $txn_type = $myPost['txn_type'];
            $agreementId = $myPost['recurring_payment_id'];

            switch ($txn_type) {

                case 'recurring_payment':

                    $payment_status = $myPost['payment_status'];

                    $payerId = isset($myPost['payer_id']) ? $myPost['payer_id'] : $myPost['buyer_id'];
                    $txn_id = $myPost['txn_id'];

                    $ipnRepo = new PaypalIpnRepo($this->getDatabase());

                    $existingTransaction = $ipnRepo->getTransactionById($txn_id);

                    if($payment_status=='Completed' && !$existingTransaction) {

                        $params = [
                            'payment_status'=>$payment_status,
                            'payer_id'=>$payerId,
                            'recurring_payment_id'=>$payerId,
                            'created_on'=>Format::dateDb()
                        ];
                        $ipnRepo->insertTransactionState($params);

                        if($payment_status=='Completed') {

                            $expiryType='new';

                        } else {

                            $expiryType='cancel';

                        }
                        $this->updatePlan($agreementId,$payment_status,$expiryType);
                    }

                    break;
                case 'recurring_payment_profile_cancel':
                    $payment_status = 'Cancelled';
                    $expiryType='cancel';

                    $this->updatePlan($agreementId,$payment_status,$expiryType);
                    break;

                case 'recurring_payment_skipped':
                    break;

                case 'recurring_payment_failed':
                    break;

                case 'recurring_payment_suspended_due_to_max_failed_payment':

                    $payment_status = 'Suspended';
                    $expiryType='now';
                    $this->updatePlan($agreementId,$payment_status,$expiryType);
                    break;


            }

        } else if (strcmp ($result, "INVALID") == 0) {
            $this->logIpn("Invalid request. Response was: " . $result);
        }

    }

    /**
     * Update plan internally
     * @param string $agreementId
     * @param string $planState
     * @param string $expiryType
     */
    private function updatePlan($agreementId,$planState,$expiryType) {


        // init repo
        $paypalPlanRepo = new PaypalPlanRepo($this->getDatabase());
        $paypalAgreementRepo = new PaypalAgreementRepo($this->getDatabase());
        $userPlanRepo = new UserPlanRepo($this->getDatabase());

        // retrieve transaction from db
        $paypalPlanData = $userPlanRepo->loadPlanByPaymentAgreementId($agreementId);

        if($paypalPlanData) {

            if($expiryType=='new') {

                $expiryTime = Plan::getExpiryTime();

            } elseif($expiryType=='now') {

                $expiryTime = time();

            } else {

                $expiryTime = strtotime($paypalPlanData['plan_expiry']);

            }

            // format date db
            $dateTimeExpiry = Format::dateDb($expiryTime);

            // set expiry date for proxies
            $this->getApi()->getUser()->extendPlan($paypalPlanData['bp_id'], $paypalPlanData['plan_country'], $paypalPlanData['plan_category'], $expiryTime);

            // update paypal plan state in database
            $paypalPlanRepo->updatePlanState($paypalPlanData['payment_plan_id'], $planState);

            // update paypal agreement state
            $paypalAgreementRepo->updateAgreement($agreementId, ['agreement_state' => $planState, 'agreement_end_date' => Format::dateDb()]);

            // update user plan state
            $userPlanRepo->updatePlanStateByAgreementId($agreementId, $planState);

            // update user plan expiry
            $userPlanRepo->updatePlan($paypalPlanData['id'], ['plan_expiry'=>$dateTimeExpiry]);

        } else {
            $this->logIpn("Plan not found in database: " . $agreementId);
        }
    }

    /**
     * Log webhook events
     * @param string $message
     */
    private function logWebhook($message) {
        file_put_contents($this->getPath('log').'/payment/paypal/paypal_webhook.log',Format::dateTimeHuman()."|". $message . PHP_EOL,FILE_APPEND);
    }

    /**
     * Log IPN transactions
     * @param string $message
     */
    private function logIpn($message) {
        file_put_contents($this->getPath('log').'/payment/paypal/paypal_ipn.log',Format::dateTimeHuman()."|". $message . PHP_EOL,FILE_APPEND);
    }

    /**
     * Call paypal API (used for IPN)
     * @param string $req
     * @return mixed
     */
    private function callApi($req)
    {
        $ch = curl_init($this->getPaymentApi()->getPaypalEndpoint());
        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        curl_setopt($ch, CURLOPT_CAINFO, $this->getPath('cert') . '/cacert.pem');

        if ( !($res = curl_exec($ch)) ) {
            $this->logIpn("Got [" . curl_error($ch) . "] when processing IPN data");
            //exit;
        }
        curl_close($ch);

        return $res;

    }
}