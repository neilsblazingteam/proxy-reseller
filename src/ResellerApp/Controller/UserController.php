<?php
namespace ResellerApp\Controller;

use ResellerApp\AppFramework;
use ResellerApp\Common\Plan;
use ResellerApp\Repo\UserPlanRepo;
use ResellerApp\Security\TokenService;
use ResellerApp\Utils\Format;
use ResellerApp\Utils\Validator;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class UserController
 *
 * @package ResellerApp\Controller
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class UserController extends AppFramework
{
    protected $user_ext;

    /**
     * UserController constructor.
     *
     * We need to make checks here for many different cases to guide/force user through UI.
     * If he fails a check he will be redirected to a specific action controller.
     *
     * Important: These checks need to be in correct order!
     *
     * 1. User has no plans and is not on payment redirect
     *    => redirect to add plan
     * 2. User has not set authorization information yet
     *    => redirect to auth settings
     * 3. User has one plan only (after register), no assigned proxy and port locations
     *    => redirect to location preferences
     * 4. User has one plan only (after register), no assigned proxy but port locations count matches plan count
     *    => redirect to pending
     * 5. User has one plan only (after register), assigned proxy but not addigned port locations
     *    => redirect to error page
     *
     */
    function __construct()
    {

        $tokenService = new TokenService();
        $cookie = $this->getRequest()->cookies->get('auth');

        if(is_null($cookie)) {

            $response = new RedirectResponse($this->getAppUrl().'/login');
            $response->send();

        } else {

            $data = $tokenService->getTokenData($cookie);

            if(!$data) {

                $response = new RedirectResponse($this->getAppUrl().'/login');
                $response->send();

            }
            // load user from repo
            $this->getUserRepo()->loadUserById($data->aud->user_id);

            $this->user_ext = $this->getApi()->getUser()->get($this->getUserRepo()->getBpId());

            // if admin and user had not been added yet, do it now.
            if(isset($this->user_ext['error'])) {

                $response = new RedirectResponse($this->getAppUrl().'/login');
                $response->send();

            }
            // check if user has assigned any locations
            $assignedProxyLocations = isset($this->user_ext['plans']) ? $this->getApi()->getHelper()->getTotalAssignedProxyLocationsCount($this->user_ext['plans']) : 0;

            // check if user is assigned any ports yet
            $assignedPortLocations = isset($this->user_ext['plans']) ? $this->getApi()->getHelper()->getTotalAssignedPortLocationsCount($this->user_ext['plans']) : 0;

            $route = $this->getRequest()->getPathInfo();

            // check if user has any plan else redirect to add plan
            // while making sure he is not redirected to locations at the same time
            if ($this->user_ext['plans'][0]['count'] == null
                && $route !== '/user/plans/add'
            ) {

                $response = new RedirectResponse($this->getAppUrl() . '/user/plans/add');
                $response->send();

            }

            // Fix for plan ports
            if (!empty($this->user_ext['plans'])) {
                foreach ($this->user_ext['plans'] as &$plan) {
                    if (empty($plan['proxies'])) {
                        continue;
                    }

                    foreach ($plan['proxies'] as &$proxy) {
                        $proxy['port_connect'] = $this->getApi()->getHelper()->getProxyPort($proxy, $this->user_ext['auth_type']);
                    }
                }
            }
            unset($plan, $proxy);

            // if user has plan
            if (count($this->user_ext['plans']) === 1 && $this->user_ext['plans'][0]['count'] != null) {

                // check if user has no auth type set yet
                if($this->user_ext['auth_type'] == null
                    && $route !== '/user/pendingAuthType'
                    && $route !== '/user/location-preferences') {

                    $response = new RedirectResponse($this->getAppUrl() . '/user/pendingAuthType');
                    $response->send();

                }
                // check if user has assigned locations yet else redirect to locations page
                if ($assignedProxyLocations === 0 && $assignedPortLocations === 0
                    && $route !== '/user/location-preferences') {

                    $response = new RedirectResponse($this->getAppUrl() . '/user/location-preferences');
                    $response->send();

                }

                $planCount = count($this->user_ext['plans'][0]);

                // check if user has assigned locations that have not been assignment yet else redirect to pending page
                if ($assignedProxyLocations === 0 && $assignedPortLocations === $planCount
                    && $route !== '/user/pendingLocations') {

                    $response = new RedirectResponse($this->getAppUrl() . '/user/pendingLocations');
                    $response->send();

                }
                // check if something went wrong and show error page if so
                if ($assignedProxyLocations === $planCount && $assignedPortLocations === 0) {

                    $params['user'] = $this->user_ext;
                    $params['error_title'] = 'Unsolvable error';
                    $params['error_content'] = 'A problem occurred while assigning your proxies. Please contact system administrator.';

                    $this->output('dashboard/user/page-error.html.twig', $params);

                }

            }
        }

    }
    /**
     * User pending locations
     * @route /pendingLocations
     */
    function pendingLocationsAction()
    {

        $params['user'] = $this->user_ext;

        $assignedLocations = $this->getApi()->getHelper()->getTotalAssignedProxyLocationsCount($params['user']['plans']);

        if($assignedLocations>0) {

            $response = new RedirectResponse($this->getAppUrl().'/dashboard');
            $response->send();

        } else {
            $this->output('dashboard/user/pending-locations.html.twig', []);
        }


    }
    /**
     * User pending auth type
     * @route /user/pendingLocations
     */
    function pendingAuthTypeAction()
    {
        $params = [];

        // if user has auth type set, redirect to dashboard
        $params['user'] = $this->user_ext;

        if($params['user']['auth_type'] !== null) {

            $response = new RedirectResponse($this->getAppUrl().'/dashboard');
            $response->send();

        }

        $params['user']['has_rotate_proxies'] = $params['user']['plans'][0]['category'] === 'rotate';

            // form submit
        $action = $this->getRequest()->get('action');
        if($action=='save_proxy_settings') {

            $requestData = $this->getRequest()->request->all();

            $params = $this->changeSettings($requestData);

            $params['user'] = $this->user_ext;

            if(isset($params['success'])) {
                $this->redirectWithNotification('/dashboard','success','Authorization settings updated successfully');
            } else {

                $params['settings']['auth_type'] = $requestData['auth_type'];
                if(isset($requestData['auth_ips'])) {
                    foreach($requestData['auth_ips'] as $ip) {
                        $params['settings']['auth_ips'][] = $ip;
                    }
                }
            }


        }

        $this->output('dashboard/user/pending-authtype.html.twig',$params);

    }
    /**
     * User dashboard
     * @route /dashboard
     */
    function indexAction() {

        $params['user'] = $this->user_ext;

        $params['original'] = $params['user'];
        $params['settings'] = $params['user'];
        $params['settings']['auth_type'] = isset($params['settings']['auth_type']) ? $params['settings']['auth_type'] : null;
        $params['settings']['auth_ips'] = isset($params['settings']['ips']) ? $params['settings']['ips'] : null;
        $params['settings']['auth_password'] = isset($params['settings']['password']) ? $params['settings']['password'] : null;

        $params['user']['data'] = $this->getUserRepo()->all();
        $params['plan_location_count'] = $this->getApi()->getHelper()->getAssignedLocationsCount($params['user']['plans']);

        // calculate how many proxies user uses
        $proxyCounter = 0;
        $planCountries = [];
        $planCategories = [];

        foreach($params['user']['plans'] as $plan) {
            $proxyCounter += (int)$plan['count'];
            $planCountries[$plan['country']] = $plan['country'];
            $planCategories[$plan['category']] = $plan['category'];
        }

        $params['user']['plan_countries'] = $planCountries;
        $params['user']['first_country'] = array_values($planCountries)[0];
        $params['user']['first_category'] = array_values($planCategories)[0];
        $params['user']['plan_categories'] = $planCategories;
        $params['user']['total_proxies'] = $proxyCounter;
        $params['user']['total_plans'] = count($params['user']['plans']);

        // do form stuff first so we always pull real info after
        $action = $this->getRequest()->get('action');

        if($action) {

            switch($action) {

                case 'replace_proxy':

                    $replaceIp = $this->getRequest()->get('replace_ip');

                    $answer = $this->getApi()->getUser()->replaceProxy($this->getUserRepo()->getBpId(),['ip'=>$replaceIp]);

                    if(isset($answer['error'])) {
                        $params = $answer;
                    } else {
                        $this->redirectWithNotification('/dashboard','success','Proxy replaced successfully');
                    }
                    break;
                case 'save_proxy_settings':

                    $requestData = $this->getRequest()->request->all();

                    $result = $this->changeSettings($requestData);

                    if(isset($result['success'])) {
                        $this->redirectWithNotification('/dashboard','success','Authorization settings updated successfully');
                    } else {
                        $params['settings'] = $requestData;
                        $params['error_msg'] = isset($result['errors']) ? $result['errors'] : $result['error_msg'];
                    }

                    break;

            }

        }

        $this->output('dashboard/user/dashboard.html.twig',$params);

    }
    /**
     * Export IP table
     * @route /user/export/ips/{$country}/{$category}
     * @param $country
     * @param $category
     */
    function exportIpTableAction($country,$category) {

        $userData = $this->user_ext;

        if(isset($userData['password']) && !is_null($userData['password'])) {
            $proxyList = $this->getApi()->getHelper()->getProxiesForExportFromPlan($userData['plans'],$country,$category,$userData['auth_type'],$userData['username'],$userData['password']);
        } else {
            $proxyList = $this->getApi()->getHelper()->getProxiesForExportFromPlan($userData['plans'],$country,$category,$userData['auth_type']);
        }

        if(!is_null($proxyList)){
            $filename = date('YmdHis')."_export_ip_table_{$country}_{$category}.csv";
            $this->getResponse()->headers->set('Content-Type', 'text/csv');
            $this->getResponse()->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', $filename));
            $this->getResponse()->setContent(implode("\n",$proxyList));
            $this->getResponse()->send();
        } else {

            $this->output('dashboard/user/dashboard.html.twig',['error_msg'=>'Could not generate CSV file']);
        }

    }

    /**
     * Show exported IP action
     * @route /user/export/ips/view/{$country}/{$category}
     * @param $country
     * @param $category
     */
    function exportIpTableViewAction($country,$category) {

        $params = [];
        $userData = $this->user_ext;

        if(isset($userData['password']) && !empty($userData['password'])) {
            $proxyList = $this->getApi()->getHelper()->getProxiesForExportFromPlan($userData['plans'],$country,$category,$userData['auth_type'],$userData['username'],$userData['password']);
        } else {
            $proxyList = $this->getApi()->getHelper()->getProxiesForExportFromPlan($userData['plans'],$country,$category,$userData['auth_type']);
        }

        $params['country'] = $country;
        $params['category'] = $category;
        $params['proxy_list'] = implode("<br />",$proxyList);

        $this->output('dashboard/user/export-proxies.html.twig',$params);

    }
    /**
     * Proxy replacements action
     * @route /user/replacements
     * @todo API currently only supports one ip, should support multiple for bulk
     */
    function replacementsAction() {


        // do form stuff first so we always pull real info after
        $action = $this->getRequest()->get('action');

        $params['textarea_value'] = '';

        if($action) {

            switch($action) {

                case 'replace_proxy':

                    $replaceIp = $this->getRequest()->get('replace_ip');

                    $answer = $this->getApi()->getUser()->replaceProxy($this->getUserRepo()->getBpId(),['ip'=>$replaceIp]);

                    if(isset($answer['error'])) {
                        $params = $answer;
                    } else {

                        $this->redirectWithNotification('/user/replacements','success','Replacement successfully carried out');
                    }
                    break;
                case 'bulk_replace_proxy':

                    $tmpReplaceIps = $this->getRequest()->get('bulk_replace');
                    $params['textarea_value'] = $tmpReplaceIps;
                    $replaceIps = preg_split("/[\r\n,]+/", $tmpReplaceIps, -1, PREG_SPLIT_NO_EMPTY);
                    $replaceIps = array_map('trim',$replaceIps);
                    $errors = [];

                    foreach($replaceIps as $ip) {
                        $answer = $this->getApi()->getUser()->replaceProxy($this->getUserRepo()->getBpId(),['ip'=>$ip]);

                        if(isset($answer['error'])) {
                            $errors[] = $ip.': '.$answer['message'];
                        }
                    }

                    if(count($errors)>0) {
                        $params['error'] = true;
                        $params['message'] = implode('<br/>',$errors);
                    } else {

                        $this->redirectWithNotification('/user/replacements','success','Bulk replacement successfully carried out');

                    }

                    break;

            }

        }
        $params['user'] = $this->user_ext;

        // calculate how many proxies user uses
        $proxyCounter = 0;
        $planCountries = [];
        $planCategories = [];
        $replacements = [];

        foreach($params['user']['plans'] as $plan) {
            $proxyCounter += (int)$plan['count'];
            $planCountries[$plan['country']] = $plan['country'];
            $planCategories[$plan['category']] = $plan['category'];
            $replacements[$plan['country']][$plan['category']] = $plan['replacements_left'];
        }

        $params['user']['plan_countries'] = $planCountries;
        $params['user']['first_country'] = array_values($planCountries)[0];
        $params['user']['plan_categories'] = $planCategories;
        $params['user']['total_proxies'] = $proxyCounter;
        $params['user']['total_plans'] = count($params['user']['plans']);
        $params['user']['replacements_left'] = $replacements;

        $this->output('dashboard/user/user-replacements.html.twig',$params);

    }

    /**
     * Location preferences action
     * @route /user/location-preferences
     */
    function locationPreferencesAction() {

        // do form stuff first so we always pull real info after
        $action = $this->getRequest()->get('action');

        if($action) {

            switch($action) {

                case 'save_location_update':

                    $formData = $this->getRequest()->request->all();
                    $portCounter = 0;
                    $locations = [];

                    foreach($formData['locations'] as $region=>$ports) {

                        if((int)$ports > 0) {
                            $locations[$region] = (int)$ports;
                            $portCounter += (int)$ports;
                        }

                    }

                    $requestData = [
                        'locations'=>$locations
                    ];

                    $answer = $this->getApi()->getUser()->saveLocationPreference($this->getUserRepo()->getBpId(),$formData['country'],$formData['category'],$requestData);

                    if(isset($answer['error'])) {
                        $params = $answer;
                    } else {
                        $params['success_msg'] = 'Location preferences updated successfully';
                    }
                    break;

            }

        }

        $params['user'] = $this->user_ext;
        $params['locations'] = $this->getApi()->getInfo()->getLocations();
        $params['meta_categories'] = $this->getApi()->getInfo()->getMetaCategories();

        $plansData = $this->extractPlansData($params['user']['plans']);
        $params['user']['plans'] = $plansData['plans'];

        // for filter
        $params['user']['plan_countries'] = $plansData['planCountries'];
        $params['user']['plan_categories'] = $plansData['planCategories'];
        $params['user']['first_country'] = array_values($plansData['planCountries'])[0];
        $params['user']['first_category'] = $plansData['countryCategories'][$params['user']['first_country']];

        $this->output('dashboard/user/user-locationpreferences.html.twig',$params);

    }
    /**
     * Settings action
     *
     * Change password and proxy settings.
     *
     * @route /user/settings
     * @return void
     */
    function settingsAction() {

        $params = [];
        // do form stuff first so we always pull real info after
        $action = $this->getRequest()->get('action');

        $settings = $this->user_ext;
        $params['original'] = $settings;
        $params['settings'] = $settings;
        $params['settings']['auth_type'] = isset($params['settings']['auth_type']) ? $params['settings']['auth_type'] : null;
        $params['settings']['auth_ips'] = isset($params['settings']['ips']) ? $params['settings']['ips'] : null;
        $params['settings']['auth_password'] = isset($params['settings']['password']) ? $params['settings']['password'] : null;
        $params['settings']['categories'] = $this->getApi()->getHelper()->getUserCategoriesFromResult($settings);

        if($action) {

            switch($action) {

                case 'save_password':

                    $passwords = $this->getRequest()->request->all();

                    $validator = new Validator();
                    $validator->validation_rules(array(
                        'password_current' => 'required',
                        'password_new' => 'required',
                        'password_new_confirm' => 'required',
                    ));

                    $validator->filter_rules(array(
                        'password_current' => 'trim',
                        'password_new' => 'trim',
                        'password_new_confirm' => 'trim',
                    ));


                    $validated_data = $validator->run($passwords);

                    if ($validated_data === false) {

                        $errors = $validator->get_errors_array();

                        $params = ['errors' => $errors];

                    } else {

                        if (!$validator->checkPasswordHash($validated_data['password_current'], $this->getUserRepo()->getPassword())) {

                            $params['error_msg'] = 'Your current password is incorrect';

                        } else {

                            if ($validated_data['password_new'] !== $validated_data['password_new_confirm']) {

                                $params['error_msg'] = 'New passwords do not match';

                            } else {

                                $hashedPassword = password_hash($passwords['password_new'], PASSWORD_DEFAULT);

                                $res = $this->getUserRepo()->updatePassword($this->getUserRepo()->getId(),$hashedPassword);

                                if($res) {

                                    $this->redirectWithNotification('/user/settings','success','Password updated successfully');

                                }

                            }

                        }

                    }
                    break;
                case 'save_proxy_settings':

                    $requestData = $this->getRequest()->request->all();

                    $result = $this->changeSettings($requestData);

                    if(isset($result['success'])) {
                        $this->redirectWithNotification('/user/settings','success','Authorization settings updated successfully');
                    } else {

                        $params['user'] = $settings;

                        $params['settings'] = $requestData;
                        $params['error_msg'] = $result['error_msg'];
                    }

                    break;

            }

        }

        $this->output('dashboard/user/user-settings.html.twig',$params);

    }

    /**
     * Change user proxy or authorization settings.
     *
     * Since auth ips need to be added separately,
     * the storing of settings is split up into different tasks.
     *
     * @param $data
     * @return array
     */
    private function changeSettings($data) {

        $hasRotate = false;
        $params = [];
        $requestData = [];
        $invalidIps = 0;
        $invalidIpsErrors = [];
        $user = $this->user_ext;

        if(!is_null($user['plans'])) {

            foreach($user['plans'] as $plan) {

                if($plan['category']=='rotate') {
                    $hasRotate = true;
                    break;
                }

            }

        }

        // clean empty keys
        $data['auth_ips'] = array_filter($data['auth_ips']);
        if(isset($data['auth_ips_delete']))
        $data['auth_ips_delete'] = array_filter($data['auth_ips_delete']);

/*
        // delete ip if it is not the users only ip when auth_type ip
        if($data['auth_type'] == 'ip' && isset($data['auth_ips_delete']) && !empty($data['auth_ips'][0]) && count($data['auth_ips_delete']) === count($data['auth_ips'])) {

            $invalidIps = 1;
            $invalidIpsErrors[] = "You cannot delete the only IP to authorize with when authorization type is set to IP";

        }*/
        // delete ip if it is not the users only ip when auth_type ip
        if($data['auth_type'] == 'ip' && isset($data['auth_ips_delete']) && count($data['auth_ips_delete']) === count($data['auth_ips']) && $hasRotate) {

            $invalidIps = 1;
            $invalidIpsErrors[] = "You cannot delete the only IP to authorize with when you have rotating proxies";

        }
        if($invalidIps === 0 && $data['auth_type'] == 'ip' && empty($data['auth_ips'][0])) {
            $invalidIps = 1;
            $invalidIpsErrors[] = "You must provide at least one IP to use your chosen authorization type";

        }
        // validate IPs
        if($invalidIps === 0 && isset($data['auth_ips']) && $data['auth_type'] == 'ip' && !empty($data['auth_ips'][0]))
            foreach($data['auth_ips'] as $ip) {

                if(filter_var($ip, FILTER_VALIDATE_IP) === false) {
                    $invalidIpsErrors[] = "IP {$ip} to add is not a valid IP address";
                }

            }
        // validate delete IPs if any
        if($invalidIps === 0 && $data['auth_type'] == 'ip' && isset($data['auth_ips_delete']) && !empty($data['auth_ips_delete'][0]))
            foreach($data['auth_ips_delete'] as $key=>$switch) {

                $ip = $data['auth_ips'][$key];

                if(filter_var($ip, FILTER_VALIDATE_IP) === false) {
                    $invalidIpsErrors[] = "IP {$ip} to delete is not a valid IP address";
                }

            }

        if(!empty($invalidIpsErrors)) {

            return $params = ['error_msg' => implode('<br/>',$invalidIpsErrors)];

        }
        // set changed variables
        $requestData['rotate_30'] = isset($data['rotate_30']) ? 1 : 0;
        $requestData['rotate_ever'] = isset($data['rotate_ever']) ? 1 : 0;
        if(isset($data['rotation_type']))
            $requestData['rotation_type'] = $data['rotation_type'];
        if(isset($data['auth_type']))
            $requestData['auth_type'] = $data['auth_type'];
        if(isset($data['auth_password']) && $data['auth_type'] == 'pw') {
            if(empty($data['auth_password'])) {
                return ['error_msg' => 'You must provide a password to use your chosen authorization type'];
            }
            $requestData['password'] = $data['auth_password'];
        }

        if(!empty($requestData)) {

            $answer = $this->getApi()->getUser()->saveSettings($this->getUserRepo()->getBpId(),$requestData);

            $invalidIpsErrors = [];


            // if ips marked as deleted
            if(isset($data['auth_ips_delete']) && count($data['auth_ips_delete'])>0 && $data['auth_type'] != 'pw') {
                foreach($data['auth_ips_delete'] as $key=>$switch) {
                    // if marked to delete and found in current IP collection go ahead
                    $ip = $data['auth_ips'][$key];
                    if($switch=='on' && in_array($ip,$user['ips'])) {
                        $answerIp = $this->getApi()->getUser()->deleteAuthIp($this->getUserRepo()->getBpId(), ['ip'=>$ip]);
                        if(isset($answerIp['error'])) {
                            $invalidIpsErrors[] = $answerIp['message'];
                        }
                    }
                }
            } else {

            }


            if(isset($data['auth_ips']) && $data['auth_type'] != 'pw' && !isset($data['auth_ips_delete'])) {

                if (!empty($user['ips']) and is_array($user['ips'])) {

                    // first release all old auth ips for security sake
                    foreach ($user[ 'ips' ] as $ip) {
                        $answerIp = $this->getApi()->getUser()->deleteAuthIp($this->getUserRepo()->getBpId(),
                            ['ip' => $ip]);
                        if (isset($answerIp[ 'error' ])) {
                            $invalidIpsErrors[] = $answerIp[ 'message' ];
                        }
                    }
                }

                // now add new ips
                if(empty($invalidIpsErrors)) {
                    foreach($data['auth_ips'] as $ip) {
                        $answerIp = $this->getApi()->getUser()->addAuthIp($this->getUserRepo()->getBpId(), ['ip'=>$ip]);
                        if(isset($answerIp['error'])) {
                            $invalidIpsErrors[] = $answerIp['message'];
                        }
                    }
                }
            }

            if(isset($answer['error'])) {
                $params['error_msg'] = implode('<br/>', $answer['message']);
            } elseif($invalidIpsErrors) {
                $params = ['errors'=>implode('<br/>',$invalidIpsErrors)];
            } else {
                $params['success'] = 'Settings updated successfully';
            }

        }
        return $params;
    }
    /**
     * Manage plans action
     * @route /user/plans
     */
    function managePlansAction() {

        $plansArr = [];

        $user = $this->user_ext;

        $planRepo = new UserPlanRepo($this->getDatabase());

        foreach($user['plans'] as $plan) {

            $planData = $planRepo->loadPlanByCriteria($this->getUserRepo()->getBpId(),$plan['country'],$plan['category']);
            $plansArr[] = $planData ? $planData : ['plan_country'=>$plan['country'],'plan_category'=>$plan['category']];

        }

        $params = [
            'plans'=>$plansArr
        ];

        $this->output('dashboard/user/plan-manage.html.twig',$params);

    }

    /**
     * Add plan action
     * @route /user/plans/add
     */
    function addPlanAction() {

        $params = [];
        $locations = $this->getApi()->getInfo()->getLocations();

        $selectionCountries = [];
        $selectionCategories = [];

        foreach($locations as $loc=>$regions) {
            $selectionCountries[$loc] = $loc;
            foreach($regions as $region=>$categories) {
                foreach($categories as $cat=>$data) {
                    $selectionCategories[$cat] = $cat;
                }
            }
        }
        $params['countries'] = $selectionCountries;
        unset($params['countries']['intl']);
        $params['categories'] = $selectionCategories;

        $params['locations'] = $this->getApi()->getInfo()->getLocations();
        unset($params['locations']['intl']);
        $pricing = $this->getPricingRepo()->getPrices();
        $tiers = $this->getPricingRepo()->getTiers();

        foreach($pricing as $price) {

            $params['pricing'][$price['country']][$price['category']] = [
                'tier_1'=>$price['price_tier_1'],
                'tier_2'=>$price['price_tier_2'],
                'tier_3'=>$price['price_tier_3'],
                'tier_4'=>$price['price_tier_4'],
                'tier_5'=>$price['price_tier_5'],
                'tier_6'=>$price['price_tier_6'],
            ];

        }

        foreach($tiers as $tier) {
            $params['tiers'][$tier['tier']] = [$tier['from_val'],$tier['to_val']];
        }

        $params['pricing_json'] = json_encode($params['pricing']);
        $params['tiers_json'] = json_encode($params['tiers']);

        $requestData = $this->getRequest()->request->all();

        if (isset($_GET['paymentPlan']) && $_GET['paymentPlan'] == 'true') {

            $token = $_GET['token'];

            // get plan data from session
            $planData = $this->getSession()->get('plan_data');

            $commonPlan = new Plan($this);
            $result = $commonPlan->finalizeNewPlan($token,$planData);

            if(true === $result) {

                $this->redirectWithNotification('/user/plans','success','Congratulations! You successfully added a plan');

            } else {

                $params['error_msg'] = implode('<br/>',$result);

            }

        } elseif (isset($_GET['paymentPlan']) && $_GET['paymentPlan'] == 'false') {

            $params['info_msg'] = 'Plan purchase aborted';

        } else {

            // do form stuff first so we always pull real info after
            $action = $this->getRequest()->get('action');

            if ($action) {

                switch ($action) {

                    case 'add_plan':

                        $params['posted_data'] = $requestData;

                        // check if user has this plan
                        // if so, redirect to plan edit page

                        if (!isset($requestData['plan'])) {

                            $params = ['error_msg' => 'You must provide plan data'];

                        } else {

                            // All good start paypal process
                            $validated_data = $requestData;

                            // Store entered user data to session since we only create users
                            // that are successfully billed by paypal. This happens on redirect
                            // from paypal
                            $this->getSession()->set('plan_data', $validated_data);

                            $commonPlan = new Plan($this);

                            $validated_data['email'] = $this->getUserRepo()->getEmail();
                            $commonPlan->createPlan($validated_data,'add');

                            if(isset($result['error']) || isset($result['error_msg'])) {

                                $params['error_msg'] = implode('<br/>',$result);

                            }

                        }
                        break;
                }

            }

        }

        $planRepo = new UserPlanRepo($this->getDatabase());


        $params['user'] = $this->user_ext;
        $params['plan'] = [
            'bp_plan_id' => $planRepo->getBpPlanId(),
            'plan_country' => $planRepo->getPlanCountry(),
            'plan_category' => $planRepo->getPlanCategory(),
            'plan_count' => (int)$planRepo->getPlanCount(),
            'plan_days' => (int)$planRepo->getPlanDays(),
            'plan_price' => $planRepo->getPlanPrice(),
            'plan_start_date' => Format::dateHuman($planRepo->getPlanPrice()),
        ];

        $pricing = $this->getPricingRepo()->getPrices();
        $tiers = $this->getPricingRepo()->getTiers();

        // Filter allowed proxies
        $allowedProxies        = $this->getResellerRepo()->getFromSettings('proxySettings.enabled', []);
        $params[ 'countries' ] = array_keys(array_filter($params[ 'locations' ], function ($data, $key) use ($allowedProxies) {
            return
                // default settings
                !isset($allowedProxies[ 'countries' ]) or
                // enabled
                !empty($allowedProxies[ 'countries' ][ $key ]);
        }, ARRAY_FILTER_USE_BOTH));
        $params['allowed_country_categories'] = $allowedProxies['countries'];

        // Some categories allow to change country after purchase,
        // so they act as category over countries, not under countries as other categories
        $params[ 'additional_locations_opts' ] = array_filter($this->getApi()->getInfo()->getMetaCategories(),
            function ($data, $key) use ($allowedProxies) {
                return
                    // default settings
                    !isset($allowedProxies[ 'metaCategories' ]) or
                    // enabled
                    !empty($allowedProxies[ 'metaCategories' ][ $key ]);
            }, ARRAY_FILTER_USE_BOTH);
        $params[ 'countries' ] = array_merge($params[ 'countries' ], array_keys($params[ 'additional_locations_opts' ]));

        foreach($pricing as $price) {

            $params['pricing'][$price['country']][$price['category']] = [
                'tier_1'=>$price['price_tier_1'],
                'tier_2'=>$price['price_tier_2'],
                'tier_3'=>$price['price_tier_3'],
                'tier_4'=>$price['price_tier_4'],
                'tier_5'=>$price['price_tier_5'],
                'tier_6'=>$price['price_tier_6'],
            ];

        }

        foreach($tiers as $tier) {
            $params['tiers'][$tier['tier']] = [$tier['from_val'],$tier['to_val']];
        }

        $params['pricing_json'] = json_encode($params['pricing']);
        $params['tiers_json'] = json_encode($params['tiers']);

        $this->output('dashboard/user/plan-add.html.twig',$params);
    }

    /**
     * Edit plan action
     * @route /user/plans/edit/{country}/{category}
     * @param $country
     * @param $category
     */
    function editPlanAction($country,$category) {

        $params = [];
        $planRepo = new UserPlanRepo($this->getDatabase());
        $planRepo->loadPlanByCriteria($this->getUserRepo()->getBpId(),$country,$category);

        $params['user'] = $this->user_ext;
        $pricing = $this->getPricingRepo()->getPrices();
        $tiers = $this->getPricingRepo()->getTiers();

        foreach($pricing as $price) {

            $params['pricing'][$price['country']][$price['category']] = [
                'tier_1'=>$price['price_tier_1'],
                'tier_2'=>$price['price_tier_2'],
                'tier_3'=>$price['price_tier_3'],
                'tier_4'=>$price['price_tier_4'],
                'tier_5'=>$price['price_tier_5'],
                'tier_6'=>$price['price_tier_6'],
            ];

        }

        foreach($tiers as $tier) {
            $params['tiers'][$tier['tier']] = [$tier['from_val'],$tier['to_val']];
        }

        $params['pricing_json'] = json_encode($params['pricing']);
        $params['tiers_json'] = json_encode($params['tiers']);

        $params['plan'] = [
            'bp_plan_id' => $planRepo->getBpPlanId(),
            'plan_country' => $planRepo->getPlanCountry(),
            'plan_category' => $planRepo->getPlanCategory(),
            'plan_count' => (int)$planRepo->getPlanCount(),
            'plan_days' => (int)$planRepo->getPlanDays(),
            'plan_price' => $planRepo->getPlanPrice(),
            'plan_start_date' => Format::dateHuman($planRepo->getPlanPrice()),
        ];

        $plansData = $this->extractPlansData($params['user']['plans']);
        $params['user']['plans'] = $plansData;
        $params['posted_data'] = $params['plan'];

        $locations = $this->getApi()->getInfo()->getLocations();

        $selectionCountries = [];
        $selectionCategories = [];

        foreach($locations as $loc=>$regions) {
            $selectionCountries[$loc] = $loc;
            foreach($regions as $region=>$categories) {
                foreach($categories as $cat=>$data) {
                    $selectionCategories[$cat] = $cat;
                }
            }
        }
        $params['countries'] = $selectionCountries;
        unset($params['countries']['intl']);
        $params['categories'] = $selectionCategories;

        $params['locations'] = $locations;
        unset($params['locations']['intl']);

        $requestData = $this->getRequest()->request->all();

        if (isset($_GET['paymentPlan']) && $_GET['paymentPlan'] == 'true') {

            $token = $_GET['token'];
            // execute paypal agreement
            $agreement = $this->getPaymentApi()->getApi('ExecuteAgreement')->execute($token);

            if($agreement) {

                // get plan data from session
                $planData = $this->getSession()->get('plan_data');

                $commonPlan = new Plan($this);
                $result = $commonPlan->finalizeChangePlan($token,$planData);

                if(true === $result) {

                    $this->redirectWithNotification('/user/plans/edit/'.$planData['plan']['country'].'/'.$planData['plan']['category'],'success','Your plan was changed successfully');

                } else {

                    $params['error_msg'] = implode('<br/>',$result);

                }

            }

        } elseif (isset($_GET['paymentPlan']) && $_GET['paymentPlan'] == 'false') {

            // get plan data from session
            $planData = $this->getSession()->get('plan_data');
            $this->getSession()->remove('plan_data');
            $this->redirectWithNotification('/user/plans/edit/'.$planData['plan']['country'].'/'.$planData['plan']['category'],'info','Plan change aborted');

        } else {

            // do form stuff first so we always pull real info after
            $action = $this->getRequest()->get('action');

            if ($action) {

                switch ($action) {

                    case 'edit_plan':

                        $validated_data = $requestData;
                        $params['posted_data'] = $validated_data;

                        // check locations
                        $preferredLocationsCount = $this->getApi()->getHelper()->extractLocationsCount($this->getApi(), $requestData['plan'],$requestData['locations']);

                        // any locations assigned
                        if ($preferredLocationsCount == 0) {

                            $params['wizard_step'] = 1;
                            $params['error'] = true;
                            $params['error_msg'] = 'You must assign your locations first';
                            $this->output('dashboard/user/plan-edit.html.twig', $params);
                            return false;

                        }

                        // locations count matches plan proxy count
                        if ($preferredLocationsCount != $validated_data['plan']['count']) {

                            $params['wizard_step'] = 1;
                            $params['error'] = true;
                            $moreLess = $preferredLocationsCount > $validated_data['plan']['count'] ? 'less' : 'more';
                            $params['error_msg'] = 'You must assign '.$moreLess.' locations';
                            $this->output('dashboard/user/plan-edit.html.twig', $params);
                            return false;

                        }

                        // all good start paypal process
                        $validated_data['tiers'] = $params['tiers'];
                        $validated_data['pricing'] = $params['pricing'];

                        if (!isset($requestData['plan'])) {

                            $params = ['error_msg' => 'You must provide plan data'];

                        } else {

                            // Store entered user data to session since we only create users
                            // that are successfully billed by paypal. This happens on redirect
                            // from paypal
                            $this->getSession()->set('plan_data', $validated_data);

                            $commonPlan = new Plan($this);

                            $validated_data['email'] = $this->getUserRepo()->getEmail();
                            $commonPlan->createPlan($validated_data, 'change');

                            if (isset($result['error']) || isset($result['error_msg'])) {

                                $params['error_msg'] = implode('<br/>', $result);

                            }

                        }
                        break;
                }

            }

        }

        $params['additional_locations_opts'] = $this->getApi()->getInfo()->getMetaCategories();

        $this->output('dashboard/user/plan-edit.html.twig',$params);

    }
    /**
     * Cancel plan action
     * @route /user/plans/cancel/{country}/{category}
     * @param $country
     * @param $category
     */
    function cancelPlanAction($country,$category) {

        $planRepo = new UserPlanRepo($this->getDatabase());

        $oldPlanData = $planRepo->loadPlanByCriteria($this->getUserRepo()->getBpId(), $country, $category);

        $commonPlan = new Plan($this);
        $commonPlan->updatePlan($oldPlanData['payment_agreement_id'], 'Cancelled');

        if (isset($ret['error']))
            $this->redirectWithNotification('/user/plans', 'error', $ret['error_msg']);

        $this->redirectWithNotification('/user/plans', 'success', 'Plan deleted successfully');

    }


    /**
     * Extract plan details that APi wont deliver
     * but is needed for UI
     *
     * @param array $plans
     * @return array
     */
    private function extractPlansData($plans) {

        $planCountries = [];
        $planCategories = [];
        $countryCategories = [];

        if(!empty($plans))
            foreach($plans as $key=>$plan) {

                $ports2assign = 0;
                $portsAssigned = 0;
                $proxies2assign = 0;
                $proxiesAssigned = 0;
                $plans[$key]['assigned_locations'] = [];

                // count assigned locations
                if(isset($plan['proxies']) && !empty($plan['proxies']))
                    foreach($plan['proxies'] as $proxy) {
                        $plusOrZeroPort = $proxy['port_location'] !== null ? 1 : 0;
                        $plusOrZeroProxy = $proxy['proxy_location'] !== null ? 1 : 0;
                        @$plans[$key]['assigned_port_locations'][$proxy['port_location']] += $plusOrZeroPort;
                        @$plans[$key]['assigned_proxy_locations'][$proxy['proxy_location']] += $plusOrZeroProxy;
                        if($plusOrZeroPort === 0) {
                            $ports2assign += 1;
                        } else {
                            $portsAssigned += 1;
                        }
                        if($plusOrZeroProxy === 0) {
                            $proxies2assign += 1;
                        } else {
                            $proxiesAssigned += 1;
                        }
                    }
                // for filtering
                $planCountries[$plan['country']] = $plan['country'];
                $planCategories[$plan['category']] = $plan['category'];
                $countryCategories[$plan['country']] = $plan['category'];
                // template ui rules
                $plans[$key]['allowed_assign_locations'] = $plan['category'] !== 'rotate';
                $plans[$key]['port_assignments_left'] = $ports2assign;
                $plans[$key]['ports_assigned'] = $portsAssigned;
                $plans[$key]['proxy_assignments_left'] = $proxies2assign;
                $plans[$key]['proxies_assigned'] = $proxiesAssigned;

            }
        return ['plans'=>$plans,'planCountries'=>$planCountries,'planCategories'=>$planCategories,'countryCategories'=>$countryCategories];
    }

}