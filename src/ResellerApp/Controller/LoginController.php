<?php
namespace ResellerApp\Controller;

use ResellerApp\AppFramework;
use ResellerApp\Security\TokenService;
use ResellerApp\Utils\Validator;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LoginController
 *
 * @package ResellerApp\Controller
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class LoginController extends AppFramework
{

    private $baseParams;
    private $template = 'login.html.twig';

    /**
     * Default action
     * @route /login
     */
    function indexAction()
    {
        // paypal is the last step on installation pages,
        // so, if this configuration wasn't created, it means installation not completed
        if (!is_file($this->getPath('config') . '/paypal.php')) {
            $this->redirectToInstallationPage();
        }

        $params = [];
        $this->output($this->getTemplate(), $params);

    }

    /**
     * Login action
     * @route /login_action
     * @param Request $request
     * @return RedirectResponse
     */
    function loginAction(Request $request)
    {

        $params = [];
        $formValues = $request->request->all();

        if ($formValues) {

            $validator = new Validator();
            $validator->validation_rules(array(
                'username' => 'required',
                'password' => 'required',
            ));

            $validator->filter_rules(array(
                'username' => 'trim|sanitize_string',
                'password' => 'trim|sanitize_string',
            ));

            $validated_data = $validator->run($formValues);

            $params = [];

            if ($validated_data === false) {

                $errors = $validator->get_errors_array();
                $params = ['errors' => $errors, 'posted_data' => $formValues];

            } else {

                $this->getUserRepo()->loadUserByUsername($validated_data['username']);

                $user = $this->getUserRepo();

                if($validator->checkPasswordHash($validated_data['password'],$user->getPassword())) {

                    $tokenService = new TokenService();

                    $token = $tokenService->generateToken($this->getAppUrl(),['user_id'=>$user->getId()]);
                    $cookie = new Cookie('auth',$token);

                    if($cookie && in_array('ROLE_ADMIN',$user->getRoles())) {
                        $response = new RedirectResponse($this->getAppUrl().'/admin/dashboard');
                        $response->headers->setCookie($cookie);
                        $response->send();
                    }
                    if($cookie && in_array('ROLE_USER',$user->getRoles()) && $user->getState() == 'active') {
                        $response = new RedirectResponse($this->getAppUrl().'/dashboard');
                        $response->headers->setCookie($cookie);
                        $response->send();
                    }

                }
                $params = ['error_msg' => 'Incorrect username or password', 'posted_data' => $formValues];
            }
        }

        $this->output($this->getTemplate(), $params);
    }

    /**
     * Login action
     * @route /logout_action
     * @param Request $request
     * @return RedirectResponse
     */
    function logoutAction(Request $request)
    {

        $cookie = new Cookie('auth','',time()-86400);
        $response = new RedirectResponse($this->getAppUrl().'/login');
        $response->headers->setCookie($cookie);
        $response->send();

    }
    /**
     * @return array
     */
    public function getBaseParams()
    {
        return $this->baseParams;
    }

    /**
     * @param array $baseParams
     */
    public function setBaseParams($baseParams)
    {
        $this->baseParams = $baseParams;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }
}