<?php
namespace ResellerApp\Controller;

use ResellerApp\AppFramework;
use ResellerApp\Security\TokenService;
use ResellerApp\Utils\Paging;
use ResellerApp\Utils\Validator;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class MessageController
 * @package ResellerApp\Controller
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class MessageController extends AppFramework
{

    /**
     * MessageController constructor.
     */
    public function __construct()
    {
        $tokenService = new TokenService();
        $cookie = $this->getRequest()->cookies->get('auth');

        if(is_null($cookie)) {

            $response = new RedirectResponse($this->getAppUrl().'/login');
            $response->send();

        } else {

            $data = $tokenService->getTokenData($cookie);

            if(!$data) {

                $response = new RedirectResponse($this->getAppUrl().'/login');
                $response->send();

            }
            $this->getUserRepo()->loadUserById($data->aud->user_id);

        }

    }

    /**
     * Message inbox action
     * @return void
     */
    public function indexAction() {

        $params = [];
        $requestData = $this->getRequest()->request->all();

        if(isset($requestData['action']) && $requestData['action'] == 'bulk_action') {

            $res = $this->purgeBulk($requestData['delete_ids']);

            if($res) {
                $this->redirectWithNotification('/user/messages/inbox','success','Message moved to trash');
            } else {
                $params['error_msg'] = 'Could not delete messages';
            }

        } else {

            $messages = $this->getMessageRepo()->getIngoingMessages($this->getUserRepo()->getId(),[20,0]);
            $messagesCount = $this->getMessageRepo()->getIngoingMessagesCount($this->getUserRepo()->getId());
            $unreadMessagesCount = $this->getMessageRepo()->getUnreadMessagesCount($this->getUserRepo()->getId());
            $ipp = $this->getRequest()->query->get('page') ? $this->getRequest()->query->get('ipp') : 10;
            $page = $this->getRequest()->query->get('page') ? $this->getRequest()->query->get('page') : 1;

            $params = Paging::get($ipp,$page,'/user/messages/inbox',$messages);
            $params['ingoing_messages_count'] = $messagesCount;
            $params['unread_messages_count'] = $unreadMessagesCount;

        }

        $this->output('dashboard/user/message-inbox.html.twig',$params);

    }
    /**
     * Message outbox action
     * @return void
     */
    public function outboxAction() {

        $messages = $this->getMessageRepo()->getOutgoingMessages($this->getUserRepo()->getId(),[20,0]);
        $messagesCount = $this->getMessageRepo()->getOutgoingMessagesCount($this->getUserRepo()->getId());
        $ipp = $this->getRequest()->query->get('page') ? $this->getRequest()->query->get('ipp') : 10;
        $page = $this->getRequest()->query->get('page') ? $this->getRequest()->query->get('page') : 1;

        $params = Paging::get($ipp,$page,'/user/messages/outbox',$messages);
        $params['outgoing_messages_count'] = $messagesCount;

        $this->output('dashboard/user/message-outbox.html.twig',$params);

    }
    /**
     * Message trash action
     * @return void
     */
    public function trashAction() {

        $params = [];
        $requestData = $this->getRequest()->request->all();

        if(isset($requestData['action']) && $requestData['action'] == 'bulk_action') {

            $res = $this->purgeBulk($requestData['purge_ids']);

            if($res) {
                $this->redirectWithNotification('/user/messages/trash','success','Message permanently deleted');
            } else {
                $params['error_msg'] = 'Could not delete message';
            }

        } else {

            $messages = $this->getMessageRepo()->getDeletedMessages($this->getUserRepo()->getId(),[20,0],1);
            $messagesCount = $this->getMessageRepo()->getDeletedMessagesCount($this->getUserRepo()->getId());

            // set paging variables
            $ipp = $this->getRequest()->query->get('page') ? $this->getRequest()->query->get('ipp') : 10;
            $page = $this->getRequest()->query->get('page') ? $this->getRequest()->query->get('page') : 1;

            $params = Paging::get($ipp,$page,'/user/messages/outbox',$messages);
            $params['deleted_messages_count'] = $messagesCount;

        }

        $this->output('dashboard/user/message-trash.html.twig',$params);

    }

    /**
     * View message action
     * @param int $id
     */
    public function messageIngoingAction($id) {

        $message = $this->getMessageRepo()->getIngoingMessageById($id,$this->getUserRepo()->getId());

        if($message['is_read_receiver'] == 0) {
            $this->getMessageRepo()->markRead($id,$this->getUserRepo()->getId());
        }

        $params['is_ingoing_message'] = true;
        $params['message'] = $message;

        $this->output('dashboard/user/message-view.html.twig',$params);

    }

    /**
     * View message action
     * @param int $id
     */
    public function messageOutgoingAction($id) {

        $message = $this->getMessageRepo()->getOutgoingMessageById($id,$this->getUserRepo()->getId());

        if($message['is_read_receiver'] == 0) {
            $this->getMessageRepo()->markRead($id,$this->getUserRepo()->getId());
        }

        $params['is_outgoing_message'] = true;
        $params['message'] = $message;

        $this->output('dashboard/user/message-view.html.twig',$params);

    }

    /**
     * Reply message action
     * @param int $id
     * @return void
     */
    public function replyAction($id) {

        // get message to reply to
        $message = $this->getMessageRepo()->getIngoingMessageById($id,$this->getUserRepo()->getId());

        $params['message'] = $message;

        // store request
        $data = $this->getRequest()->request->all();

        if(isset($data['action']) && $data['action'] == 'reply_message') {

            $params['message'] = [
                'subject'=>$data['contact_subject'],
                'message'=>$data['contact_message']
            ];
            $validator = new Validator();
            $validator->validation_rules(array(
                'contact_subject' => 'required',
                'contact_message' => 'required',
            ));

            $validator->filter_rules(array(
                'contact_subject' => 'trim',
                'contact_message' => 'trim',
            ));

            $validated_data = $validator->run($data);

            if ($validated_data === false) {

                $errors = $validator->get_errors_array();

                $params = ['errors' => $errors];

            } else {

                // insert message to database
                $res = $this->getMessageRepo()->insertMessage($this->getUserRepo()->getId(),$message['sender'],$validated_data['contact_subject'],$validated_data['contact_message']);

                if($res) {
                    $this->redirectWithNotification('/user/messages/inbox','success','Message successfully sent');
                } else {
                    $params['error_msg'] = 'Could not send message';
                }


            }

        }

        $this->output('dashboard/user/message-reply.html.twig',$params);

    }
    /**
     * Delete message from inbox action
     * @param int $id
     */
    public function deleteIngoingAction($id) {

        $this->getMessageRepo()->markDeletedIngoing($id,$this->getUserRepo()->getId());

        $response = new RedirectResponse($this->getAppUrl().'/user/messages/inbox');
        $response->send();

    }
    /**
     * Delete message from outbox action
     * @param int $id
     */
    public function deleteOutgoingAction($id) {

        $this->getMessageRepo()->markDeletedOutgoing($id,$this->getUserRepo()->getId());

        $response = new RedirectResponse($this->getAppUrl().'/user/messages/outbox');
        $response->send();

    }
    /**
     * Delete message in bulk
     * @param array $ids
     * @return bool
     * @todo implement the button
     */
    private function deleteBulk($ids) {

        $res = [];

        foreach($ids as $id=>$switch) {
            if($switch == 'on')
                $res[$id] = $this->getMessageRepo()->markDeleted($id,$this->getUserRepo()->getId());
        }

        return !empty($res);

    }
    /**
     * Purge messages
     * @param array $ids
     * @return bool
     */
    private function purgeBulk($ids) {

        $res = [];

        foreach($ids as $id=>$switch) {
            if($switch == 'on')
            $res[$id] = $this->getMessageRepo()->markPurged($id,$this->getUserRepo()->getId());
        }

        return !empty($res);

    }

    /**
     * Compose message action
     * @return void
     */
    function composeAction() {

        $params = [];
        $data = $this->getRequest()->request->all();

        if(isset($data['action']) && $data['action'] == 'send_message') {

            $validator = new Validator();
            $validator->validation_rules(array(
                'contact_subject' => 'required',
                'contact_message' => 'required',
            ));

            $validator->filter_rules(array(
                'contact_subject' => 'trim',
                'contact_message' => 'trim',
            ));


            $validated_data = $validator->run($data);

            if ($validated_data === false) {

                $errors = $validator->get_errors_array();

                $params = ['errors' => $errors];


            } else {

                $res = $this->getMessageRepo()->insertMessage($this->getUserRepo()->getId(),1,$validated_data['contact_subject'],$validated_data['contact_message']);

                if($res) {
                    $this->redirectWithNotification('/user/messages/compose','success','Message successfully sent');
                } else {
                    $params['error_msg'] = 'Could not send message';
                }

            }

        }

        $this->output('dashboard/user/message-compose.html.twig',$params);

    }

}