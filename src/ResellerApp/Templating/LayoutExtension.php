<?php
namespace ResellerApp\Templating;

use ResellerApp\AppFramework;
use ResellerApp\Utils\Format;

/**
 * Class LayoutExtension
 * Custom template functions can be added here.
 * @package ResellerApp\Templating
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class LayoutExtension extends \Twig_Extension
{

    /**
     * Get template functions
     * @return array
     */
    public function getFunctions()
    {
        $returnArray = array();
        $methods = array(
            'getProxyLabel',
            'getCategoryLabel',
            'getCountryLabel',
            'calcTimeElapsed',
            'convertUserRolesToLabel',
            'convertPlanStateToLabel',
            'convertUserStateToLabel',
            'number',
            'formatDate',
            'formatDateTime',
            'var_dump',
            'in_array',
        );

        foreach ($methods as $methodName) {
            $returnArray[$methodName] = new \Twig_Function_Method($this, $methodName);
        }

        return $returnArray;
    }

    public function getFilters()
    {
        $returnArray = [];
        $methods = [
            'globalVars' => ['is_safe' => ['html']]
        ];

        foreach ($methods as $methodName => $args) {
            $returnArray[$methodName] = new \Twig_Filter_Method($this, $methodName, $args);
        }

        return $returnArray;
    }


    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    function getName()
    {
        return 'proxy_helper';
    }

    /**
     * Check if array
     * @param string $needle
     * @param array $haystack
     * @return bool
     */
    function in_array($needle,$haystack) {

        return in_array($needle,$haystack);

    }

    /**
     * Get human category label
     * @param string $input Technical category
     * @return mixed
     */
    function getCategoryLabel($input) {
        return $this->getProxyLabel($input);
    }

    /**
     * Feeds getCategoryLabel()
     * @param $input
     * @return mixed
     * @todo Replace this function with getCategoryLabel() and remove this function after
     */
    function getProxyLabel($input) {

        static $labels = [
            'static'    => 'Dedicated',
            'dedicated' => 'Dedicated',
            'semi-3'    => 'Semi-Dedicated',
            'rotate'    => 'Rotating',
            'rotating'  => 'Rotating'
        ];

        // Defined or cached
        if (isset($labels[ $input ])) {
            return $labels[ $input ];
        }

        // Load meta-categories
        foreach (AppFramework::getInstance()->getApi()->getInfo()->getMetaCategories() as $id => $data) {
            $labels[$id] = $data['title'];
            $labels[$data['key']] = $data['title'];
        }

        // 2nd try
        if (isset($labels[ $input ])) {
            return $labels[ $input ];
        }

        // Fallback
        return $input;

    }

    /**
     * Get country label
     * @param string $code
     * @return string
     */
    function getCountryLabel($rawCode) {

        $countries = array
        (
            'AF' => 'Afghanistan',
            'AX' => 'Aland Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua And Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BA' => 'Bosnia And Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CG' => 'Congo',
            'CD' => 'Congo, Democratic Republic',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'CI' => 'Cote D\'Ivoire',
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands (Malvinas)',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard Island & Mcdonald Islands',
            'VA' => 'Holy See (Vatican City State)',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran, Islamic Republic Of',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle Of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KR' => 'Korea',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Lao People\'s Democratic Republic',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libyan Arab Jamahiriya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macedonia',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia, Federated States Of',
            'MD' => 'Moldova',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'AN' => 'Netherlands Antilles',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territory, Occupied',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russian Federation',
            'RW' => 'Rwanda',
            'BL' => 'Saint Barthelemy',
            'SH' => 'Saint Helena',
            'KN' => 'Saint Kitts And Nevis',
            'LC' => 'Saint Lucia',
            'MF' => 'Saint Martin',
            'PM' => 'Saint Pierre And Miquelon',
            'VC' => 'Saint Vincent And Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome And Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia And Sandwich Isl.',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard And Jan Mayen',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad And Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks And Caicos Islands',
            'TV' => 'Tuvalu',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'UM' => 'United States Outlying Islands',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela',
            'VN' => 'Viet Nam',
            'VG' => 'Virgin Islands, British',
            'VI' => 'Virgin Islands, U.S.',
            'WF' => 'Wallis And Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe'
        );

        $code = strtoupper($rawCode);

        return isset($countries[$code]) ? $countries[$code] : $rawCode;

    }

    /**
     * Convert multiple roles to icon + role line
     * @param string $input
     * @return string
     */
    function convertUserRolesToLabel($input) {

        $labels = [
            'ROLE_ADMIN'=>'<span class="label label-success">Admin</span>',
            'ROLE_USER'=>'<span class="label label-default">User</span>'
        ];
        $roles = explode(',',$input);
        $roleArr = [];
        foreach($roles as $role) {
            $roleArr[] = isset($labels[$role]) ? $labels[$role] : $input;
        }
        return implode(' ',$roleArr);

    }

    /**
     * Convert plan state to label
     * @param string $input
     * @return mixed
     */
    function convertPlanStateToLabel($input) {

        $labels = [
            'Suspended'=>'<span class="label label-default">Suspended</span>',
            'Complete'=>'<span class="label label-default">Active</span>',
            'Canceled'=>'<span class="label label-default">Canceled</span>',
            'Active'=>'<span class="label label-default">Active</span>'
        ];
        return $labels[$input];

    }

    /**
     * Convert user state to label
     * @param string $input
     * @return mixed
     */
    function convertUserStateToLabel($input) {

        $labels = [
            'suspended'=>'<span class="label label-default">Suspended</span>',
            'active'=>'<span class="label label-default"><i class="fa fa-check"></i> Active</span>',
            'inactive'=>'<span class="label label-default">Inactive</span>',
            'deleted'=>'<span class="label label-default">Deleted</span>'
        ];
        return isset($labels[$input]) ? $labels[$input] : $input;

    }
    /**
     * Format elapsed time
     * @param string $input
     * @return string
     */
    function calcTimeElapsed($input) {

        return Format::getTimeElapsed($input);

    }

    /**
     * Format number
     * @param string|int|float $input
     * @return string
     */
    function number($input) {

        return Format::number($input);

    }

    /**
     * Format date
     * @param string $input
     * @return bool|string
     */
    function formatDate($input) {

        return Format::dateHuman($input);

    }

    /**
     * Format datetime
     * @param string $input
     * @return bool|string
     */
    function formatDateTime($input) {

        return Format::dateTimeHuman($input);

    }

    /**
     * Var dump on the fly
     * @param mixed $input
     */
    function var_dump($input) {
        var_dump($input);
    }

    public function globalVars($vars) {
        $return = '';

        foreach ($vars as $key => $data) {
            // Key must be non numeric
            if (!ctype_digit($key)) {
                $return .= "<div id=\"$key\">" . json_encode($data) . '</div>';
            }
        }

        return trim ($return) ?
            '<div class="hidden" style="display: none">' . $return . '</div>' :
            '';
    }
}