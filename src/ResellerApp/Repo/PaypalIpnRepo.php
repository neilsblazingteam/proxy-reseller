<?php
namespace ResellerApp\Repo;

use ResellerApp\Entity\PaypalPlan;

/**
 * Class PaypalIpnRepo
 * @package ResellerApp\Repo
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class PaypalIpnRepo extends PaypalPlan
{
    /**
     * Get transaction by transaction id
     * @param string $id Paypal transaction id
     * @return mixed
     */
    function getTransactionById($id) {
        return $this->db->select('bp_paypal_ipn')->where('txn_id','=',$id)->pull(1);
    }

    /**
     * Add transaction
     * @param array $data Transaction data
     * @return bool
     */
    function addTransaction($data) {
        return $this->db->insert('bp_paypal_ipn',$data)->go();
    }

    /**
     * Update transaction data
     * @param string $txnId
     * @param string $state
     * @return mixed
     */
    function updateTransactionState($txnId,$state) {
        return $this->db->update('bp_paypal_ipn',['payment_state'=>$state])->where('txn_id','=',$txnId)->go();
    }

    /**
     * Insert transaction state
     * @param array $params
     * @return mixed
     * @todo Remove if not used
     */
    function insertTransactionState($params) {
        return $this->db->insert('bp_paypal_ipn',$params)->go();
    }
}