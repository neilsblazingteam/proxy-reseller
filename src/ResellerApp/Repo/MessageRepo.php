<?php
namespace ResellerApp\Repo;

use ResellerApp\Entity\MessageEntity;
use ResellerApp\Utils\Format;

/**
 * Class MessageRepo
 * @package ResellerApp\Repo
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class MessageRepo extends MessageEntity
{

    /**
     * Insert message to database.
     * @param int $sender
     * @param int $receiver
     * @param string $subject
     * @param string $message
     * @return bool
     */
    function insertMessage($sender,$receiver,$subject,$message)
    {

        return $this->db->insert('bp_messages',[
            'sender'=>$sender,
            'receiver'=>$receiver,
            'subject'=>$subject,
            'message'=>$message,
            'created_on'=>Format::dateDb()
        ])->go();

    }

    /**
     * Mark message as read.
     * @param int $id
     * @param int $uid
     * @return bool
     */
    function markRead($id,$uid) {

        return $this->db->update('bp_messages',['is_read_receiver'=>1])->where('id','=',$id)->and_where('receiver','=',$uid)->go();

    }

    /**
     * Mark message as deleted for sender.
     * @param int $id
     * @param int $uid
     * @return bool
     */
    function markDeletedOutgoing($id,$uid) {

        return $this->db->update('bp_messages',['is_deleted_sender'=>1])->where('id','=',$id)->and_where('sender','=',$uid)->go();

    }


    /**
     * Mark message as deleted for receiver.
     * @param int $id
     * @param int $uid
     * @return bool
     */
    function markDeletedIngoing($id,$uid) {

        return $this->db->update('bp_messages',['is_deleted_receiver'=>1])->where('id','=',$id)->and_where('receiver','=',$uid)->go();

    }

    /**
     * Mark message as important.
     * @param int $id
     * @param int $uid
     * @return bool
     */
    function markImportant($id,$uid) {

        return $this->db->update('bp_messages',['is_important_receiver'=>1])->where('id','=',$id)->and_where('receiver','=',$uid)->go();

    }

    /**
     * Mark message as purged.
     * @param $id
     * @param $uid
     * @return mixed
     */
    function markPurged($id,$uid) {

        return $this->db->update('bp_messages',['is_purged_receiver'=>1])->where('id','=',$id)->and_where('receiver','=',$uid)->go();

    }

    /**
     * Get ingoing messages
     * @param int $id
     * @param array $limit
     * @param int $isDeleted
     * @return array
     */
    function getIngoingMessages($id,$limit=[5,0],$isDeleted=0) {
        $data = $this->db->select('bp_messages AS a','a.id,a.subject,a.message,a.is_read_receiver,a.is_important_receiver,b.username,b.first_name,b.last_name,a.created_on')
            ->left_join('bp_user AS b','a.sender','=','b.id')
            ->where('a.receiver','=',$id)
            ->and_where('a.is_deleted_receiver','=',$isDeleted)
            ->order_by('created_on','DESC')
            ->limit($limit[0],$limit[1])
            ->pull();

        return $data;
    }

    /**
     * Get outgoing messages
     * @param int $id User id
     * @param array $limit Message limit
     * @return mixed
     */
    function getOutgoingMessages($id,$limit=[5,0]) {
        $data = $this->db->select('bp_messages')->where('sender','=',$id)->and_where('is_deleted_sender','=',0)->order_by('created_on','DESC')->limit($limit[0],$limit[1])->pull();
        return $data;
    }
    /**
     * Get deleted messages
     * @param int $id User id
     * @param array $limit Message limit
     * @return mixed
     */
    function getDeletedMessages($id,$limit=[5,0]) {
        $data = $this->db->select('bp_messages')->where('receiver','=',$id)->and_where('is_deleted_receiver','=',1)->and_where('is_purged_receiver','=',0)->order_by('created_on','DESC')->limit($limit[0],$limit[1])->pull();
        return $data;
    }
    /**
     * Get unread messages
     * @param int $id User id
     * @param array $limit Message limit
     * @return mixed
     */
    function getUnreadMessages($id,$limit=[5,0]) {
        $data = $this->db->select('bp_messages')->where('receiver','=',$id)->and_where('is_read_receiver','=',0)->and_where('is_deleted_receiver','=',0)->order_by('created_on','DESC')->limit($limit[0],$limit[1])->pull();
        return $data;
    }
    /**
     * Get unread messages count
     * @param int $id User id
     * @return mixed
     */
    function getUnreadMessagesCount($id) {
        $data = $this->db->select('bp_messages','COUNT(id) AS message_count')->where('receiver','=',$id)->and_where('is_read_receiver','=',0)->order_by('created_on','DESC')->pull(1);
        return $data['message_count'];
    }
    /**
     * Get deleted messages count
     * @param int $id User id
     * @return mixed
     */
    function getDeletedMessagesCount($id) {
        $data = $this->db->select('bp_messages','COUNT(id) AS message_count')->where('receiver','=',$id)->and_where('is_purged_receiver','=',0)->and_where('is_deleted_receiver','=',1)->pull(1);
        return $data['message_count'];
    }
    /**
     * Get ingoing messages count
     * @param int $id User id
     * @return mixed
     */
    function getIngoingMessagesCount($id) {
        $data = $this->db->select('bp_messages','COUNT(id) AS message_count')->where('receiver','=',$id)->and_where('is_deleted_receiver','=',0)->pull(1);
        return $data['message_count'];
    }
    /**
     * Get outgoing messages count
     * @param int $id User id
     * @return mixed
     */
    function getOutgoingMessagesCount($id) {
        $data = $this->db->select('bp_messages','COUNT(id) AS message_count')->where('sender','=',$id)->and_where('is_deleted_sender','=',0)->pull(1);
        return $data['message_count'];
    }

    /**
     * Get message by id
     * @param int $id Message id
     * @param int $uid
     * @return mixed
     */
    function getIngoingMessageById($id,$uid) {
        $data = $this->db->select('bp_messages AS a','a.id,a.subject,a.message,a.sender,a.receiver,a.is_read_receiver,a.is_important_receiver,b.username,b.first_name,b.last_name')
            ->left_join('bp_user AS b','a.sender','=','b.id')->where('a.id','=',$id)->and_where('a.receiver','=',$uid)->pull(1);
        return $data;
    }
    /**
     * Get message by id
     * @param int $id Message id
     * @param int $uid
     * @return mixed
     */
    function getOutgoingMessageById($id,$uid) {
        $data = $this->db->select('bp_messages AS a','a.id,a.subject,a.message,a.sender,a.receiver,a.is_read_receiver,a.is_important_receiver,b.username,b.first_name,b.last_name')
            ->left_join('bp_user AS b','a.receiver','=','b.id')->where('a.id','=',$id)->and_where('a.sender','=',$uid)->pull(1);
        return $data;
    }
}