<?php
namespace ResellerApp\Repo;

use ResellerApp\Entity\PricingEntity;

/**
 * Class PricingRepo
 * @package ResellerApp\Repo
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class PricingRepo extends PricingEntity
{
    /**
     * Get reseller prices
     * @return array
     */
    function getPrices() {
        $data = $this->db->select('bp_pricing_reseller','id,country,category,price_tier_1,price_tier_2,price_tier_3,price_tier_4,price_tier_5,price_tier_6')->group_by('country,category')->pull();
        $this->set($data);
        return $data;
    }

    /**
     * Get original prices
     * @return array
     */
    function getPricesOriginal() {
        $data = $this->db->select('bp_pricing_original','id,country,category,price_tier_1,price_tier_2,price_tier_3,price_tier_4,price_tier_5,price_tier_6')->group_by('country,category')->pull();
        $this->set($data);
        return $data;
    }

    /**
     * Get reseller price by price id
     * @param int $id
     * @return mixed
     */
    function getPrice($id) {
        $data = $this->db->select('bp_pricing_reseller','id,country,category,price_tier_1,price_tier_2,price_tier_3,price_tier_4,price_tier_5,price_tier_6')->where('id','=',$id)->pull(1);
        $this->set($data);
        return $data;
    }
    /**
     * Update price
     * @param int $id
     * @param array $params
     * @return mixed
     */
    function updatePrice($id,$params) {
        return $this->db->update('bp_pricing_reseller',$params)->where('id','=',$id)->go();
    }

    /**
     * Get price tiers
     * @return array
     */
    function getTiers() {
        $data = $this->db->select('bp_pricing_tier','tier,from_val,to_val')->order_by('from_val')->pull();
        $this->set($data);
        return $data;
    }

    /**
     * Get price by county
     * @param string $country
     * @return mixed
     */
    function getPricesByCountry($country) {
        $data = $this->db->select('bp_pricing_reseller','country,category,price_tier_1,price_tier_2,price_tier_3,price_tier_4,price_tier_5,price_tier_6')->where('country','=',$country)->pull(1);
        $this->set($data);
        return $data;
    }

    /**
     * Get prices by multiple countries
     * @param array $countriesCodes
     * @return mixed
     */
    public function getPricesByCountriesCodes(array $countriesCodes = [])
    {
        $data = $this->db
            ->select('bp_pricing_reseller','id,country,category,price_tier_1,price_tier_2,price_tier_3,price_tier_4,price_tier_5,price_tier_6')
            ->where_in('country', $countriesCodes)
            ->group_by('country,category')
            ->pull();

        $this->set($data);
        return $data;
    }

    /**
     * Get price by category
     * @param string $country
     * @param string $category
     * @return mixed
     */
    function getPricesByCategory($country,$category) {
        $data = $this->db->select('bp_pricing_reseller','country,category,price_tier_1,price_tier_2,price_tier_3,price_tier_4,price_tier_5,price_tier_6')->where('country','=',$country)->and_where('category','=',$category)->pull(1);
        $this->set($data);
        return $data;
    }

}