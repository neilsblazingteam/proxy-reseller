<?php
namespace ResellerApp\Repo;

use ResellerApp\Entity\PaypalPlan;

/**
 * Class PaypalAgreementRepo
 * @package ResellerApp\Repo
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class PaypalAgreementRepo extends PaypalPlan
{
    /**
     * Get agreement by id
     * @param string $agreementId
     * @return array
     */
    function getAgreementByPlanId($agreementId) {
        return $this->db->select('bp_paypal_agreement')->where('plan_id','=',$agreementId)->pull(1);
    }
    /**
     * Get user agreements
     * @param int $uid
     * @return array
     */
    function getUserAgreements($uid) {
        return $this->db->select('bp_paypal_agreement')->where('user_id','=',$uid)->pull(1);
    }
    /**
     * Get all agreements
     * @return array
     */
    function getAgreements() {
        return $this->db->select('bp_paypal_agreement')->pull();
    }
    /**
     * Insert agreement
     * @param array $params
     * @return bool
     */
    function insertAgreement($params) {
        return $this->db->insert('bp_paypal_agreement',$params)->go();
    }
    /**
     * Update agreement
     * @param array $params
     * @return bool
     */
    function updateAgreement($id,$params) {
        return $this->db->update('bp_paypal_agreement',$params)->where('agreement_id','=',$id)->go();
    }
}