<?php
namespace ResellerApp\Repo;

use ResellerApp\Entity\PaypalPlan;

/**
 * Class PaypalWebhookRepo
 * @package ResellerApp\Repo
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class PaypalWebhookRepo extends PaypalPlan
{
    /**
     * Get webhook by id
     * @param string $id PayPal webhook id
     * @return array
     */
    function getWebhookById($id) {
        return $this->db->select('bp_paypal_webhook')->where('webhook_id','=',$id)->pull(1);
    }
    /**
     * Add webhook
     * @param array $data New data
     * @return bool
     */
    function addWebhookEvent($data) {
        return $this->db->insert('bp_paypal_webhook_event',$data)->go();
    }

    /**
     * Get webhook event by id
     * @param string $eventId
     * @return array
     */
    function getWebhookEventById($eventId) {
        return $this->db->select('bp_paypal_webhook_event')->where('event_id','=',$eventId)->pull(1);
    }
}