<?php
namespace ResellerApp\Repo;

use ResellerApp\Entity\ResellerEntity;
use ResellerApp\Utils\Format;

/**
 * Class ResellerRepo
 * @package ResellerApp\Repo
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class ResellerRepo extends ResellerEntity
{

    /**
     * Computes the copyright years and shows either
     * the year only or a to - from pattern
     * @return string
     * @todo move this to Utility Helper Class
     */
    function computeCopyrightDate() {

        $yearCurrent = Format::dateHuman(null, 'Y');
        $yearCreated = Format::dateHuman($this->getCreatedOn(), 'Y');

        return $yearCurrent==$yearCreated ? $yearCreated : $yearCurrent.' - '.$yearCreated;

    }

    /**
     * Get reseller API key
     * @return mixed
     */
    function getApiKey() {

        $data = $this->db->select('bp_api_credentials','api_key')
            ->order_by('created_on','DESC')
            ->pull(1);

        return $data['api_key'];

    }

    /**
     * Get app data
     * @return array
     */
    function getAppData() {

        $data = $this->db->select('bp_reseller')->where('id', '=', $this->getId())->pull(1);
        return $data;

    }

    /**
     * Update app data
     * @param array $params
     * @return bool
     */
    function updateAppData($params) {

        return $this->db->update('bp_reseller',$params)->where('id', '=', $this->getId())->go();

    }
    /**
     * Update app terms
     * @param string $content
     * @return bool
     */
    function updateTerms($content) {
        if(!$this->getTerms()) {
            return $this->db->insert('bp_terms',['id'=>1,'terms_content'=>$content])->go();
        } else {
            return $this->db->update('bp_terms',['terms_content'=>$content])->where('id','=',1)->go();
        }
    }
    /**
     * Get terms and conditions
     * @return array
     */
    function getTerms() {

        $data = $this->db->select('bp_terms')->where('id','=',1)->pull(1);
        return $data;

    }

}