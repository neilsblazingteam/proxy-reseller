<?php
namespace ResellerApp\Repo;

use ResellerApp\Entity\UserEntity;
use ResellerApp\Utils\Format;

/**
 * Class UserRepo
 * @package ResellerApp\Repo
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class UserRepo extends UserEntity
{

    /**
     * Get number of users
     * @return int
     */
    function countUsers()
    {
        $data = $this->db->select('bp_user','COUNT(id) AS users')->pull(1);
        return $data['users'];
    }

    /**
     * Inserts password token for 'forgot password'
     * @param string $email
     * @param string $token
     * @return array
     */
    function insertResetPasswordToken($email,$token)
    {
        return $this->db->insert('bp_user_fp',['email'=>$email,'token'=>$token,'created_on'=>Format::dateDb()])->go();
    }

    /**
     * Retrieve email from password
     * @param string $token
     * @return mixed
     */
    function getEmailFromPasswordToken($token)
    {
        return $this->db->select('bp_user_fp','email')->where('token','=',$token)->and_where('is_reset','=',0)->pull(1);
    }

    /**
     * Count users by date range
     * @param string  $start
     * @param string  $end
     * @return array
     */
    function countUsersByDateRange($start,$end)
    {
        $data = $this->db->select('bp_user','COUNT(id) AS users')->where_between('created_on',$start,$end)->pull(1);
        return $data['users'];
    }

    /**
     * Load users and return resut
     * @param array $limit [10,0]
     * @return array
     */
    function loadUsers($limit)
    {
        $data = $this->db->select('bp_user')
        ->limit($limit[0],$limit[1])
        ->pull();
         return $data;
    }

    /**
     * Load user by username and return result
     * @param string $username
     * @return array
     */
    function loadUserByUsername($username)
    {
        $data = $this->db->select('bp_user')->where('username','=',$username)->pull(1);
        $this->set($data);
        return $data;
    }

    /**
     * Load user by email and return result
     * @param string $email
     * @return array
     */
    function loadUserByEmail($email)
    {
        $data = $this->db->select('bp_user')->where('email','=',$email)->pull(1);
        $this->set($data);
        return $data;
    }

    /**
     * Search user by username and return result
     * @param string $username
     * @return array
     */
    function searchUserByUsername($username)
    {
        $ret = $this->db->select('bp_user')->where('username','LIKE','%'.$username.'%')->pull(1);
        return $ret;
    }

    /**
     * Search users by username and return result
     * @param string $username
     * @return array
     */
    function searchUsersByUsername($username)
    {
        $ret = $this->db->select('bp_user')->where('username','LIKE','%'.$username.'%')->pull();
        return $ret;
    }

    /**
     * Load user by internal user id
     * @param int $uid
     * @return array
     */
    function loadUserById($uid)
    {
        $data = $this->db->select('bp_user')->where('id','=',$uid)->pull(1);
        $this->set($data);
        return $data;
    }

    /**
     * Load user by external user id
     * @param int $uid
     * @return array
     */
    function loadUserByBpId($uid)
    {
        $data = $this->db->select('bp_user')->where('bp_id','=',$uid)->pull(1);
        $this->set($data);
        return $data;
    }

    /**
     * Load user by token
     * @param string $token
     * @return array
     */
    function loadUserByToken($token)
    {
        $data = $this->db->select('bp_user')->where('auth_token','=',$token)->pull(1);
        $this->set($data);
        return $data;
    }

    /**
     * Get user by external user id
     * @param $bpId
     * @return mixed
     */
    function getUserByBpId($bpId)
    {
        return $this->db->select('bp_user')->where('bp_id','=',$bpId)->pull(1);
    }

    /**
     * Get user by external user id
     * @param int $uid
     * @param string $password
     * @return bool
     */
    function updatePassword($uid,$password)
    {
        return $this->db->update('bp_user',['passphrase'=>$password])->where('id','=',$uid)->go();
    }

    /**
     * Update password reset
     * @param string $token
     * @param int $status
     * @return bool
     */
    function setPasswordReset($token,$status=1)
    {
        return $this->db->update('bp_user_fp',['is_reset'=>$status])->where('token','=',$token)->go();
    }

    /**
     * Update user and return load new data
     * @param int $uid
     * @param array $params
     */
    function updateUser($uid,$params)
    {
        $this->db->update('bp_user',$params)->where('bp_id','=',$uid)->go();
        $data = $this->db->select('bp_user')->where('bp_id','=',$uid)->pull(1);
        $this->set($data);
    }

    /**
     * Update user by email
     * @param string $email
     * @param array $params
     * @return bool
     */
    function updateUserByEmail($email,$params)
    {
        return $this->db->update('bp_user',$params)->where('email','=',$email)->go();
    }

    /**
     * Insert new user
     * @param array $params
     * @return bool
     */
    function insertUser($params)
    {
        return $this->db->insert('bp_user',$params)->go();
    }

    /**
     * Check if user has specific role
     * @param string $role
     * @return bool
     */
    function hasRole($role) {
        return in_array($role,$this->getRoles());
    }

}