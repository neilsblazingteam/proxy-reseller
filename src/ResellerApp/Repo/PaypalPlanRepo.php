<?php
namespace ResellerApp\Repo;


use ResellerApp\Entity\PaypalPlan;

/**
 * Class PaypalPlanRepo
 * @package ResellerApp\Repo
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class PaypalPlanRepo extends PaypalPlan
{
    /**
     * Get PayPal plan by plan id
     * @param string $planId
     * @return array
     */
    function getPlanByPlanId($planId) {
        return $this->db->select('bp_paypal_plan')->where('plan_id','=',$planId)->pull(1);
    }

    /**
     * Insert new PayPal plan
     * @param array $params
     * @return bool
     */
    function insertPlan($params) {
        return $this->db->insert('bp_paypal_plan',$params)->go();
    }

    /**
     * Update PayPal plan state
     * @param string $id PayPal Plan id
     * @param string $newState PayPal state
     * @return bool
     */
    function updatePlanState($id,$newState) {
        return $this->db->update('bp_paypal_plan',['plan_state'=>$newState])->where('plan_id','=',$id)->go();
    }
}