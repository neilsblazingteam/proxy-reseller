<?php
namespace ResellerApp\Repo;

use ResellerApp\Entity\UserPlan;

/**
 * Class UserPlanRepo
 * @package ResellerApp\Repo
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class UserPlanRepo extends UserPlan
{

    /**
     * @var string
     */
    private $values2fetch = '
    a.id,
    a.bp_id,
    a.bp_plan_id,
    a.plan_count,
    a.plan_country,
    a.plan_category,
    a.payment_plan_id,
    a.payment_type,
    a.payment_plan_id,
    a.payment_agreement_id,
    a.plan_days,plan_price,
    a.plan_start,
    DATE_ADD(a.plan_start, INTERVAL a.plan_days DAY) AS plan_end,
    a.plan_expiry,
    a.plan_status';

    /**
     * Count user plans
     * @return array
     */
    function countPlans()
    {
        $data = $this->db->select('bp_user_plan AS a','COUNT(id) AS plans')
            ->where('plan_status','=','active')
            ->or_where('plan_status','=','completed')
            ->pull(1);
        return $data['plans'];
    }

    /**
     * Count user plans by date range
     * @param string $start
     * @param string $end
     * @return array
     */
    function countPlansByDateRange($start,$end) {
        $data = $this->db->select('bp_user_plan AS a','COUNT(id) AS plans')
            ->where_between('plan_start',$start,$end)
            ->and_where('plan_status','!=','canceled')
            ->and_where('plan_status','!=','deleted')
            ->or_where('plan_status','!=','purged')->pull(1);
        return $data['plans'];
    }

    /**
     * Sum user plan costs
     * @return array
     */
    function sumPlanAmount()
    {
        $data = $this->db->select('bp_user_plan AS a','SUM(plan_price) AS amt')
            ->where('plan_status','!=','canceled')
            ->and_where('plan_status','!=','deleted')
            ->and_where('plan_status','!=','purged')->pull(1);
        return $data['amt'];
    }

    /**
     * Count user plans by date range
     * @param string $start
     * @param string $end
     * @return array
     */
    function sumPlanAmountByDateRange($start,$end)
    {
        $data = $this->db->select('bp_user_plan AS a','SUM(plan_price) AS amt')
            ->where_between('plan_start',$start,$end)
            ->and_where('plan_status','!=','canceled')
            ->and_where('plan_status','!=','deleted')
            ->and_where('plan_status','!=','purged')->pull(1);
        return $data['amt'];
    }

    /**
     * Load all user plans and feed entity
     * @return void
     * @todo do we need this still?
     */
    function loadAllPlans() {
        $data = $this->db->select('bp_user_plan AS a',$this->values2fetch.",CONCAT(b.first_name,' ',b.last_name) AS full_name")
            ->left_join('bp_user AS b','a.bp_id','=','b.bp_id')
            ->where('a.plan_status','!=','Purged')
            ->group_by('a.payment_plan_id')
            ->order_by('a.id','DESC')
            ->pull();
        $this->set($data);
    }

    /**
     * Load all user plans and feed entity
     * @return array
     */
    function loadPlans($limit) {
        $data = $this->db->select('bp_user_plan AS a',$this->values2fetch)
            ->where('a.plan_status','!=','Purged')
            ->group_by('a.payment_plan_id')
            ->order_by('a.id','DESC')
            ->limit($limit[0],$limit[1])
            ->pull();
        return $data;
    }

    /**
     * Load user plans by external id
     * @param int $uid
     * @return array
     */
    function loadPlansByBpId($uid) {
        $data = $this->db->select('bp_user_plan AS a',$this->values2fetch)
            ->where('bp_id','=',$uid)
            ->and_where('plan_status','!=','canceled')
            ->and_where('plan_status','!=','deleted')
            ->and_where('plan_status','!=','purged')
            ->group_by('a.plan_category,a.plan_country')
            ->order_by('a.id','DESC')
            ->pull();
        return $data;
    }

    /**
     * Load single user plan by external plan id
     * @return array
     */
    function loadPlan($pid) {
        $data = $this->db->select('bp_user_plan AS a',$this->values2fetch)
            ->where('bp_plan_id','=',$pid)
            ->pull(1);
        $this->set($data);
        return $data;
    }

    /**
     * Load single user plan by external plan id
     * @return array
     */
    function loadPlanById($id) {
        $data = $this->db->select('bp_user_plan AS a',$this->values2fetch)
            ->where('id','=',$id)
            ->pull(1);
        $this->set($data);
        return $data;
    }

    /**
     * Load single user plan by external user id, country and category
     * @param int $uid
     * @param string $country
     * @param string $category
     * @return array
     */
    function loadPlanByCriteria($uid,$country,$category) {
        $obj = $this->db->select('bp_user_plan AS a',$this->values2fetch)
            ->where('bp_id','=',$uid)
            ->and_where('plan_country','=',$country)
            ->and_where('plan_category','=',$category)
            ->order_by('id','DESC');

        $data = $obj->pull(1);
        $this->set($data);
        return $data;
    }

    /**
     * Load single user plan by external user id, country and category
     * @param int $uid
     * @param string $country
     * @param string $category
     * @return array
     * @note Admin function
     */
    function loadPlansByCriteria($uid,$country,$category) {
        $data = $this->db->select('bp_user_plan AS a',$this->values2fetch)
            ->where('bp_id','=',$uid)
            ->and_where('plan_country','=',$country)
            ->and_where('plan_category','=',$category)
            ->and_where('plan_status','!=','canceled')
            ->and_where('plan_status','!=','deleted')
            ->and_where('plan_status','!=','purged')
            ->group_by('a.payment_plan_id')
            ->pull();
        $this->set($data);
        return $data;
    }

    /**
     * Loads last plan by user id, country and category
     * @param $uid
     * @param $country
     * @param $category
     * @return mixed
     */
    function loadLastPlanByCriteria($uid,$country,$category) {
    $data = $this->db->select('bp_user_plan AS a',$this->values2fetch)
        ->where('bp_id','=',$uid)
        ->and_where('plan_country','=',$country)
        ->and_where('plan_category','=',$category)
        ->and_where('plan_status','!=','canceled')
        ->and_where('plan_status','!=','deleted')
        ->and_where('plan_status','!=','purged')
        ->order_by('plan_start','DESC')
        ->pull(1);
    $this->set($data);
    return $data;
    }

    /**
     * Load plan by external user id
     * @param int $uid
     * @param string $status
     * @return void
     */
    function loadPlanByUserId($uid,$status='')
    {
        $obj = $this->db->select('bp_user_plan AS a',$this->values2fetch)->where('bp_id','=',$uid);
        if(!empty($status))
            $obj->and_where('plan_status','=',$status);
        $data = $obj->pull();
        $this->set($data);
    }

    /**
     * Load plan by payment plan id
     * @param string $paymentPlanId
     * @return array
     */
    function loadPlanByPaymentPlanId($paymentPlanId)
    {
        $data = $this->db->select('bp_user_plan AS a',$this->values2fetch)
            ->where('payment_plan_id','=',$paymentPlanId)->pull(1);
        $this->set($data);
        return $data;
    }

    /**
     * Load plans by payment agreement id
     * @param $agreementId
     * @return array
     */
    function loadPlanByPaymentAgreementId($agreementId)
    {
        $data = $this->db->select('bp_user_plan AS a',$this->values2fetch)->where('payment_agreement_id','=',$agreementId)->pull(1);
        $this->set($data);
        return $data;
    }
    /**
     * Insert plan
     * @param array $params
     * @return bool
     */
    function insertPlan($params) {
        return $this->db->insert('bp_user_plan',$params)->go();
    }

    /**
     * Update plan
     * @param int $id
     * @param array $params
     * @return bool
     */
    function updatePlan($id,$params) {
        return $this->db->update('bp_user_plan',$params)->where('id','=',$id)->go();
    }

    /**
     * Set plan status to 'purged'
     * @param int $uid
     * @param string $country
     * @param string $category
     * @return mixed
     */
    function purgePlan($uid,$country,$category) {
        return $this->db->update('bp_user_plan',['plan_status'=>'Purged'])
            ->where('bp_id','=',$uid)
            ->and_where('plan_country','=',$country)
            ->and_where('plan_category','=',$category)
            ->go();
    }

    /**
     * Update plan state by payment id
     * @param string $id Paypal plan id
     * @param string $newState
     * @return bool
     */
    function updatePlanStateByPaymentId($id,$newState) {
        return $this->db->update('bp_user_plan',['plan_status'=>$newState])->where('payment_plan_id','=',$id)->go();
    }

    /**
     * Update plan state by agreement id
     * @param string $agreementId Paypal agreement id
     * @param string $newState
     * @return bool
     */
    function updatePlanStateByAgreementId($agreementId,$newState) {
        return $this->db->update('bp_user_plan',['plan_status'=>$newState])->where('payment_agreement_id','=',$agreementId)->go();
    }
}