<?php
namespace ResellerApp\Routing;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class Router
 * @package ResellerApp\Routing
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class Router extends RouteCollection
{

    /**
     * @var
     */
    private $parameters;
    /**
     * @var
     */
    private $matcher;
    /**
     * @var
     */
    private $routes;

    /**
     * Router constructor.
     * @param $routeFile
     */
    function __construct($routeFile)
    {
        $this->setupYamlRouting($routeFile);
    }

    /**
     * Reads routing config from yaml file
     * @param $routeFile
     */
    function setupYamlRouting($routeFile) {

        $locator = new FileLocator([$routeFile]);
        $loader = new YamlFileLoader($locator);
        $collection = $loader->load('routes.yml');
        $this->routes = new RouteCollection();
        $this->routes->addCollection($collection);

    }

    /**
     * Get parameters
     * @return mixed
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Set parameters
     * @param array $parameters
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * Get matcher
     * @param $context
     * @return mixed
     */
    public function getMatcher($context)
    {

        $this->matcher = new UrlMatcher($this->routes, $context);
        return $this->matcher;
    }

    /**
     * Get routes
     * @return mixed
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Set routes
     * @param mixed $routes
     */
    public function setRoutes($routes)
    {
        $this->routes = $routes;
    }

}