<?php
namespace ResellerApp\Common;

/**
 * App Helper Class
 * @package ResellerApp\Common
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class Helper
{
    /**
     * Returns an array of prices from db result
     * @param array $result Database result
     * @return array
     */
    function extractPricesFromDatabaseResult($result) {

        $array = [];

        if(!empty($result))
        foreach ($result as $price) {

            $array[$price['country']][$price['category']] = [
                'tier_1' => $price['price_tier_1'],
                'tier_2' => $price['price_tier_2'],
                'tier_3' => $price['price_tier_3'],
                'tier_4' => $price['price_tier_4'],
                'tier_5' => $price['price_tier_5'],
                'tier_6' => $price['price_tier_6'],
            ];

        }

        return $array;

    }

    /**
     * Returns an array of pricing tiers from db result
     * @param array $result Database result
     * @return array
     */
    function extractTiersFromDatabaseResult($result) {

        $array = [];

        foreach ($result as $tier) {
            $array[$tier['tier']] = [$tier['from_val'], $tier['to_val']];
        }

        return $array;

    }
}