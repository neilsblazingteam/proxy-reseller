<?php
namespace ResellerApp\Common;

use PayPal\Api\Agreement;
use PayPal\Api\AgreementTransaction;
use PayPal\Exception\PayPalConnectionException;
use ResellerApp\AppFramework;
use ResellerApp\Repo\PaypalAgreementRepo;
use ResellerApp\Repo\PaypalPlanRepo;
use ResellerApp\Repo\UserPlanRepo;
use ResellerApp\Utils\Format;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class Plan
 * Handles payment plans
 * @package ResellerApp\Common
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class Plan
{
    private $app;

    function __construct(AppFramework $app) {
        $this->app = $app;
    }

    /**
     * Creates a payment plan
     * @param array $data
     * @param string $type
     */
    function createPlan($data,$type='add')
    {

        $trialText = '';
        $createdPlan = null;
        $currency = 'USD';
        $pricing = $this->getApp()->getPricingRepo()->getPrices();
        $tiers = $this->getApp()->getPricingRepo()->getTiers();

        $commonHelper = new Helper();
        $data['pricing'] = $commonHelper->extractPricesFromDatabaseResult($pricing);
        $data['tiers'] = $commonHelper->extractTiersFromDatabaseResult($tiers);
        $data['pricing_json'] = json_encode($data['pricing']);
        $data['tiers_json'] = json_encode($data['tiers']);

        // Set subscription data
        $productName = $data['plan']['count'] . '-' . $data['plan']['country'] . '-' . $data['plan']['category'];
        $productDesc = $data['plan']['count'] . ' ' . $data['plan']['country'] . ' ' . $data['plan']['category'] . ' Proxies';

        // Calculate price again instead of relying on user input
        $productPrice = $this->calculatePrice($data);

        $planSettings = [
            'frequency' => 'DAY', // Possible values: WEEK, DAY, YEAR, MONTH
            'interval'  => 30,
            'cycles'    => 12
        ];

        if ($productPrice > 0) {

            if($type=='change') {

                $planRepo = new UserPlanRepo($this->getApp()->getDatabase());
                $plan = $planRepo->loadPlanByCriteria($this->getApp()->getUserRepo()->getBpId(),$data['plan']['country'],$data['plan']['category']);

                $isUpgrade = $data['plan']['count'] > $plan['plan_count'];

                $priceTier = $this->getPriceTierFromProxyCount($data['tiers'],$data['plan']['count']);
                $this->extractPricePerProxy($data['pricing'],$data['plan']['country'],$data['plan']['category'],$priceTier);

                // calculate new price, bearing in mind the upgrade
                // or downgrade difference that must be either credited
                // or deducted from the new price
                $dStart = new \DateTime();
                $dEnd  = new \DateTime($plan['plan_end']);
                $daysLeft = $dStart->diff($dEnd)->days;
                $newPlanCostPerDay = $productPrice / 30;
                $newPlanCostPerDayPerProxy = $newPlanCostPerDay / $data['plan']['count'];
                $oldPlanCostPerDay = $plan['plan_price'] / 30;
                $oldPlanCostPerDayPerProxy = $oldPlanCostPerDay / $plan['plan_count'];

                if($isUpgrade) {
                    $trialDays = 0;
                    $data['locations']['Mixed'] = $data['plan']['count'];
                    $proxyCountDifference = $data['plan']['count'] - $plan['plan_count'];
                    $costDiff = ($newPlanCostPerDayPerProxy * $proxyCountDifference) * $daysLeft;
                    $setupFee = $productPrice + $costDiff;
                    $trialText = 'Difference of '.Format::number($costDiff).' ' . $currency . ' is charged with setup fee.';
                } else {
                    $proxyCountDifference = $plan['plan_count'] - $data['plan']['count'];
                    $costDiff = ($oldPlanCostPerDayPerProxy * $proxyCountDifference) * $daysLeft;
                    $trialDays = round($costDiff * $daysLeft);
                    $setupFee = $productPrice;
                    $trialText = $trialDays.' days compensation trial ('.Format::number($costDiff).' ' . $currency . ') added.';
                    $costDiff = 0;
                }

                $redirectSuccess = $this->getApp()->getAppUrl() . '/user/plans/edit/'.$data['plan']['country'].'/'.$data['plan']['category'].'?paymentPlan=true';
                $redirectCancel = $this->getApp()->getAppUrl() . '/user/plans/edit/'.$data['plan']['country'].'/'.$data['plan']['category'].'?paymentPlan=false';

                // create new plan
                $createdPlan = $this->getApp()->getPaymentApi()->getApi('CreatePlan')->create(
                    $productName,
                    $productDesc,
                    $productPrice,
                    $redirectSuccess,
                    $redirectCancel,
                    $setupFee,
                    true,
                    'USD',
                    'fixed',
                    'Regular Payments',
                    'REGULAR',
                    $planSettings['frequency'],
                    $planSettings['interval'],
                    $planSettings['cycles'],
                    'Trial Period',
                    'TRIAL',
                    'Day',
                    1,
                    $trialDays,
                    $costDiff);


            } else if($type=='add') {

                $redirectSuccess = $this->getApp()->getAppUrl().'/user/plans/add?paymentPlan=true';
                $redirectCancel = $this->getApp()->getAppUrl().'/user/plans/add?paymentPlan=true';

                $createdPlan = $this->getApp()->getPaymentApi()->getApi('CreatePlan')->create(
                    $productName,
                    $productDesc,
                    $productPrice,
                    $redirectSuccess,
                    $redirectCancel,
                    $productPrice,
                    false,
                    'USD',
                    'fixed',
                    'Regular Payments',
                    'REGULAR',
                    $planSettings['frequency'],
                    $planSettings['interval'],
                    $planSettings['cycles']
                );

            } else if($type=='new') {

                $redirectSuccess = $this->getApp()->getAppUrl() . '/register?paymentPlan=true';
                $redirectCancel = $this->getApp()->getAppUrl() . '/register?paymentPlan=false';

                $createdPlan = $this->getApp()->getPaymentApi()->getApi('CreatePlan')->create(
                    $productName,
                    $productDesc,
                    $productPrice,
                    $redirectSuccess,
                    $redirectCancel,
                    $productPrice,
                    false,
                    'USD',
                    'fixed',
                    'Regular Payments',
                    'REGULAR',
                    $planSettings['frequency'],
                    $planSettings['interval'],
                    $planSettings['cycles']
                );
            }

            // if plan was created
            if ($createdPlan) {

                $activatedPlan = $this->getApp()->getPaymentApi()->getApi('ActivatePlan')->activate($createdPlan['plan']);

                // if plan was activated at paypal
                if($activatedPlan) {

                    // update plan
                    $paypalPlanRepo = new PaypalPlanRepo($this->getApp()->getDatabase());

                    $dbParams = [
                        'user_email' => $data['email'],
                        'plan_id' => $activatedPlan['id'],
                        'plan_state' => $activatedPlan['state'],
                        'plan_create_time' => $activatedPlan['created'],
                    ];
                    $paypalPlanRepo->insertPlan($dbParams);

                    // store plan id to session
                    $this->getApp()->getSession()->set('paypal_plan_id', $activatedPlan['id']);

                    // set name, description for purchase and create agreement link
                    if($type=='change') {
                        $agreementName = 'Plan ' . $productName;
                        $agreementDesc = $data['plan']['count'] . ' Proxies ' . $data['plan']['category'] . ' in ' . $data['plan']['country'] . ' for ' . Format::number($productPrice) . ' ' . $currency . ' per  ' . $planSettings['interval'] . ' ' . strtolower($planSettings['frequency']) . '(s) ' . $trialText;
                        $redirectUrl = $this->getApp()->getPaymentApi()->getApi('CreateAgreementPaypal')->create($activatedPlan, $agreementName, $agreementDesc);

                    } else {
                        $agreementName = 'Plan ' . $productName;
                        $agreementDesc = $data['plan']['count'] . ' Proxies ' . $data['plan']['category'] . ' in ' . $data['plan']['country'] . ' for ' . Format::number($productPrice) . ' USD per ' . $planSettings['interval'] . ' ' . strtolower($planSettings['frequency']) . '(s)';
                        $redirectUrl = $this->getApp()->getPaymentApi()->getApi('CreateAgreementPaypal')->create($activatedPlan, $agreementName, $agreementDesc);
                    }

                    // if link was created
                    if ($redirectUrl) {

                        // redirect to approval page
                        $response = new RedirectResponse($redirectUrl);
                        $response->send();

                    }

                }
            }

        }
    }
    /**
     * Creates first user plan and add user
     * @param string $token Paypal token
     * @param array $data
     * @return mixed
     */
    public function finalizeFirstPlan($token,$data)
    {
        $agreement = $this->getApp()->getPaymentApi()->getApi('ExecuteAgreement')->execute($token);

        if ($agreement) {

            $agreementIsOk = $this->checkAgreement($agreement);

            if (true !== $agreementIsOk) {
                return $agreementIsOk;
            }

            $bpRes = $this->getApp()->getApi()->getUser()->add($data['username']);

            if (isset($bpRes['error'])) {
                return $bpRes;
            }

            $bpId = $bpRes['user_id'];

            $userRepo = $this->getApp()->getUserRepo();

            $userParams = [
                'bp_id' => $bpId,
                'username' => $data['username'],
                'email' => $data['email'],
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'passphrase' => password_hash($data['password'], PASSWORD_DEFAULT),
                'roles' => 'ROLE_USER',
                'created_on' => Format::dateDb()
            ];

            $userRepo->insertUser($userParams);

            return $this->finalizePlan($bpId,$data,$agreement,'new');

        }

        return false;

    }
    /**
     * Creates new user plan
     * @param string $token Paypal token
     * @param array $data
     * @return mixed
     */
    public function finalizeNewPlan($token,$data)
    {
        $agreement = $this->getApp()->getPaymentApi()->getApi('ExecuteAgreement')->execute($token);

        if ($agreement) {
            $agreementIsOk = $this->checkAgreement($agreement);

            if (true !== $agreementIsOk) {
                return $agreementIsOk;
            }

            return $this->finalizePlan($this->getApp()->getUserRepo()->getBpId(),$data,$agreement,'add');

        }

        return false;
    }

    /**
     * Upgrade/Downgrade user plan
     * The old plan will be purged
     * @param string $token Paypal token
     * @param array $data
     * @return mixed
     */
    public function finalizeChangePlan($token,$data)
    {
        $agreement = $this->getApp()->getPaymentApi()->getApi('ExecuteAgreement')->execute($token);

        if ($agreement) {
            $agreementIsOk = $this->checkAgreement($agreement);

            if (true !== $agreementIsOk) {
                return $agreementIsOk;
            }

            return $this->finalizePlan($this->getApp()->getUserRepo()->getBpId(),$data,$agreement,'change');
        }

        return false;
    }

    public function cancelPlan($planId) {

        $this->paypalUpdatePlan($planId,'Cancelled');
    }

    private function checkAgreement(Agreement $agreement)
    {
        $wait = 1;
        $retry = 5;

        for ($retry--; $retry >= 0; $retry--) {

            try {
                $transactions = Agreement::searchTransactions($agreement->getId(), [
                    'start_date' => date('Y-m-d', strtotime('-1 day')),
                    'end_date'   => date('Y-m-d')
                ], $this->getApp()->getPaymentApi()->getApiContext());

            } catch (PayPalConnectionException $e) {
                return ['error_msg' => $e->getMessage(), 'code' => $e->getCode(), 'data' => $e->getData()];
            } catch (\Exception $e) {
                return ['error_msg' => $e->getMessage(), 'code' => $e->getCode()];
            }

            foreach (array_reverse($transactions->getAgreementTransactionList()) as $transaction) {
                /** @var AgreementTransaction $transaction */

                // Skipped status, skip this transaction
                if (in_array($transaction->getStatus(), ['Created', 'Reactivated', 'Active'])) {
                    continue;
                }

                // Good status means OK
                if (in_array($transaction->getStatus(), ['Completed'])) {
                    return true;
                }

                // Bad status means immediately failure
                if (in_array($transaction->getStatus(), ['Failed'])) {
                    return ['error_msg' => sprintf('PayPal transaction is in "%s" status!', $transaction->getStatus())];
                }

                // Any other status means failure if no retries are left
                if (!$retry) {
                    return ['error_msg' => sprintf('PayPal transaction is in "%s" status!', $transaction->getStatus())];
                }
            }

            // Wait for PayPal processing
            sleep($wait);
        }

        return ['error_msg' => 'Unknown error'];
    }

    /**
     * Finalize plan internally
     * @param string $bpId
     * @param array $data
     * @param object $agreement
     * @param string $type
     * @return mixed
     */
    private function finalizePlan($bpId,$data,$agreement,$type)
    {

        // load plan repo
        $planRepo = new UserPlanRepo($this->getApp()->getDatabase());

        $pricing = $this->getApp()->getPricingRepo()->getPrices();
        $tiers = $this->getApp()->getPricingRepo()->getTiers();

        $commonHelper = new Helper();
        $data['pricing'] = $commonHelper->extractPricesFromDatabaseResult($pricing);
        $data['tiers'] = $commonHelper->extractTiersFromDatabaseResult($tiers);
        $data['pricing_json'] = json_encode($data['pricing']);
        $data['tiers_json'] = json_encode($data['tiers']);

        // calculate price
        $productPrice = $this->calculatePrice($data);

        // load new user for further processing
        $this->getApp()->getUserRepo()->loadUserByBpId($bpId);

        $paypalPlanId = $this->getApp()->getSession()->get('paypal_plan_id');

        // set start time with subtracting 12 hours for paypal lag
        $startTime = Plan::getStartTime();
        $expiryTime = Plan::getExpiryTime();

        $dateTimeStart = Format::dateDb($startTime);
        $dateTimeExpiry = Format::dateDb($expiryTime);

        if ($type == 'add' || $type == 'new') {
            // add user plan to blazing proxies
            $planRes = $this->getApp()->getApi()->getUser()->addPlan($bpId, [
                'country' => $data['plan']['country'],
                'category' => $data['plan']['category'],
                'count' => $data['plan']['count']
            ]);

        }
        if ($type == 'change') {

            // update user plan expiry
            $planRepo->purgePlan($bpId, $data['plan']['country'], $data['plan']['category']);

            // delete plan externally
            $this->getApp()->getApi()->getUser()->purgePlan($bpId,$data['plan']['country'], $data['plan']['category']);

            // retrieve transaction from db
            $planRes = $this->getApp()->getApi()->getUser()->updatePlan($bpId, [
                'country' => $data['plan']['country'],
                'category' => $data['plan']['category'],
                'count' => $data['plan']['count']
            ]);

        }
        // set expiry date for proxies
        $this->getApp()->getApi()->getUser()->extendPlan($bpId, $data['plan']['country'], $data['plan']['category'], $expiryTime);

            if (isset($planRes['error'])) {
                return $planRes;
            } else {

                // extract locations from form data
                $preferredLocations = $this->getApp()->getApi()->getHelper()->extractLocations($this->getApp()->getApi(), $data['plan'], $data['locations']);

                if (count($preferredLocations) > 0) {

                    // store location preferences to blazing proxies
                    $this->getApp()->getApi()->getUser()->saveLocationPreference($bpId, $data['plan']['country'], $data['plan']['category'], ['locations' => $preferredLocations]);

                }

                $agreementParams = [
                    'bp_id' => $bpId,
                    'bp_plan_id' => $planRes['plan_id'],
                    'agreement_id' => $agreement->getId(),
                    'agreement_name' => !is_null($agreement->getName()) ? $agreement->getName() : '',
                    'agreement_desc' => $agreement->getDescription(),
                    'agreement_state' => $agreement->getState(),
                    'agreement_start_date' => Format::dateDb($agreement->getStartDate()),
                    'payer_id' => $agreement->getPayer()->getPayerInfo()->getPayerId()
                ];

                $agreementRepo = new PaypalAgreementRepo($this->getApp()->getDatabase());
                $agreementRepo->insertAgreement($agreementParams);

                $planParams = [
                    'bp_id' => $bpId,
                    'bp_plan_id' => isset($planRes['plan_id']) ? $planRes['plan_id'] : 0,
                    'payment_type' => 'paypal',
                    'payment_plan_id' => $paypalPlanId,
                    'payment_agreement_id' => $agreementParams['agreement_id'],
                    'plan_country' => $data['plan']['country'],
                    'plan_category' => $data['plan']['category'],
                    'plan_count' => $data['plan']['count'],
                    'plan_days' => isset($data['plan']['days']) ? $data['plan']['days'] : 30,
                    'plan_price' => $productPrice,
                    'plan_start' => $dateTimeStart,
                    'plan_expiry' => $dateTimeExpiry,
                    'plan_status' => 'active'
                ];

                $res = $planRepo->insertPlan($planParams);

            }



        return true;

    }

    /**
     * Update plan internally
     * @param $agreementId
     * @param $planState
     * @return bool
     */
    public function updatePlan($agreementId,$planState) {

        // init repo
        $userPlanRepo = new UserPlanRepo($this->getApp()->getDatabase());

        // retrieve transaction from db
        $planData = $userPlanRepo->loadPlanByPaymentAgreementId($agreementId);

        if($planData) {

            // update plan at paypal
            $res = $this->paypalUpdatePlan($planData['payment_plan_id'],$planState);

            if(!$res)
                return false;

            // update user plan state
            $userPlanRepo->updatePlanStateByAgreementId($agreementId, $planState);

            return true;

        }

        return false;

    }
    /**
     * Update paypal plan
     * @param string $planId Paypal plan id
     * @param string $state created|deleted|inactive|active
     * @return bool
     */
    public function paypalUpdatePlan($planId,$state) {

        $paypalPlanRepo = new PaypalPlanRepo($this->getApp()->getDatabase());
        $paypalAgreementRepo = new PaypalAgreementRepo($this->getApp()->getDatabase());

        // init repo
        $userPlanRepo = new UserPlanRepo($this->getApp()->getDatabase());

        // retrieve transaction from db
        $paypalPlanData = $userPlanRepo->loadPlanByPaymentPlanId($planId);

        try {

            $result = null;

            $createdPlan = $this->getApp()->getPaymentApi()->getApi('GetPlan')->get($planId);
            $paypalPlanState = $state;

            if($state=='Cancelled' || $state=='Purged') {
                $paypalPlanState = 'deleted';
            }
            // update paypal plan state
            $result = $this->getApp()->getPaymentApi()->getApi('UpdatePlan')->update($createdPlan,$paypalPlanState);

            if($state=='Cancelled') {

                // cancel paypal agreement
                $result = $this->getApp()->getPaymentApi()->getApi('CancelAgreement')->cancel($paypalPlanData['payment_agreement_id']);

            }
            if($result) {

                // update paypal plan state in database
                $paypalPlanRepo->updatePlanState($planId, $state);

                // update paypal agreement state
                $paypalAgreementRepo->updateAgreement($paypalPlanData['payment_agreement_id'], ['agreement_state' => $state, 'agreement_end_date' => Format::dateDb()]);

                return true;
            } else {
                return false;
            }

        } catch (\Exception $pce) {
            //@todo log failures so reseller can act on them accordingly
        }

        return false;

    }

    /**
     * Calculate price so we do not rely on user input
     * @param $data
     * @return mixed
     */
    private function calculatePrice($data) {

        $priceTier = $this->getPriceTierFromProxyCount($data['tiers'],$data['plan']['count']);

        // Calculate price
        return $this->extractPricePerProxy($data['pricing'],$data['plan']['country'],$data['plan']['category'],$priceTier) * $data['plan']['count'];
    }
    private function getPriceTierFromProxyCount($tiers,$count) {
        // Get price tier
        $priceTier = 0;
        foreach ($tiers as $tier => $ranges) {
            if ($count <= $ranges[1]) {
                $priceTier = $tier;
                break;
            }
        }
        return $priceTier;
    }
    private function extractPricePerProxy($pricing,$country,$category,$tier) {
        return $pricing[$country][$category]['tier_' . $tier];
    }
    /**
     * @return AppFramework
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * @param mixed $app
     */
    public function setApp($app)
    {
        $this->app = $app;
    }

    /**
     * Get plan start time 12 hours ahead
     * @return int
     */
    static public function getStartTime()
    {
        return time();
    }

    /**
     * Get plan expiry time 12 hours ahead
     * @return int
     */
    static public function getExpiryTime()
    {
        return self::getStartTime()+(86400*30)+(3600*12);
    }
}