<?php
namespace ResellerApp\Security;

use JWT;

/**
 * Class TokenService
 * @package ResellerApp\Security
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class TokenService
{
    /**
     * @var null
     */
    protected $token = null;
    /**
     * @var array|null
     */
    protected $config = null;
    /**
     * @var null
     */
    protected $request = null;
    /**
     * @var null
     */
    protected $response = null;
    /**
     * @var string
     */
    private $error = '';

    /**
     * TokenService constructor.
     */
    function __construct() {

        $this->config = [
                'client_id'=>1,
                'token_salt'=>'diosazhfdioahfdjeehjfdkjfnkjd',
                'token_algorithm'=>['HS256'],
            ];

    }

    /**
     * Get configuration
     * @param string $key
     * @return array|null
     */
    private function getConfig($key='') {
        return !empty($this->config[$key]) ? $this->config[$key] : $this->config;
    }

    /**
     * Generate token
     * @param string $issuer
     * @param array $audience
     * @param int $expiry
     * @return string
     */
    function generateToken($issuer,$audience,$expiry=0) {

        $expiryTime = $expiry > 0 ? $expiry : time()+(3600*24);

        $params = array(
            "iss" => $issuer,
            "aud" => array_merge(['client_id'=>$this->config['client_id']],$audience),
            "iat" => time(),
            "exp" => $expiryTime
        );

        $token = JWT::encode($params,$this->getConfig('token_salt'));
        $this->token = $token;

        return $token;
    }

    /**
     * Check if token is valid
     * @param object $tokenData
     * @return bool
     */
    function isValidToken($tokenData) {

        if(!is_object($tokenData))
            return false;

        if($tokenData===null)
            return false;

        if ($tokenData->aud->client_id != $this->getConfig('client_id') && $tokenData->exp > time())
            return false;

        return true;

    }

    /**
     * Decode token
     * @param string $token
     * @return null|object
     */
    function decodeToken($token) {
        $tks = explode('.', $token);
        if (count($tks) === 3) {
            return JWT::decode($token, $this->getConfig('token_salt'), $this->getConfig('token_algorithm'));
        }
        return null;
    }

    /**
     * Get token data
     * @param object $token
     * @return bool|null|object
     */
    function getTokenData($token) {

        try {

            if(empty($token))
                throw new \Exception('Token is empty');

            if(!$decoded_token = $this->decodeToken($token))
                throw new \Exception('Could not decode token');

            if(!is_object($decoded_token))
                throw new \Exception('Invalid token');

            // validate that this token was made for us
            if ($decoded_token->aud->client_id != $this->getConfig('client_id')
                || $decoded_token->exp < time()) {
                throw new \Exception('Token validation failed');
            }

            return $decoded_token;

        } catch(\Exception $ex) {
            $this->error = $ex->getMessage();
            return false;
        }

    }

    /**
     * Get error string
     * @return string
     */
    function getError() {
        return $this->error;
    }

}