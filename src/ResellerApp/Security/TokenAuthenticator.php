<?php
namespace ResellerApp\Security;

use ResellerApp\Repo\UserRepo;
use ResellerApp\Utils\Validator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class TokenAuthenticator
 * @package ResellerApp\Security
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class TokenAuthenticator extends AbstractGuardAuthenticator
{

    /**
     * @var null|UserRepo
     */
    private $userRepo = null;

    /**
     * TokenAuthenticator constructor.
     * @param $userRepo
     */
    public function __construct($userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * Called on every request. Return whatever credentials you want,
     * or null to stop authentication.
     * @param Request $request
     * @return array|mixed|null|void
     */
    public function getCredentials(Request $request)
    {

        if (!$token = $request->headers->get('X-AUTH-TOKEN')) {
            return null;
        }

        return array(
            'token' => $token,
        );
    }

    /**
     * Get user from token
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return array|null
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = $credentials['token'];
        $this->getUserRepo()->loadUserByToken($token);

        return $this->getUserRepo()->all();
    }

    /**
     * Check user credentials
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {

        $validator = new Validator();

        if($validator->checkPasswordHash($credentials->getPassword(),$user->getPassword()))
            return true;

        return false;

    }

    /**
     * Authentication success callback
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    /**
     * Authentication failure callback
     * @param Request $request
     * @param AuthenticationException $exception
     * @return JsonResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = array(
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        );

        return new JsonResponse($data, 403);
    }

    /**
     * Called when authentication is needed, but it's not sent
     * @param Request $request
     * @param AuthenticationException $authException
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            'message' => 'Authentication Required'
        );

        return new JsonResponse($data, 401);
    }

    public function supportsRememberMe()
    {
        return false;
    }

    /**
     * @return null|UserRepo
     */
    public function getUserRepo()
    {
        return $this->userRepo;
    }

    /**
     * @param null $userRepo
     */
    public function setUserRepo($userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * Checks if a user session is valid
     * @param $session Session
     * @return bool
     */
    function isAuth($session) {
        return $session->get('auth')===true;
    }

}