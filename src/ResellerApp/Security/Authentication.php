<?php
namespace ResellerApp\Security;

use ResellerApp\AppFramework;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Class Authentication
 * @package ResellerApp\Security
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class Authentication {

    /**
     * @var null
     */
    private $tokenService = null;
    /**
     * @var null
     */
    private $authTokenData = null;
    /**
     * @var string
     */
    private $authToken = '';

    /**
     * Authentication constructor.
     */
    public function __construct() {

        $this->framework = new AppFramework();

        $this->setTokenService(new TokenService());

    }

    /**
     * Check if user is authenticated
     * @return bool
     */
    public function isAuth() {

        $tokenData = $this->getAuthTokenData();

        if($this->getTokenService()->isValidToken($tokenData)) {

            //@todo IMPORTANT! implement user check
            return true;

        } else {
            return false;
        }

    }

    /**
     * Get authentication token
     * @return string
     */
    function getAuthToken() {

        if(empty($this->authToken))
            $this->authToken = $this->framework->getRequest()->cookies->get('auth');

        return $this->authToken;
    }

    /**
     * Get auth token data
     * @return bool|null|object
     */
    function getAuthTokenData() {

        if(null===$this->authTokenData) {

            $token = $this->getAuthToken();
            $this->authTokenData = $this->getTokenService()->getTokenData($token);

        }
        return $this->authTokenData;
    }

    /**
     * Retrieve user id from token
     * @return null|int
     */
    function getAuthTokenUserId() {
        $data = $this->getAuthTokenData();
        return isset($data->aud) ? $data->aud->user_id : null;
    }

    /**
     * Create auth cookie
     * @param $token
     * @return void
     */
    function createAuthCookie($token) {

        $this->framework->getResponse()->headers->setCookie(new Cookie('auth', $token, time() + (3600 * 48), '/', null, false, false));

    }

    /**
     * Delete auth cookie
     * @return void
     */
    function deleteAuthCookie() {

        $this->framework->getResponse()->headers->setCookie(new Cookie('auth', '', time() - (3600 * 48), '/', null, false, false));

    }

    /**
     * @return TokenService
     */
    public function getTokenService()
    {
        return $this->tokenService;
    }

    /**
     * @param null $tokenService
     */
    public function setTokenService($tokenService)
    {
        $this->tokenService = $tokenService;
    }
}