<?php
namespace ResellerApp\Api;

/**
 * Interface BlazingProxiesInterface
 *
 * Any implementation that wants to communicate
 * with blazing proxies backend, needs to implement
 * this interface.
 *
 * @package ResellerApp\Api
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
interface BlazingProxiesInterface
{
    /**
     * @param array $credentials
     * @return void
     */
    function setCredentials($credentials);
}