<?php
namespace ResellerApp\Api;
use ResellerApp\AppFramework;
use ResellerApp\Utils\Format;

/**
 * Class BlazingProxiesRequest
 * @package ResellerApp\Api
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class BlazingProxiesRequest
{

    /**
     * @var null
     */
    private $handle = null;
    /**
     * @var null
     */
    private $defaultHeaders = [];
    /**
     * @var null|array
     */
    private $opts = array();
    /**
     * @var null|array
     */
    private $jsonOpts = array();
    /**
     * @var
     */
    private $verifyPeer;
    /**
     * @var
     */
    private $verifyHost;
    /**
     * @var
     */
    private $headers;

    /**
     * Set headers to send on every request
     *
     * @param array $headers headers array
     * @return array
     */
    public function defaultHeaders($headers=[])
    {
        return $this->defaultHeaders = array_merge($this->defaultHeaders, $headers);
    }

    /**
     * Set a new header to send on every request
     *
     * @param string $name header name
     * @param string $value header value
     * @return string
     */
    public function defaultHeader($name, $value)
    {
        return $this->defaultHeaders[$name] = $value;
    }

    /**
     * @param $url
     * @param null $parameters
     * @return BlazingProxiesResponse
     * @throws \Exception
     */
    public function head($url, $parameters = null)
    {
        return $this->send('HEAD', $url, $parameters);
    }

    /**
     * Get request
     * @param $url
     * @param null $parameters
     * @return BlazingProxiesResponse
     * @throws \Exception
     */
    public function get($url, $parameters = null)
    {
        return $this->send('GET', $url, $parameters);
    }

    /**
     * Post request
     * @param $url
     * @param null $body
     * @return BlazingProxiesResponse
     * @throws \Exception
     */
    public function post($url, $body = null)
    {
        return $this->send('POST', $url, $body);
    }

    /**
     * Delete request
     * @param $url
     * @param null $body
     * @return BlazingProxiesResponse
     * @throws \Exception
     */
    public function delete($url, $body = null)
    {
        return $this->send('DELETE', $url, $body);
    }

    /**
     * Put request
     * @param $url
     * @param null $body
     * @return BlazingProxiesResponse
     * @throws \Exception
     */
    public function put($url, $body = null)
    {
        return $this->send('PUT', $url, $body);
    }

    /**
     * Patch request
     * @param $url
     * @param null $body
     * @return BlazingProxiesResponse
     * @throws \Exception
     */
    public function patch($url, $body = null)
    {
        return $this->send('PATCH', $url, $body);
    }

    /**
     * @param $method
     * @param $url
     * @param null $body
     * @return BlazingProxiesResponse
     * @throws \Exception
     */
    public function send($method, $url, $body = null)
    {

        $this->handle = curl_init();
        $rBody = urldecode(http_build_query($this->getQuery($body)));

        if ($method !== 'GET') {
            curl_setopt($this->handle, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($this->handle, CURLOPT_POSTFIELDS, $rBody);
        } elseif (is_array($body)) {
            if (strpos($url, '?') !== false) {
                $url .= '&';
            } else {
                $url .= '?';
            }
            $url .= $rBody;
        }

        $curl_base_options = [
            CURLOPT_URL => $this->encodeUrl($url),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_HEADER => true,
            CURLOPT_HTTPHEADER => self::getFormattedHeaders($this->getHeaders()),
            CURLOPT_SSL_VERIFYPEER => $this->verifyPeer,
            CURLOPT_SSL_VERIFYHOST => $this->verifyHost === false ? 0 : 2,
            CURLOPT_ENCODING => ''
        ];
        curl_setopt_array($this->handle, $this->mergeCurlOptions($curl_base_options, $this->opts));
        curl_setopt($this->handle, CURLOPT_VERBOSE, true);

        $response   = curl_exec($this->handle);
        $error      = curl_error($this->handle);
        $info       = $this->getInfo();


        if ($error) {
            throw new \Exception($error);
        }
        // Split the full response in its headers and body
        $header_size = $info['header_size'];
        $header      = substr($response, 0, $header_size);
        $body        = substr($response, $header_size);
        $httpCode    = $info['http_code'];
        $this->log('Request: '.$method.' '.$url.'|'.$rBody);
        if(!empty($rBody))
        $this->log('Payload: '.$rBody);
        $this->log('Response: |'.$body);
        return new BlazingProxiesResponse($httpCode, $body, $header, $this->jsonOpts);
    }

    /**
     * Get curl query
     * @param $data
     * @param bool $parent
     * @return array
     */
    public function getQuery($data, $parent = false)
    {
        $result = array();
        if (is_object($data)) {
            $data = get_object_vars($data);
        }
        foreach ($data as $key => $value) {
            if ($parent) {
                $new_key = sprintf('%s[%s]', $parent, $key);
            } else {
                $new_key = $key;
            }
            if (!$value instanceof \CURLFile and (is_array($value) or is_object($value))) {
                $result = array_merge($result, $this->getQuery($value, $new_key));
            } else {
                $result[$new_key] = $value;
            }
        }
        return $result;
    }
    private function encodeUrl($url)
    {
        $url_parsed = parse_url($url);
        $scheme = $url_parsed['scheme'] . '://';
        $host   = $url_parsed['host'];
        $path   = (isset($url_parsed['path']) ? $url_parsed['path'] : null);
        $query  = (isset($url_parsed['query']) ? $url_parsed['query'] : null);
        if ($query !== null) {
            $query = '?' . http_build_query($this->getArrayFromQuerystring($query));
        }
        $result = $scheme . $host . $path . $query;
        return $result;
    }

    private function getArrayFromQuerystring($query)
    {
        $query = preg_replace_callback('/(?:^|(?<=&))[^=[]+/', function ($match) {
            return bin2hex(urldecode($match[0]));
        }, $query);
        parse_str($query, $values);
        return array_combine(array_map('hex2bin', array_keys($values)), $values);
    }

    public function getInfo($opt = false)
    {
        if ($opt) {
            $info = curl_getinfo($this->handle, $opt);
        } else {
            $info = curl_getinfo($this->handle);
        }
        return $info;
    }

    /**
     *
     * @param $headers
     * @return array
     */
    public function getFormattedHeaders($headers)
    {
        $formattedHeaders = array();

        $combinedHeaders = array_change_key_case(array_merge($this->defaultHeaders, (array) $headers));

        foreach ($combinedHeaders as $key => $val) {
            $formattedHeaders[] = $this->getSingleHeaderString($key, $val);
        }
        if (!array_key_exists('user-agent', $combinedHeaders)) {
            $formattedHeaders[] = 'user-agent: api_key/1.0';
        }
        if (!array_key_exists('expect', $combinedHeaders)) {
            $formattedHeaders[] = 'expect:';
        }
        return $formattedHeaders;
    }

    /**
     * Get single header string
     * @param $key
     * @param $val
     * @return string
     */
    private function getSingleHeaderString($key, $val)
    {
        $key = trim(strtolower($key));
        return $key . ': ' . $val;
    }

    /**
     * Merge cURL option arrays
     * @param $existing_options
     * @param $new_options
     * @return mixed
     */
    private function mergeCurlOptions(&$existing_options, $new_options)
    {
        $existing_options = $new_options + $existing_options;
        return $existing_options;
    }

    /**
     * Get cURL handle
     * @return null|Resource
     */
    public function getHandle()
    {
        return $this->handle;
    }

    /**
     * Set cURL handle
     * @param $handle
     */
    public function setHandle($handle)
    {
        return $this->handle = $handle;
    }
    private function getHeaders()
    {

        return $this->headers;
    }

    /**
     * @param mixed $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }
    /**
     * @param string $message
     */
    public function log($message)
    {
        $line = Format::dateDb() . '|'.$_SERVER['REMOTE_ADDR'].'|' .$message.PHP_EOL;
        $log_path = AppFramework::getInstance()->getAppPath() . '/app/log/bp/api.log';

        // Ensure log file can be created
        if (!is_dir(dirname($log_path))) {
            mkdir(dirname($log_path), 0777, true);

            // If there is no chance to created directory hierarchy, we just fail out with error in next lines
        }

        file_put_contents($log_path,$line,FILE_APPEND);
    }
}