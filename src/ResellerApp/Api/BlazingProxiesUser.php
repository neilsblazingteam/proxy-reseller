<?php
namespace ResellerApp\Api;

/**
 * Class BlazingProxiesUser
 * @package ResellerApp\Api
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class BlazingProxiesUser
{
    /**
     * @var array
     */
    protected $user = null;

    /**
     * @var string
     */
    private $endpoint = '/user';

    /**
     * @var null|BlazingProxiesAdapter
     */
    private $adapter = null;

    /**
     * BlazingProxiesUser constructor.
     * @param BlazingProxiesAdapter $adapter
     */
    function __construct(BlazingProxiesAdapter $adapter)
    {
        $this->setAdapter($adapter);
    }

    /**
     * Get a list of users
     * @return array
     */
    function all() {
        return $this->getAdapter()->callApi('GET',$this->getEndpoint());
    }

    /**
     * Get user by id
     * @param $id
     * @return array
     */
    function get($id) {
        if(is_null($this->user))
            $this->user = $this->getAdapter()->callApi('GET',$this->getEndpoint().'/'.$id);
        return $this->user;
    }

    /**
     * Add new user
     * @param string $username
     */
    function add($username) {
        return $this->getAdapter()->callApi('POST',$this->getEndpoint(),['username'=>$username]);
    }

    /**
     * Get a list of resellers users
     * @param $id
     * @param $data
     * @return array
     */
    function replaceProxy($id,$data) {
        $this->user = null;
        return $this->getAdapter()->callApi('PATCH',$this->getEndpoint().'/'.$id.'/replace',$data);
    }

    /**
     * Save user settings
     * @param $id
     * @param $data
     * @return array
     */
    function saveSettings($id,$data) {
        $this->user = null;
        return $this->getAdapter()->callApi('PATCH',$this->getEndpoint().'/'.$id.'/settings',$data);
    }

    /**
     * Save user location preference
     * @param $id
     * @param $country
     * @param $category
     * @param $data
     * @return array
     */
    function saveLocationPreference($id,$country,$category,$data) {
        $this->user = null;
        return $this->getAdapter()->callApi('PATCH',$this->getEndpoint().'/'.$id.'/plan/'.$country.'/'.$category.'/locations',$data);
    }

    /**
     * Add plan
     * @param $id
     * @param $data
     * @return array
     */
    function addPlan($id,$data) {
        $this->user = null;
        return $this->getAdapter()->callApi('POST',$this->getEndpoint().'/'.$id.'/plan',$data);
    }
    /**
     * Update plan
     * @param int $id
     * @param array $data
     * @return array
     * @todo should be PATCH method??
     */
    function updatePlan($id,$data) {
        $this->user = null;
        return $this->getAdapter()->callApi('POST',$this->getEndpoint().'/'.$id.'/plan',$data);
    }

    /**
     * Delete plan
     * @param int $id
     * @param string $country
     * @param string $category
     * @return array
     */
    function deletePlan($id,$country,$category) {
        $this->user = null;
        return $this->getAdapter()->callApi('DELETE',$this->getEndpoint().'/'.$id.'/plan/'.$country.'/'.$category);
    }

    /**
     * Delete plan
     * @param int $id
     * @param string $country
     * @param string $category
     * @return array
     */
    function purgePlan($id,$country,$category) {
        return $this->getAdapter()->callApi('DELETE',$this->getEndpoint().'/'.$id.'/refund',['country'=>$country,'category'=>$category]);
    }

    /**
     * Add auth ip
     * @param int $id
     * @param array $data [ip=>1.1.1.1]
     * @return array
     */
    function addAuthIp($id,$data) {
        return $this->getAdapter()->callApi('POST',$this->getEndpoint().'/'.$id.'/ip',$data);
    }

    /**
     * Delete auth ip
     * @param $id
     * @param array $data [ip=>1.1.1.1]
     * @return array
     */
    function deleteAuthIp($id,$data) {
        return $this->getAdapter()->callApi('DELETE',$this->getEndpoint().'/'.$id.'/ip',$data);
    }

    /**
     * Set new expire date
     * @param $id
     * @param $country
     * @param $category
     * @param $expiration
     * @return array
     */
    function extendPlan($id,$country,$category,$expiration) {
        $data = ['country'=>$country,'category'=>$category,'expiration'=>$expiration];
        return $this->getAdapter()->callApi('POST',$this->getEndpoint().'/'.$id.'/plan/expiration',$data);
    }

    /**
     * @return BlazingProxiesAdapter
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * @param BlazingProxiesAdapter $adapter
     */
    public function setAdapter($adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @param string $endpoint
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

}