<?php
namespace ResellerApp\Api;

/**
 * Class BlazingProxiesHelper
 * @package ResellerApp\Api
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class BlazingProxiesHelper
{

    /**
     * Convert object to associative array
     * @param $d
     * @return array
     */
    function objectToArray($d) {

        if (is_object($d)) {

            $d = get_object_vars($d);
        }

        if (is_array($d)) {
            return array_map([$this,'objectToArray'], $d);
        } else {

            return $d;
        }
    }
    function getUserCategoriesFromResult($result) {

        $categories = [];

        foreach($result['plans'] as $plan) {
            $categories[$plan['category']] = $plan['category'];
        }

        return $categories;

    }

    /**
     * Get count of proxies assigned with locations
     * @param $plans
     * @return int
     */
    function getTotalAssignedProxyLocationsCount($plans) {

        $assignedLocations = 0;

        foreach($plans as $plan) {
            foreach($plan['proxies'] as $proxy) {
                $assignedLocations += $proxy['proxy_location'] !== null ? 1 : 0;
            }
        }
        return $assignedLocations;
    }
    /**
     * Get count of proxies assigned with locations
     * @param $plans
     * @return int
     */
    function getTotalAssignedPortLocationsCount($plans) {

        $assignedLocations = 0;

        foreach($plans as $plan) {
            foreach($plan['proxies'] as $proxy) {
                $assignedLocations += $proxy['port_location'] !== null ? 1 : 0;
            }
        }
        return $assignedLocations;
    }
    /**
     * Get count of assigned proxies
     * @param $plans
     * @return int
     */
    function getTotalAssignedProxyCount($plans) {

        $assignedLocations = 0;

        foreach($plans as $plan) {
            foreach($plan['proxies'] as $proxy) {
                $assignedLocations += $proxy['ip'] !== null ? 1 : 0;
            }
        }
        return $assignedLocations;
    }
    /**
     * Get count of assigned user locations
     * @param $plans
     * @return array
     */
    function getAssignedLocationsCount($plans) {

        $assignedLocations = [];

        foreach($plans as $plan) {
            $assignedLocations[$plan['country']][$plan['category']] = 0;
            foreach($plan['proxies'] as $proxy) {
                $assignedLocations[$plan['country']][$plan['category']] += $proxy['port_location'] !== null ? 1 : 0;
            }
        }

        return $assignedLocations;

    }

    /**
     * Extract and map proxies to country category array
     * @param array $plans
     * @param string $country
     * @param string $category
     * @param string $authType
     * @param string $username
     * @param string $password
     * @return array
     */
    function getProxiesForExportFromPlan($plans,$country,$category,$authType,$username='',$password='') {

        $list = [];
        foreach($plans as $plan) {
            if($plan['country'] == $country && $plan['category'] == $category) {
                foreach($plan['proxies'] as $proxy) {
                    if($proxy['ip'] !== null) {
                        $tmp = [$proxy['ip'],$this->getProxyPort($proxy, $authType)];

                        if($authType==='pw') {
                            array_push($tmp,$username);
                            array_push($tmp,$password);
                        }

                        $list[] = implode(':',$tmp);
                    }
                }
            }
        }
        return $list;
    }

    /**
     * Get correspondent proxy port, based on authType
     * @param array $proxy
     * @param $authType
     * @return bool|int
     */
    public function getProxyPort(array $proxy, $authType) {
        if ($authType === 'pw') {
            return 4444;
        }
        elseif ($authType === 'ip') {
            return 3128;
        }
        elseif (!empty($proxy['port'])) {
            return $proxy['port'];
        }
        else {
            return false;
        }
    }

    function getFilteredProxies($plans) {

    }

    /**
     * Return array of locations where dedicated categories
     * other that mixed are unset. Dedicated category only
     * allows for mixed at the moment.
     *
     * @param $apiLocations
     * @return mixed
     */
    function getPossibleLocations($apiLocations) {

        foreach($apiLocations as $country=>$locations) {
            foreach($locations as $location=>$categories){
                foreach($categories as $category=>$count) {
                    if ($category === 'dedicated' && $location !== 'Mixed') {
                        if(isset($apiLocations[$country][$location][$category]))
                        unset($apiLocations[$country][$location][$category]);
                    }
                }
            }
        }

        return $apiLocations;

    }

    /**
     * Get preferred locations for sending to API
     *
     * @param BlazingProxiesAdapter $adapter
     * @param $plan
     * @param $locations
     * @return mixed
     */
    function extractLocations(BlazingProxiesAdapter $adapter, $plan,$locations) {

        $preferredLocations = [];

        if(in_array($plan['category'], ['rotate'])) {
            // workaround for dedicated proxies not allowing locations other than mixed
            $preferredLocations['Mixed'] = $plan['count'];
        }
        // default location for category over country
        elseif ($found = array_values(array_filter($adapter->getInfo()->getMetaCategories(), function($category) use ($plan) {
            return $plan['category'] == $category['key'];
        })) and
            $category = $found[0] and
            !empty($category['defaultLocation'])
        ) {
            // workaround for dedicated proxies not allowing locations other than default
            $category = $found[0];
            $preferredLocations[$category['location']] = $plan['count'];
        } else {
            // regular location request preparation
            foreach ($locations[$plan['country']] as $region => $categories) {
                $value = 0;
                if(isset($categories[$plan['category']]))
                $value = (int)$categories[$plan['category']];
                if ($value > 0)
                    $preferredLocations[$region] = $value;
            }

        }

        return $preferredLocations;

    }

    /**
     * Get preferred locations count
     *
     * @param BlazingProxiesAdapter $adapter
     * @param $plan
     * @param $locations
     * @return mixed
     */
    function extractLocationsCount(BlazingProxiesAdapter $adapter, $plan, $locations) {

        $count = 0;
        $locations = $this->extractLocations($adapter, $plan, $locations);

        if(!empty($locations))
            $count = array_sum($locations);

        return $count;

    }
}
