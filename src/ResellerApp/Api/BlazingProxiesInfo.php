<?php
namespace ResellerApp\Api;

/**
 * Class BlazingProxiesInfo
 * @package ResellerApp\Api
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class BlazingProxiesInfo
{

    /**
     * @var string
     */
    private $endpoint = '';

    /**
     * @var null|BlazingProxiesAdapter
     */
    private $adapter = null;

    /**
     * BlazingProxiesUser constructor.
     * @param BlazingProxiesAdapter $adapter
     */
    function __construct(BlazingProxiesAdapter $adapter)
    {
        $this->setAdapter($adapter);
    }

    /**
     * Get proxy locations
     * @return mixed
     */
    function getLocations()
    {
        return $this->getAdapter()->callApi('GET', $this->getEndpoint() . '/locations');
    }

    /**
     * Get proxy countries
     * @return mixed
     */
    function getCountries()
    {

        return $this->getAdapter()->callApi('GET', $this->getEndpoint() . '/countries');

    }

    public function getMetaCategories()
    {
        return [
            'sneaker' => [
                'key' => 'sneaker',
                'country' => 'us',
                'defaultLocation' => false,
                'strictToLocations' => [
                    'Los Angeles',
                    'Buffalo'
                ],
                'title' => 'Footsite Proxies'
            ],
            'supreme' => [
                'key' => 'supreme',
                'country' => 'us',
                'defaultLocation' => false,
                'strictToLocations' => [
                    'Los Angeles',
                    'Buffalo'
                ],
                'title' => 'Supreme Proxies'
            ]
        ];
    }

    /**
     * @return null|BlazingProxiesAdapter
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * @param null|BlazingProxiesAdapter $adapter
     */
    public function setAdapter($adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @param string $endpoint
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

}