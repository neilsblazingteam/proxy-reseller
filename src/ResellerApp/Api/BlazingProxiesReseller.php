<?php
namespace ResellerApp\Api;

/**
 * Class BlazingProxiesReseller
 * @package ResellerApp\Api
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class BlazingProxiesReseller
{

    /**
     * @var string
     */
    private $endpoint = '';

    /**
     * @var null|BlazingProxiesAdapter
     */
    private $adapter = null;

    function __construct(BlazingProxiesAdapter $adapter)
    {
        $this->setAdapter($adapter);
    }

    /**
     * Get reseller balance
     * @return mixed
     */
    function getBalance() {
        return $this->getAdapter()->callApi('GET',$this->getEndpoint().'/balance');
    }

    public function getPricing()
    {
        return $this->getAdapter()->callApi('GET',$this->getEndpoint().'/pricing');
    }

    /**
     * @return BlazingProxiesAdapter
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * @param BlazingProxiesAdapter $adapter
     */
    public function setAdapter($adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @param string $endpoint
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }
}