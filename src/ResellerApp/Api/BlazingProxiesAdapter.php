<?php
namespace ResellerApp\Api;

use ResellerApp\AppFramework;

/**
 * Class BlazingProxiesAdapter
 *
 * Handles communication with blazing proxies API
 *
 * @package ResellerApp\Api
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class BlazingProxiesAdapter implements BlazingProxiesInterface
{

    /**
     * @var string
     */
    private $api_url = '';
    /**
     * @var string
     */
    private $api_user = '';
    /**
     * @var string
     */
    private $api_key = '';
    /**
     * @var array
     */
    private $errors = [];

    function __construct($credentials)
    {
        $apiConfig = AppFramework::getInstance()->getConfig('app')['api'];
        $this->api_url = "{$apiConfig['protocol']}://{$apiConfig['host']}/" . ltrim($apiConfig['url'], '/');

        $this->setCredentials($credentials);
        return $this;
    }

    /**
     * Set credentials
     * @param array $credentials
     * @return bool|void
     */
    function setCredentials($credentials)
    {

        if(!isset($credentials['api_key'])) {

            $this->errors[] = 'You must provide a api key';
            return false;

        }

        $this->setApiKey($credentials['api_key']);

        return true;

    }

    /**
     * Get user object
     * @return BlazingProxiesUser
     */
    function getUser() {
        return new BlazingProxiesUser($this);
    }

    /**
     * Get reseller object
     * @return BlazingProxiesReseller
     */
    function getReseller() {
        return new BlazingProxiesReseller($this);
    }

    /**
     * Get info object
     * @return BlazingProxiesInfo
     */
    function getInfo() {
        return new BlazingProxiesInfo($this);
    }
    /**
     * Get helper object
     * @return BlazingProxiesHelper
     */
    function getHelper() {
        return new BlazingProxiesHelper();
    }

    /**
     * @return string
     */
    public function getApiUrl()
    {
        return $this->api_url;
    }

    /**
     * @param string $api_url
     */
    public function setApiUrl($api_url)
    {
        $this->api_url = $api_url;
    }

    /**
     * @return string
     */
    public function getApiUser()
    {
        return $this->api_user;
    }

    /**
     * @param string $api_user
     */
    public function setApiUser($api_user)
    {
        $this->api_user = $api_user;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->api_key;
    }

    /**
     * @param string $api_key
     */
    public function setApiKey($api_key)
    {
        $this->api_key = $api_key;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Make api call
     * @param $method
     * @param $endpoint
     * @param $params
     */
    public function callApi($method, $endpoint, $params=[])
    {

        $params['api_key'] = $this->getApiKey();

        $request = new BlazingProxiesRequest();
        $request->defaultHeaders([
            'Content-Type: application/json'
        ]);

        $method = strtolower($method);

        $response = $request->$method($this->getApiUrl().$endpoint,$params);

        return $response->getResponseArray();

    }

}