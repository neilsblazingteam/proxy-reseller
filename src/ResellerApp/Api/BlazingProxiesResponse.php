<?php

namespace ResellerApp\Api;

/**
 * Class BlazingProxiesResponse
 * @package ResellerApp\Api
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class BlazingProxiesResponse
{
    /**
     * @var int
     */
    public $code = 0;
    /**
     * @var string
     */
    public $raw_body = '';
    /**
     * @var mixed
     */
    public $body;
    /**
     * @var array
     */
    public $headers = [];
    /**
     * @var BlazingProxiesParser
     */
    private $parser = null;

    /**
     * @var BlazingProxiesHelper
     */
    private $helper = null;

    /**
     * BlazingProxiesResponse constructor.
     * @param int $code Response code
     * @param $raw_body
     * @param $headers
     * @param array $json_args
     */
    public function __construct($code, $raw_body, $headers, $json_args = array())
    {

        $this->setParser(new BlazingProxiesParser());
        $this->setHelper(new BlazingProxiesHelper());

        $this->code     = $code;
        $this->raw_body = $raw_body;
        $this->headers  = $this->parseHeaders($headers);
        $this->body     = $raw_body;

        array_unshift($json_args, $raw_body);
        if (function_exists('json_decode')) {
            $json = call_user_func_array('json_decode', $json_args);
            if (json_last_error() === JSON_ERROR_NONE) {
                $this->body = $json;
            }
        }
    }
    function getResponseArray() {
        return $this->getHelper()->objectToArray($this->body);
    }
    /**
     * http://php.net/manual/en/function.http-parse-headers.php#112986
     * @param string $raw_headers raw headers
     * @return array
     */
    private function parseHeaders($raw_headers)
    {
        if (function_exists('http_parse_headers')) {
            return http_parse_headers($raw_headers);
        } else {
            $key = '';
            $headers = array();
            foreach (explode("\n", $raw_headers) as $i => $h) {
                $h = explode(':', $h, 2);
                if (isset($h[1])) {
                    if (!isset($headers[$h[0]])) {
                        $headers[$h[0]] = trim($h[1]);
                    } elseif (is_array($headers[$h[0]])) {
                        $headers[$h[0]] = array_merge($headers[$h[0]], array(trim($h[1])));
                    } else {
                        $headers[$h[0]] = array_merge(array($headers[$h[0]]), array(trim($h[1])));
                    }
                    $key = $h[0];
                } else {
                    if (substr($h[0], 0, 1) == "\t") {
                        $headers[$key] .= "\r\n\t".trim($h[0]);
                    } elseif (!$key) {
                        $headers[0] = trim($h[0]);
                    }
                }
            }
            return $headers;
        }
    }

    /**
     * @return BlazingProxiesParser
     */
    public function getParser()
    {
        return $this->parser;
    }

    /**
     * @param BlazingProxiesParser $parser
     */
    public function setParser($parser)
    {
        $this->parser = $parser;
    }

    /**
     * @return BlazingProxiesHelper
     */
    public function getHelper()
    {
        return $this->helper;
    }

    /**
     * @param BlazingProxiesHelper $helper
     */
    public function setHelper($helper)
    {
        $this->helper = $helper;
    }
}