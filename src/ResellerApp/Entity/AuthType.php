<?php
namespace ResellerApp\Entity;

use ResellerApp\Database\QueryBuilder;

/**
 * Class AuthType
 * @package ResellerApp\Entity
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class AuthType
{

    /**
     * @var null|QueryBuilder
     */
    public $db = null;
    /**
     * @var null|array
     */
    private $data = null;

    /**
     * Reseller constructor.
     * @param $db QueryBuilder
     */
    function __construct($db) {
        $this->db = $db;
    }
    /**
     * @param $data
     * @return array|null
     */
    function set($data) {
        $this->data = $data;
    }
    /**
     * @return array|null
     */
    function all() {
        return $this->data;
    }
    /**
     * Get id
     * @return string
     */
    function getId() {
        return $this->data['id'];
    }
    /**
     * Get auth type (ip|pw)
     * @return string
     */
    function getAuthType() {
        return $this->data['auth_type'];
    }
    /**
     * Get auth value (ip address|password)
     * @return string
     */
    function getAuthValue() {
        return $this->data['auth_value'];
    }

}