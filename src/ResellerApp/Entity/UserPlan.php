<?php
namespace ResellerApp\Entity;

use ResellerApp\Database\QueryBuilder;

/**
 * Class UserPlan
 * @package ResellerApp\Entity
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class UserPlan
{
    /**
     * @var null|QueryBuilder
     */
    public $db = null;
    /**
     * @var null|array
     */
    private $data = null;

    /**
     * UserPlan constructor.
     * @param $db
     */
    function __construct($db) {
        $this->db = $db;
    }

    /**
     * @param $data
     * @return array|null
     */
    function set($data) {
        $this->data = $data;
    }

    /**
     * @return array|null
     */
    function all() {
        return $this->data;
    }

    /**
     * Get id
     * @return string
     */
    function getId() {
        return $this->data['id'];
    }

    /**
     * Get country
     * @return string
     */
    function getBpPlanId() {
        return $this->data['bp_plan_id'];
    }

    /**
     * Get country
     * @return string
     */
    function getPlanCountry() {
        return $this->data['plan_country'];
    }
    /**
     * Get category
     * @return string
     */
    function getPlanCategory() {
        return $this->data['plan_category'];
    }
    /**
     * Get days
     * @return string
     */
    function getPlanDays() {
        return $this->data['plan_days'];
    }
    /**
     * Get price
     * @return string
     */
    function getPlanPrice() {
        return $this->data['plan_price'];
    }
    /**
     * Get count
     * @return string
     */
    function getPlanCount() {
        return $this->data['plan_count'];
    }

    /**
     * Get start date
     * @return string
     */
    function getPlanStart() {
        return $this->data['plan_start'];
    }


}