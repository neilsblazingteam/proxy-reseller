<?php
namespace ResellerApp\Entity;

use ResellerApp\Database\QueryBuilder;

/**
 * Class ResellerEntity
 * @package ResellerApp\Entity
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class ResellerEntity
{
    /**
     * @var null|QueryBuilder
     */
    public $db = null;
    /**
     * @var null|array
     */
    private $resellerCredentials = null;
    /**
     * @var null|array
     */
    private $data = null;

    /**
     * ResellerEntity constructor.
     * @param $db
     */
    function __construct($db) {
        $this->db = $db;
    }

    /**
     * Load reseller data by default.
     * We use this data to form framework parameters
     * used in every template. Like footer copyright.
     */
    function load() {
        $this->data = $this->db->select('bp_reseller')->order_by('id', 'DESC')->limit(1)->pull(1);
        $this->resellerCredentials = $this->db->select('bp_api_credentials')->pull(1);
    }

    /**
     * Get reseller id
     * @return string
     */
    function getId() {
        return $this->data['id'];
    }

    /**
     * Get reseller name
     * @return string
     */
    function getName() {
        return $this->data['name'];
    }

    /**
     * Get reseller logo
     * @return string
     */
    function getLogo() {
        return $this->data['logo'];
    }

    /**
     * Get reseller create date
     * @return string
     */
    function getCreatedOn() {
        return $this->data['created_on'];
    }

    /**
     * Get reseller API key
     * @return mixed
     */
    function getApiKey() {
        return $this->resellerCredentials['api_key'];
    }

    public function getSettings() {
        if (!isset($this->data[ '$settings_parsed' ])) {
            $this->data[ '$settings_parsed' ] = !empty($this->data[ 'settings' ]) ?
                json_decode($this->data[ 'settings' ], true) :  [];
        }

        return $this->data[ '$settings_parsed' ];
    }

    public function setSettings(array $data) {
        $this->data[ '$settings_parsed' ] = $data;

        $this->db->update('bp_reseller', ['settings' => json_encode($data)])->order_by('id', 'DESC')->limit(1)->go();

        return $this;
    }

    public function getFromSettings($key, $default = false) {
        $settings = $this->getSettings();

        return isset($settings[$key]) ? $settings[$key] : $default;
    }

    public function setToSettings($key, $value) {
        $this->data[ '$settings_parsed' ][ $key ] = $value;

        return $this;
    }

    public function persistSettings() {
        $this->db->update('bp_reseller', [
            'settings' => json_encode($this->data[ '$settings_parsed' ])
        ])->where('id', '=', $this->getId())->go();

        return $this;
    }
}