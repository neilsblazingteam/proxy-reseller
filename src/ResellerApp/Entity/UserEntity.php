<?php
namespace ResellerApp\Entity;

use ResellerApp\Database\QueryBuilder;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserEntity
 * @package ResellerApp\Entity
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class UserEntity implements UserInterface
{

    /**
     * @var null|QueryBuilder
     */
    public $db = null;
    /**
     * @var null|array
     */
    private $data = null;

    /**
     * UserEntity constructor.
     * @param $db
     */
    function __construct($db) {
        $this->db = $db;
    }

    /**
     * @param $data
     * @return array|null
     */
    function set($data) {
        $this->data = $data;
    }

    /**
     * @return array|null
     */
    function all() {
        return $this->data;
    }
    /**
     * Get user id
     * @return string
     */
    function getId() {
        return $this->data['id'];
    }
    function getBpId() {
        return $this->data['bp_id'];
    }
    /**
     * Get user name
     * @return string
     */
    function getUsername() {
        return $this->data['username'];
    }
    /**
     * Get user email
     * @return string
     */
    function getEmail() {
        return $this->data['email'];
    }

    /**
     * Get user password
     * @return string
     */
    function getPassword() {
        return $this->data['passphrase'];
    }

    /**
     * Get user create date
     * @return string
     */
    function getCreatedOn() {
        return $this->data['created_on'];
    }
    /**
     * Get user state
     * @return array
     */
    function getState() {
        return $this->data['state'];
    }
    /**
     * Get user roles
     * @return array
     */
    function getRoles() {
        return explode(',',$this->data['roles']);
    }

    /**
     * Return user data
     * @return array|null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}