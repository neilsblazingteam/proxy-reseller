<?php
namespace ResellerApp\Entity;

use ResellerApp\Database\QueryBuilder;

/**
 * Class PricingEntity
 * @package ResellerApp\Entity
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class PricingEntity
{

    /**
     * @var null|QueryBuilder
     */
    public $db = null;

    /**
     * @var null|array
     */
    private $data = null;

    /**
     * PricingEntity constructor.
     * @param $db
     */
    function __construct($db) {
        $this->db = $db;
    }

    /**
     * @param $data
     * @return array|null
     */
    function set($data) {
        $this->data = $data;
    }

    /**
     * @return array|null
     */
    function all() {
        return $this->data;
    }

    /**
     * Get id
     * @return string
     */
    function getId() {
        return $this->data['id'];
    }

    /**
     * Get country
     * @return string
     */
    function getCountry() {
        return $this->data['country'];
    }
    /**
     * Get category
     * @return string
     */
    function getCategory() {
        return $this->data['category'];
    }

}