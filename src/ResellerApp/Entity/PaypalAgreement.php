<?php
namespace ResellerApp\Entity;

use ResellerApp\Database\QueryBuilder;

/**
 * Class PaypalAgreement
 * @package ResellerApp\Entity
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class PaypalAgreement
{

    /**
     * @var null|QueryBuilder
     */
    public $db = null;
    /**
     * @var null|array
     */
    private $data = null;

    /**
     * PaypalAgreement constructor.
     * @param $db
     */
    function __construct($db) {
        $this->db = $db;
    }

    /**
     * @param $data
     * @return array|null
     */
    function set($data) {
        $this->data = $data;
    }

    /**
     * @return array|null
     */
    function all() {
        return $this->data;
    }

    /**
     * Get id
     * @return string
     */
    function getId() {
        return $this->data['id'];
    }
    /**
     * Get external user id
     * @return int
     */
    function getBpId() {
        return $this->data['bp_id'];
    }
    /**
     * Get agreement name
     * @return int
     */
    function getPlanId() {
        return $this->data['bp_plan_id'];
    }
    /**
     * Get agreement name
     * @return string
     */
    function getAgreementName() {
        return $this->data['agreement_name'];
    }
    /**
     * Get agreement description
     * @return string
     */
    function getAgreementDesc() {
        return $this->data['agreement_desc'];
    }
    /**
     * Get agreement start date
     * @return string
     */
    function getAgreementStartDate() {
        return $this->data['agreement_start_date'];
    }

}