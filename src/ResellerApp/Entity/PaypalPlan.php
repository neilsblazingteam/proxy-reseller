<?php
namespace ResellerApp\Entity;

use ResellerApp\Database\QueryBuilder;

/**
 * Class PaypalPlan
 * @package ResellerApp\Entity
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class PaypalPlan
{

    /**
     * @var null|QueryBuilder
     */
    public $db = null;
    /**
     * @var null|array
     */
    private $data = null;

    /**
     * PaypalPlan constructor.
     * @param $db
     */
    function __construct($db) {
        $this->db = $db;
    }

    /**
     * @param $data
     * @return array|null
     */
    function set($data) {
        $this->data = $data;
    }

    /**
     * @return array|null
     */
    function all() {
        return $this->data;
    }

    /**
     * Get id
     * @return string
     */
    function getId() {
        return $this->data['id'];
    }

    /**
     * Get tmp id
     * @return string
     */
    function getTmpId() {
        return $this->data['tmp_id'];
    }

    /**
     * Get plan id
     * @return string
     */
    function getPlanId() {
        return $this->data['plan_id'];
    }
    /**
     * Get plan state
     * @return string
     */
    function getPlanState() {
        return $this->data['plan_state'];
    }
    /**
     * Get plan create time
     * @return string
     */
    function getPlanCreateTime() {
        return $this->data['plan_create_time'];
    }

}