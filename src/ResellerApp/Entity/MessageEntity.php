<?php
namespace ResellerApp\Entity;

use ResellerApp\Database\QueryBuilder;

/**
 * Class MessageEntity
 * @package ResellerApp\Entity
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class MessageEntity
{

    /**
     * @var null|QueryBuilder
     */
    public $db = null;
    /**
     * @var null|array
     */
    private $data = null;

    /**
     * MessageEntity constructor.
     * @param $db
     */
    function __construct($db) {
        $this->db = $db;
    }

    /**
     * @param $data
     * @return array|null
     */
    function set($data) {
        $this->data = $data;
    }

    /**
     * @return array|null
     */
    function all() {
        return $this->data;
    }

    /**
     * Get user id
     * @return string
     */
    function getId() {
        return $this->data['id'];
    }

    /**
     * Get receiver id
     * @return string
     */
    function getReceiver() {
        return $this->data['receiver'];
    }
    /**
     * Get sender id
     * @return string
     */
    function getSender() {
        return $this->data['sender'];
    }
    /**
     * Get message subject
     * @return string
     */
    function getSubject() {
        return $this->data['subject'];
    }
    /**
     * Get message
     * @return string
     */
    function getMessage() {
        return $this->data['message'];
    }
    /**
     * Get create date
     * @return string
     */
    function getCreatedOn() {
        return $this->data['created_on'];
    }
    /**
     * Get read flag
     * @return string
     */
    function isRead() {
        return $this->data['is_read'];
    }
    /**
     * Get read flag
     * @return string
     */
    function isDeleted() {
        return $this->data['is_deleted'];
    }

}