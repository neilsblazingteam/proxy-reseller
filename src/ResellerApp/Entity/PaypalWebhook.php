<?php
namespace ResellerApp\Entity;

use ResellerApp\Database\QueryBuilder;

/**
 * Class PaypalWebhook
 * @package ResellerApp\Entity
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class PaypalWebhook
{

    /**
     * @var null|QueryBuilder
     */
    public $db = null;
    /**
     * @var null|array
     */
    private $data = null;

    /**
     * PaypalWebhook constructor.
     * @param $db
     */
    function __construct($db) {
        $this->db = $db;
    }

    /**
     * @param $data
     * @return array|null
     */
    function set($data) {
        $this->data = $data;
    }

    /**
     * @return array|null
     */
    function all() {
        return $this->data;
    }

    /**
     * Get id
     * @return string
     */
    function getId() {
        return $this->data['id'];
    }

    /**
     * Get plan id
     * @return string
     */
    function getWebhookId() {
        return $this->data['webhook_id'];
    }
    /**
     * Get webhook url
     * @return string
     */
    function getWebhookUrl() {
        return $this->data['webhook_url'];
    }
    /**
     * Get plan create time
     * @return string
     */
    function getWebhookEventTypes() {
        return $this->data['webhook_event_types'];
    }
    /**
     * Get valid from datetime
     * @return string
     */
    function getWebhookValidFrom() {
        return $this->data['webhook_valid_from'];
    }
    /**
     * Get valid to datetime
     * @return string
     */
    function getWebhookValidTo() {
        return $this->data['webhook_valid_to'];
    }

}