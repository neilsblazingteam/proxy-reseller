<?php
namespace ResellerApp;

define('PATH_APP', \SymlinkDetective::detectPath(__FILE__, '/../../..'));

use ResellerApp\Api\BlazingProxiesAdapter;
use ResellerApp\Database\Connection;
use ResellerApp\Database\QueryBuilder;
use ResellerApp\Repo\MessageRepo;
use ResellerApp\Repo\PricingRepo;
use ResellerApp\Repo\ResellerRepo;
use ResellerApp\Repo\UserRepo;
use ResellerApp\Routing\Router;
use ResellerApp\Templating\LayoutExtension;
use ResellerApp\Templating\TwigEngine;
use ResellerApp\Utils\Mailer;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Yaml\Yaml;
use Trifledev\Phpay\Paypal\PayPalAdapter;
use Twig_Environment;

/**
 * Class AppFramework
 *
 * Base application controller.
 * Controllers will extend this class and other
 * script can call the static getInstance() function
 * to use any generic functions provided by this class.
 *
 * @package ResellerApp
 * @author Victoria Speckmann-Bresges <vspeckmann@gmail.com>
 */
class AppFramework
{
    /**
     * @var null
     */
    protected static $instance;
    /**
     * @var BlazingProxiesAdapter
     */
    protected $api;
    /**
     * @var string
     */
    private $app_url;
    /**
     * @var string
     */
    protected $mode = 'test';
    /**
     * @var null|Request Request object
     */
    protected $request = null;
    /**
     * @var null|Response Response object
     */
    protected $response = null;
    /**
     * @var QueryBuilder $db
     */
    protected $database = null;
    /**
     * @var null
     */
    protected $view = null;
    /**
     * @var null
     */
    protected $route = null;
    /**
     * @var null|Session
     */
    protected $session = null;
    /**
     * @var array|mixed|null
     */
    protected $config = null;
    /**
     * @var null|Mailer
     */
    protected $mailer = null;
    /**
     * @var null|Router
     */
    protected $router;
    /**
     * @var array
     */
    protected $config_files_with_mode_importance = ['routes','database'];
    /**
     * @var null | TwigEngine
     */
    private $templating;
    /**
     * @var ResellerRepo
     */
    private $resellerRepo;
    /**
     * @var UserRepo
     */
    private $userRepo;
    /**
     * @var MessageRepo
     */
    private $messageRepo;
    /**
     * @var PricingRepo
     */
    private $pricingRepo;
    /**
     * @var PayPalAdapter
     */
    private $paymentApi;
    /**
     * @var array
     */
    private $app_paths = [
            'templates'=>'/app/resources/templates/twig',
            'config'=>'/app/config',
            'log'=>'/app/log',
            'cert'=>'/app/cert'
        ];

    function setup() {

        $this->loadConfig();
        $this->setRequest(Request::createFromGlobals());
        $this->setResponse(new Response());
        $this->setRouter(new Router(dirname(__FILE__).DIRECTORY_SEPARATOR .'..'.DIRECTORY_SEPARATOR .'..'.DIRECTORY_SEPARATOR .'app/config'));
        $this->setTemplating(new TwigEngine(new Twig_Environment(), new TemplateNameParser(), dirname(__FILE__).'/../../app/resources/templates/twig'));
        $this->setDatabase($this->connectDatabase($this->getConfig('database')));
        $this->loadController();

    }
    /**
     * Returns the instance of this class.
     *
     * @return AppFramework The instance.
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }
    public function __construct()
    {
        static::$instance = $this;
    }

    public function loadConfig($type = null, $configPath = null) {
        if (!$configPath) {
            $configPath = $this->getAppPath() . '/app/config';
        }

        $defaultTypes = ["database","paypal","mail","app"];
        $configContainer = [];

        // Already loaded
        if (isset($this->config[$type])) {
            return $this->config;
        }

        if ($type) {
            $files = [$type];
        }
        else {
            $files = $defaultTypes;
        }

        foreach ($files as $filename) {
            foreach (array_filter(['php', 'yml']) as $ext) {
                $filePath = $configPath . DIRECTORY_SEPARATOR . $filename . '.' . $ext;

                $config = null;
                if (is_readable($filePath)) {
                    switch ($ext) {
                        case 'php':
                            $config = include $filePath;
                            break;

                        case 'yml':
                            $config = Yaml::parse(file_get_contents($filePath));
                            break;
                    }

                    if (!is_null($config)) {
                        $configContainer[ $filename ] = $config;

                        break;
                    }
                }
            }
        }

        $this->config = array_merge($this->config ? $this->config : [], $configContainer);

        return $this->config;

    }

    protected function redirectToInstallationPage($stopExecution = true) {
        header('Location: /install/');

        if ($stopExecution) {
            exit;
        }
    }

    /**
     * @param $params
     * @return \ResellerApp\Database\QueryBuilder
     */
    private function connectDatabase($params) {

        $dbq = null;

        // if no database exists, redirect to setup
        if(!isset($params['database']))
            $this->redirectToInstallationPage();

        // connect to database or instance
        $conn = Connection::get_instance();
        $conn->switchProfilingOn();

        if (is_scalar($params))	{
            $dbq = $conn->get_connection($params);
        } elseif ( ! is_scalar($params) && ! is_null($params)) {

            $params_object = new \stdClass();

            foreach($params as $k => $v) {
                $params_object->$k = $v;
            }

            // otherwise, return new connection
            $dbq = $conn->connect($params_object);

        }

        // if no api_credentials table exists at least, redirect to setup
        try {

            $dbq->select('bp_api_credentials')->pull(1);

        } catch (\Exception $e) {
            $this->redirectToInstallationPage();
        }

        return $dbq;

    }

    /**
     * @param $mode
     */
    function setMode($mode) {
        $this->mode = $mode;
    }

    /**
     * Returns application mode
     * @return string test|prod|stag
     */
    function getMode() {
        return $this->mode;
    }

    /**
     * Get application path
     * @param string $path
     * @return null
     */
    function getPath($path) {
        $rootPath = $this->getAppPath();
        $realPath = isset($this->app_paths[$path]) ? $rootPath.$this->app_paths[$path] : null;
        return $realPath;
    }
    /**
     * @return null|Session
     */
    public function getSession() {
        if(is_null($this->session))
            $this->setSession(new Session());
        return $this->session;
    }

    /**
     * @return QueryBuilder
     */
    public function getDatabase() {
        if(is_null($this->database))
            $this->setDatabase($this->connectDatabase($this->getConfig('database')));
        return $this->database;
    }

    /**
     * @return null|Request
     */
    public function getRequest() {
        if(is_null($this->request))
            $this->setRequest(Request::createFromGlobals());
        return $this->request;
    }

    /**
     * @return null|Response
     */
    public function getResponse() {
        if(is_null($this->response))
            $this->setResponse(new Response());
        return $this->response;
    }

    /**
     * @param string $type
     * @return array|null
     */
    public function getConfig($type='') {
        if(is_null($this->config))
            $this->loadConfig(null,$this->getPath('config'));
        if(isset($this->config[$type]))
            return $this->config[$type];
        if(empty($type))
            return $this->config;
        return null;

    }
    /**
     * @return null|Mailer
     */
    public function getMailer() {
        if(is_null($this->mailer))
            $this->setMailer(new Mailer($this->getTemplating(),$this->getConfig('mail')));
        return $this->mailer;
    }
    /**
     * @return null|TwigEngine
     */
    public function getTemplating()
    {
        $env = new Twig_Environment();
        $env->addExtension(new LayoutExtension());
        $this->templating = new TwigEngine($env, new TemplateNameParser(), $this->getPath('templates'));
        return $this->templating;
    }

    /**
     * @return null|Router
     */
    public function getRouter()
    {
        if(is_null($this->router))
            $this->setRouter(new Router(dirname(__FILE__).DIRECTORY_SEPARATOR .'..'.DIRECTORY_SEPARATOR .'..'.DIRECTORY_SEPARATOR .'app/config'));
        return $this->router;
    }
    /**
     * @param null|TwigEngine $templating
     */
    public function setTemplating($templating)
    {
        $this->templating = $templating;
    }

    /**
     * @param null|Response $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @param null|Request $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @param array|mixed|null $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @param $key
     * @param array|mixed|null $config
     */
    public function setConfigKey($key,$config)
    {
        $this->config[$key] = $config;
    }

    /**
     * @param null|Router $router
     */
    public function setRouter($router)
    {
        $this->router = $router;
    }

    /**
     * @param null|Session $session
     */
    public function setSession($session)
    {
        $this->session = $session;
    }

    /**
     * @param QueryBuilder $database
     */
    public function setDatabase($database)
    {
        $this->database = $database;
    }

    /**
     * Load front controller
     * @return mixed
     */
    private function loadController()
    {

        $context = new RequestContext();
        $context->fromRequest($this->getRequest());

        $matcher = new UrlMatcher($this->getRouter()->getRoutes(), $context);
        $resolver = new ControllerResolver;
        $matchedRoutes = $matcher->match($this->getRequest()->getPathInfo());

        $this->getRequest()->attributes->add($matchedRoutes);

        $controller = $resolver->getController($this->getRequest());
        $arguments = $resolver->getArguments($this->getRequest(), $controller);

        $response = call_user_func_array($controller, $arguments);
        return $response;

    }

    /**
     * Set mailer class
     * @param $mailer|Mailer
     */
    private function setMailer($mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Get reseller repository
     *
     * @return ResellerRepo
     */
    public function getResellerRepo()
    {
        if(is_null($this->resellerRepo)) {
            $this->resellerRepo =  new ResellerRepo($this->getDatabase());
            $this->resellerRepo->load();
        }
        return $this->resellerRepo;
    }
    /**
     * Get pricing repository
     *
     * @return PricingRepo
     */
    public function getPricingRepo()
    {
        if(is_null($this->pricingRepo)) {
            $this->pricingRepo =  new PricingRepo($this->getDatabase());
        }
        return $this->pricingRepo;
    }

    /**
     * Get reseller repository
     *
     * @return UserRepo
     */
    public function getUserRepo()
    {
        if(is_null($this->userRepo)) {
            $this->userRepo =  new UserRepo($this->getDatabase());
        }
        return $this->userRepo;
    }

    /**
     * Get message repository
     *
     * @return MessageRepo
     */
    public function getMessageRepo()
    {
        if(is_null($this->messageRepo)) {
            $this->messageRepo =  new MessageRepo($this->getDatabase());
        }
        return $this->messageRepo;
    }

    /**
     * Get payment API
     *
     * @return PaypalAdapter
     */
    public function getPaymentApi()
    {
        if(is_null($this->paymentApi)) {
            $paypalConfig = $this->getConfig('paypal');
            $logfile = $this->getAppPath().'/app/log/payment/paypal/paypal.log';
            $appEndpoint = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}";
            $mode = $paypalConfig['mode'];
            $clientId = $paypalConfig['credentials'][$mode]['clientId'];
            $secret = $paypalConfig['credentials'][$mode]['secret'];
            $this->paymentApi =  new PaypalAdapter($clientId,$secret,$appEndpoint,$logfile);
            $this->paymentApi->setMode($mode);
        }
        return $this->paymentApi;
    }

    /**
     * @return BlazingProxiesAdapter
     */
    public function getApi()
    {

        if(is_null($this->api)) {

            $this->api = new BlazingProxiesAdapter([
                'api_key'=>$this->getResellerRepo()->getApiKey()
            ]);

        }

        return $this->api;
    }

    /**
     * @param BlazingProxiesAdapter $api
     */
    public function setApi($api)
    {
        $this->api = $api;
    }

    /**
     * Output template to browser
     *
     * @param $template
     * @param array $params
     */
    function output($template,$params=[]) {

        $template = $this->render($template,$params);
        $this->getResponse()->setContent($template);

        $this->getResponse()->send();
        exit;

    }

    /**
     * Render template
     *
     * Sets framework parameters needed all around
     * the frontend. If a user is logged in, his info
     * will be pulled and set as well.
     *
     * @param $template
     * @param array $params
     * @return string
     */
    function render($template,$params=[]) {

        $reseller = $this->getResellerRepo();
        $uData = $this->getUserRepo()->all();
        // see if we have stored messages in flash bag to output
        $successMessages = $this->getSession()->getFlashBag()->get('success');
        if($successMessages) {
            $params['success_msg'] = implode('<br>',$successMessages);
        }
        $errorMessages = $this->getSession()->getFlashBag()->get('error');
        if($errorMessages) {
            $params['error_msg'] = implode('<br>',$errorMessages);
        }
        $infoMessages = $this->getSession()->getFlashBag()->get('info');
        if($infoMessages) {
            $params['info_msg'] = implode('<br>',$infoMessages);
        }

        $baseParams = [
            'app_url'=>$this->getAppUrl(),
            'app_routes'=>$this->getRouter()->all(),
            'logo_url'=>$this->getAppUrl().'/gui/img/logo.'. ($reseller->getLogo() ? $reseller->getLogo() : 'jpg'),
            'app_name'=>$reseller->getName(),
            'copyright_year'=>$reseller->computeCopyrightDate(),
            'show_paging'=>true,
            'is_admin_path'=>strpos($this->getRequest()->getUri(), 'admin') !== false
        ];

        if(isset($uData['username'])) {
            $baseParams['username'] = $uData['username'];
            $baseParams['is_admin'] = in_array('ROLE_ADMIN',explode(',',$uData['roles']));
        }

        $userParams['unread_messages'] = $this->getMessageRepo()->getUnreadMessages($this->getUserRepo()->getId());
        $userParams['unread_messages_count'] = $this->getMessageRepo()->getUnreadMessagesCount($this->getUserRepo()->getId());

        $params = array_merge($baseParams,$params,$userParams);

        $template = $this->getTemplating()->render($template, $params);

        return $template;

    }

    /**
     * Stores a message to flashbag for display
     * after user has been redirected to given route.
     * Optionally a url can be passed.
     *
     * @param $endpoint
     * @param $type
     * @param $content
     * @param string $url
     */
    public function redirectWithNotification($endpoint,$type,$content,$url='') {

        $this->getSession()->getFlashBag()->add($type,$content);

        if(empty($url))
            $url = $this->getAppUrl().$endpoint;

        $response = new RedirectResponse($url);
        $response->send();
        exit;

    }
    /**
     * Get full application URL
     * @return mixed
     */
    public function getAppUrl()
    {
        return 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}";
    }
    /**
     * Set full application URL
     * @param mixed $app_url
     */
    public function setAppUrl($app_url)
    {
        $this->app_url = $app_url;
    }

    /**
     * Determine if application is running in test mode
     * @return int
     */
    public function isTestMode() {
        return $this->mode === 'test' ? 1 : 0;
    }

    /**
     * @return string
     */
    public function getAppPath()
    {
        return PATH_APP;
    }

}