<?php

namespace Migration\Mapping;

use Doctrine\ORM\Mapping as ORM;

/**
 * BpTerms
 *
 * @ORM\Table(name="bp_terms")
 * @ORM\Entity
 */
class BpTerms
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="terms_content", type="text", length=65535, nullable=false)
   */
  private $termsContent;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="created_on", type="datetime", nullable=false)
   */
  private $createdOn;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="valid_until", type="datetime", nullable=false)
   */
  private $validUntil;


}

