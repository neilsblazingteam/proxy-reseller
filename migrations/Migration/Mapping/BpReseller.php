<?php

namespace Migration\Mapping;

use Doctrine\ORM\Mapping as ORM;

/**
 * BpReseller
 *
 * @ORM\Table(name="bp_reseller")
 * @ORM\Entity
 */
class BpReseller
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=255, nullable=false)
   */
  private $name;

  /**
   * @var string
   *
   * @ORM\Column(name="logo", type="string", length=3, nullable=false)
   */
  private $logo;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="created_on", type="datetime", nullable=false)
   */
  private $createdOn;

  /**
   * @var string
   *
   * @ORM\Column(name="settings", type="text", length=65535, nullable=true)
   */
  private $settings;


}

