<?php

namespace Migration\Mapping;

use Doctrine\ORM\Mapping as ORM;

/**
 * BpApiCredentials
 *
 * @ORM\Table(name="bp_api_credentials")
 * @ORM\Entity
 */
class BpApiCredentials
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="api_key", type="string", length=255, nullable=false)
   */
  private $apiKey;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="created_on", type="datetime", nullable=false)
   */
  private $createdOn;


}

