<?php

namespace Migration\Mapping;

use Doctrine\ORM\Mapping as ORM;

/**
 * BpPricingReseller
 *
 * @ORM\Table(name="bp_pricing_reseller")
 * @ORM\Entity
 */
class BpPricingReseller
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="country", type="string", length=2, nullable=false)
   */
  private $country;

  /**
   * @var string
   *
   * @ORM\Column(name="category", type="string", length=45, nullable=false)
   */
  private $category;

  /**
   * @var string
   *
   * @ORM\Column(name="price_tier_1", type="decimal", precision=10, scale=6, nullable=false)
   */
  private $priceTier1;

  /**
   * @var string
   *
   * @ORM\Column(name="price_tier_2", type="decimal", precision=10, scale=6, nullable=false)
   */
  private $priceTier2;

  /**
   * @var string
   *
   * @ORM\Column(name="price_tier_3", type="decimal", precision=10, scale=6, nullable=false)
   */
  private $priceTier3;

  /**
   * @var string
   *
   * @ORM\Column(name="price_tier_4", type="decimal", precision=10, scale=6, nullable=false)
   */
  private $priceTier4;

  /**
   * @var string
   *
   * @ORM\Column(name="price_tier_5", type="decimal", precision=10, scale=6, nullable=false)
   */
  private $priceTier5;

  /**
   * @var string
   *
   * @ORM\Column(name="price_tier_6", type="decimal", precision=10, scale=6, nullable=false)
   */
  private $priceTier6;


}

