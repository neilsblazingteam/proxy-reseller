<?php

namespace Migration\Mapping;

use Doctrine\ORM\Mapping as ORM;

/**
 * BpUser
 *
 * @ORM\Table(name="bp_user")
 * @ORM\Entity
 */
class BpUser
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var integer
   *
   * @ORM\Column(name="bp_id", type="integer", nullable=false)
   */
  private $bpId;

  /**
   * @var string
   *
   * @ORM\Column(name="email", type="string", length=255, nullable=false)
   */
  private $email;

  /**
   * @var string
   *
   * @ORM\Column(name="username", type="string", length=45, nullable=false)
   */
  private $username;

  /**
   * @var string
   *
   * @ORM\Column(name="passphrase", type="string", length=255, nullable=false)
   */
  private $passphrase;

  /**
   * @var string
   *
   * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
   */
  private $firstName;

  /**
   * @var string
   *
   * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
   */
  private $lastName;

  /**
   * @var string
   *
   * @ORM\Column(name="roles", type="string", length=255, nullable=false)
   */
  private $roles = 'USER';

  /**
   * @var string
   *
   * @ORM\Column(name="state", type="string", length=24, nullable=false)
   */
  private $state = 'active';

  /**
   * @var string
   *
   * @ORM\Column(name="auth_token", type="string", length=255, nullable=false)
   */
  private $authToken;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="created_on", type="datetime", nullable=false)
   */
  private $createdOn;


}

