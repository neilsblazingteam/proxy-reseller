<?php

namespace Migration\Mapping;

use Doctrine\ORM\Mapping as ORM;

/**
 * BpPricingTier
 *
 * @ORM\Table(name="bp_pricing_tier")
 * @ORM\Entity
 */
class BpPricingTier
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var integer
   *
   * @ORM\Column(name="tier", type="integer", nullable=false)
   */
  private $tier;

  /**
   * @var integer
   *
   * @ORM\Column(name="from_val", type="integer", nullable=false)
   */
  private $fromVal;

  /**
   * @var integer
   *
   * @ORM\Column(name="to_val", type="integer", nullable=false)
   */
  private $toVal;


}

