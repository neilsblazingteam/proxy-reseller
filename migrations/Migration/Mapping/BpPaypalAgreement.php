<?php

namespace Migration\Mapping;

use Doctrine\ORM\Mapping as ORM;

/**
 * BpPaypalAgreement
 *
 * @ORM\Table(name="bp_paypal_agreement")
 * @ORM\Entity
 */
class BpPaypalAgreement
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var integer
   *
   * @ORM\Column(name="bp_id", type="integer", nullable=false)
   */
  private $bpId;

  /**
   * @var integer
   *
   * @ORM\Column(name="bp_plan_id", type="integer", nullable=false)
   */
  private $bpPlanId;

  /**
   * @var string
   *
   * @ORM\Column(name="agreement_id", type="string", length=255, nullable=false)
   */
  private $agreementId;

  /**
   * @var string
   *
   * @ORM\Column(name="agreement_name", type="string", length=255, nullable=false)
   */
  private $agreementName;

  /**
   * @var string
   *
   * @ORM\Column(name="agreement_desc", type="string", length=255, nullable=false)
   */
  private $agreementDesc;

  /**
   * @var string
   *
   * @ORM\Column(name="agreement_state", type="string", length=45, nullable=false)
   */
  private $agreementState;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="agreement_start_date", type="datetime", nullable=false)
   */
  private $agreementStartDate;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="agreement_end_date", type="datetime", nullable=false)
   */
  private $agreementEndDate;

  /**
   * @var string
   *
   * @ORM\Column(name="plan_id", type="string", length=255, nullable=false)
   */
  private $planId;

  /**
   * @var string
   *
   * @ORM\Column(name="payer_id", type="string", length=255, nullable=false)
   */
  private $payerId;


}

