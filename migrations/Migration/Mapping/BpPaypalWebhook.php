<?php

namespace Migration\Mapping;

use Doctrine\ORM\Mapping as ORM;

/**
 * BpPaypalWebhook
 *
 * @ORM\Table(name="bp_paypal_webhook")
 * @ORM\Entity
 */
class BpPaypalWebhook
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="webhook_id", type="string", length=255, nullable=false)
   */
  private $webhookId;

  /**
   * @var string
   *
   * @ORM\Column(name="webhook_url", type="string", length=255, nullable=false)
   */
  private $webhookUrl;

  /**
   * @var string
   *
   * @ORM\Column(name="webhook_event_types", type="string", length=255, nullable=false)
   */
  private $webhookEventTypes;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="valid_from", type="datetime", nullable=false)
   */
  private $validFrom;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="valid_to", type="datetime", nullable=false)
   */
  private $validTo;


}

