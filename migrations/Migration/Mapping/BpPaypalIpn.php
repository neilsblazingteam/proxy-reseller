<?php

namespace Migration\Mapping;

use Doctrine\ORM\Mapping as ORM;

/**
 * BpPaypalIpn
 *
 * @ORM\Table(name="bp_paypal_ipn")
 * @ORM\Entity
 */
class BpPaypalIpn
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="txn_id", type="string", length=255, nullable=false)
   */
  private $txnId;

  /**
   * @var string
   *
   * @ORM\Column(name="payment_status", type="string", length=45, nullable=false)
   */
  private $paymentStatus;

  /**
   * @var string
   *
   * @ORM\Column(name="payer_id", type="string", length=255, nullable=false)
   */
  private $payerId;

  /**
   * @var string
   *
   * @ORM\Column(name="recurring_payment_id", type="string", length=255, nullable=false)
   */
  private $recurringPaymentId;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="created_on", type="datetime", nullable=false)
   */
  private $createdOn;


}

