<?php

namespace Migration\Mapping;

use Doctrine\ORM\Mapping as ORM;

/**
 * BpPaypalPlan
 *
 * @ORM\Table(name="bp_paypal_plan")
 * @ORM\Entity
 */
class BpPaypalPlan
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="user_email", type="string", length=255, nullable=false)
   */
  private $userEmail;

  /**
   * @var string
   *
   * @ORM\Column(name="buyer_id", type="string", length=255, nullable=false)
   */
  private $buyerId;

  /**
   * @var string
   *
   * @ORM\Column(name="plan_id", type="string", length=255, nullable=false)
   */
  private $planId;

  /**
   * @var string
   *
   * @ORM\Column(name="plan_state", type="string", length=255, nullable=false)
   */
  private $planState;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="plan_create_time", type="datetime", nullable=false)
   */
  private $planCreateTime;


}

