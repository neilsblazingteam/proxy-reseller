<?php

namespace Migration\Mapping;

use Doctrine\ORM\Mapping as ORM;

/**
 * BpPaypalWebhookEvent
 *
 * @ORM\Table(name="bp_paypal_webhook_event")
 * @ORM\Entity
 */
class BpPaypalWebhookEvent
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="event_id", type="string", length=255, nullable=false)
   */
  private $eventId;

  /**
   * @var string
   *
   * @ORM\Column(name="event_type", type="string", length=255, nullable=false)
   */
  private $eventType;

  /**
   * @var string
   *
   * @ORM\Column(name="agreement_id", type="string", length=255, nullable=false)
   */
  private $agreementId;

  /**
   * @var string
   *
   * @ORM\Column(name="payer_id", type="string", length=255, nullable=false)
   */
  private $payerId;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="created_on", type="datetime", nullable=false)
   */
  private $createdOn;


}

