<?php

namespace Migration\Mapping;

use Doctrine\ORM\Mapping as ORM;

/**
 * BpUserPlan
 *
 * @ORM\Table(name="bp_user_plan")
 * @ORM\Entity
 */
class BpUserPlan
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var integer
   *
   * @ORM\Column(name="bp_id", type="integer", nullable=false)
   */
  private $bpId;

  /**
   * @var integer
   *
   * @ORM\Column(name="bp_plan_id", type="integer", nullable=false)
   */
  private $bpPlanId;

  /**
   * @var string
   *
   * @ORM\Column(name="payment_type", type="string", length=25, nullable=false)
   */
  private $paymentType;

  /**
   * @var string
   *
   * @ORM\Column(name="payment_plan_id", type="string", length=255, nullable=false)
   */
  private $paymentPlanId;

  /**
   * @var string
   *
   * @ORM\Column(name="payment_agreement_id", type="string", length=255, nullable=false)
   */
  private $paymentAgreementId;

  /**
   * @var string
   *
   * @ORM\Column(name="plan_country", type="string", length=2, nullable=false)
   */
  private $planCountry;

  /**
   * @var string
   *
   * @ORM\Column(name="plan_category", type="string", length=45, nullable=false)
   */
  private $planCategory;

  /**
   * @var integer
   *
   * @ORM\Column(name="plan_count", type="integer", nullable=false)
   */
  private $planCount;

  /**
   * @var integer
   *
   * @ORM\Column(name="plan_days", type="integer", nullable=false)
   */
  private $planDays;

  /**
   * @var string
   *
   * @ORM\Column(name="plan_price", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $planPrice;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="plan_start", type="datetime", nullable=false)
   */
  private $planStart;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="plan_expiry", type="datetime", nullable=false)
   */
  private $planExpiry;

  /**
   * @var string
   *
   * @ORM\Column(name="plan_status", type="string", length=24, nullable=false)
   */
  private $planStatus = 'active';


}

