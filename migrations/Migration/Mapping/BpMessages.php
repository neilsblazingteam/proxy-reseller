<?php

namespace Migration\Mapping;

use Doctrine\ORM\Mapping as ORM;

/**
 * BpMessages
 *
 * @ORM\Table(name="bp_messages")
 * @ORM\Entity
 */
class BpMessages
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var integer
   *
   * @ORM\Column(name="receiver", type="integer", nullable=false)
   */
  private $receiver;

  /**
   * @var integer
   *
   * @ORM\Column(name="sender", type="integer", nullable=false)
   */
  private $sender;

  /**
   * @var string
   *
   * @ORM\Column(name="subject", type="string", length=255, nullable=false)
   */
  private $subject;

  /**
   * @var string
   *
   * @ORM\Column(name="message", type="text", length=65535, nullable=false)
   */
  private $message;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="created_on", type="datetime", nullable=false)
   */
  private $createdOn;

  /**
   * @var boolean
   *
   * @ORM\Column(name="is_read_receiver", type="boolean", nullable=false)
   */
  private $isReadReceiver = '0';

  /**
   * @var boolean
   *
   * @ORM\Column(name="is_important_receiver", type="boolean", nullable=false)
   */
  private $isImportantReceiver = '0';

  /**
   * @var boolean
   *
   * @ORM\Column(name="is_deleted_receiver", type="boolean", nullable=false)
   */
  private $isDeletedReceiver = '0';

  /**
   * @var boolean
   *
   * @ORM\Column(name="is_purged_receiver", type="boolean", nullable=false)
   */
  private $isPurgedReceiver = '0';

  /**
   * @var boolean
   *
   * @ORM\Column(name="is_read_sender", type="boolean", nullable=false)
   */
  private $isReadSender = '0';

  /**
   * @var boolean
   *
   * @ORM\Column(name="is_important_sender", type="boolean", nullable=false)
   */
  private $isImportantSender = '0';

  /**
   * @var boolean
   *
   * @ORM\Column(name="is_deleted_sender", type="boolean", nullable=false)
   */
  private $isDeletedSender = '0';

  /**
   * @var boolean
   *
   * @ORM\Column(name="is_purged_sender", type="boolean", nullable=false)
   */
  private $isPurgedSender = '0';


}

