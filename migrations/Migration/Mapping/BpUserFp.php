<?php

namespace Migration\Mapping;

use Doctrine\ORM\Mapping as ORM;

/**
 * BpUserFp
 *
 * @ORM\Table(name="bp_user_fp")
 * @ORM\Entity
 */
class BpUserFp
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="email", type="string", length=45, nullable=false)
   */
  private $email;

  /**
   * @var string
   *
   * @ORM\Column(name="token", type="string", length=255, nullable=false)
   */
  private $token;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="created_on", type="datetime", nullable=false)
   */
  private $createdOn;

  /**
   * @var boolean
   *
   * @ORM\Column(name="is_reset", type="boolean", nullable=false)
   */
  private $isReset = '0';


}

