------------------------------------------------------------------
Installation instructions for Blazing Proxies Reseller Application
------------------------------------------------------------------

REQUIREMENTS
------------
This application needs a web server running PHP 5.5 or later (http://www.php.net/)
and MySQL 5.5 or later (http://www.mysql.com/). Apache web server and MySQL database are recommended;
other web server and database combinations were not tested to the same extend.

You will need a Paypal account and setup API credentials for this application at https://www.paypal.com

EXTENSIONS
----------
Following extensions must be installed and enabled prior to installation.

- cURL
- IMAP
- PDO Mysql

PREREQUISITES
-------------
You can download the most recent build from Bitbucket. https://bitbucket.org/blazingseoneil/proxy-reseller

1.) Set up a database in MySQL
2.) Upload the contents of the ZIP file to your server and point to the --public-- directory.
3.) Visit your installation script: http(s)://example-website.com/install and follow the instructions

NOTE: If you chose to use IPN Url to receive PayPal transactions you must add IPN listener URL to your
paypal merchant account. Instructions: https://developer.paypal.com/docs/classic/ipn/integration-guide/IPNSetup/

APP INSTALLATION
----------------
1.) Enter database name, host, username and password for you MySQL dtabase.
2.) Check the requirements and enable any missing modules or update PHP or MySQL.
3.) Enter your API key you received from Blazing Proxies and choose a username for your first user and admin (you).
    Set a password for your new user and to log into your application after finalizing installation.
    Enter your name or your company and add a logo (optional).
4.) Enter PayPal credentials (client id and client secret)
5.) Press finalize button
6.) Remove install directory in --public-- directory


CONFIGURATION NGINX
-------------------
Example of minimal configuration for nginx web server:
server {
        listen       80;
        server_name  example-website.com;
        root /usr/share/nginx/html/blazing_proxies_reseller_app/public;

        location ^~ /install/ {
            fastcgi_pass   unix:/var/run/php5-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
        }
        location /gui/ {}
        location / {
            try_files $uri /app.php$is_args$args;
        }
        location ~ \.php$ {
                try_files $uri =404;
                fastcgi_pass unix:/var/run/php5-fpm.sock;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                include fastcgi_params;
        }
}



IMPORTANT NOTES
---------------
-- .htaccess
For this application to work properly, it is essential that mod_rewrite be enabled in your server config.
Bear in mind that some FTP clients will interpret .htaccess as a hidden file and not copy them to the server by default.
So please make sure the .htaccess files is found in public root path.

-- SSL and Paypal
For webhooks to work SSL must be enabled on your website.

-- SELinux on centos
You must set the correct directory permissions to the application folder. See https://wiki.centos.org/HowTos/SELinux.

-- Directory Permissions
The directories /app/config and /public/gui/img must be writable by the webserver user. 
Config and image files will be created upon installation.
